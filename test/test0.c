#include <mkit.h>

gint
main (gint argc, gchar **argv)
{
    g_autoptr (MkGirepo) gir = NULL;
    g_autoptr (GError) error = NULL;

    gir = mk_girepo_new ();

    mk_girepo_load (gir, "Gtk-4.0", &error);

    if (error) {
	g_message ("%s", error->message);
    }

    return 0;
}
