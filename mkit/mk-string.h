#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define MK_TYPE_STRING (mk_string_get_type ())

#ifdef __GI_SCANNER__
#define MK_STRING_STRUCT
#endif

#ifdef MK_STRING_STRUCT
typedef struct {
    gchar s[0];
} MkString;
#else
typedef gchar MkString;
#endif

#define	mk_string_newa_len(str, len) \
    mk_string_new_full (str, len, -1, \
			alloca (mk_string_meta_size + (len) + 1))
#define	mk_string_newa(str) \
    mk_string_new_full (str, -1, -1, \
			alloca (mk_string_meta_size + strlen (str) + 1))
#define mk_string_refa(str) \
    mk_string_new_full (str, mk_string_len (str), -1, \
			alloca (mk_string_meta_size + mk_string_len (str) + 1))

extern const guint mk_string_meta_size;

GType		mk_string_get_type	    (void) G_GNUC_CONST;
MkString       *mk_string_new		    (const gchar *str)
					    G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_new_shared	    (const gchar *str)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_new_shared_len    (const gchar *str,
					     gssize len)
					    G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_new_length	    (const gchar *str,
					     gssize len)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_new_full	    (const gchar *str,
					     gssize len,
					     gssize cap,
					     gpointer alloca_mem)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_ref		    (MkString *self);
void		mk_string_unref		    (MkString *self);
MkString       *mk_string_repeat_c	    (MkString *self,
					     gchar c, guint n)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_set_len	    (MkString *self,
					     gsize len)
                                            G_GNUC_WARN_UNUSED_RESULT;
const guint8   *mk_string_data		    (MkString *self,
					     gsize *len);
const gchar    *mk_string_str		    (MkString *self);
guint		mk_string_len		    (MkString *self);
guint		mk_string_cap		    (MkString *self);
gboolean	mk_string_is_mutable	    (MkString *self);
gboolean	mk_string_is_shared	    (MkString *self);
gboolean	mk_string_is_alloca	    (MkString *self);
gboolean	mk_string_is_single	    (MkString *self);
gboolean	mk_string_is_empty	    (MkString *self);
MkString       *mk_string_make_mutable	    (MkString *self,
					     gssize cap)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_make_shared	    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_assign	    (MkString *self,
					     const gchar *str)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_assign_len	    (MkString *self,
					     const gchar *str,
					     gssize len)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_tr_char	    (MkString *self,
					     gint (*tr) (gint))
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_del_char	    (MkString *self,
					     gint c)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_underscorify	    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_tolower	    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_toupper	    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_tocamel	    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_tocanon	    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_remove_dash	    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_append_s	    (MkString *self,
					     const gchar *str)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_append_c	    (MkString *self,
					     gchar c)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_append_0	    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_append_v	    (MkString *self,
					     const gchar *fmt,
					     va_list args)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_append	    (MkString *self,
					     const gchar *fmt,
					     ...)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_prepend_s	    (MkString *self,
					     const gchar *str)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_prepend_c	    (MkString *self,
					     gchar c)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_prepend	    (MkString *self,
					     const gchar *fmt,
					     ...)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_chomp		    (MkString *self)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_substring	    (MkString *self,
					     glong begin,
					     glong length)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_trunc		    (MkString *self,
					     glong size)
                                            G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_cut		    (MkString *self,
					     gulong begin,
					     gulong length)
					    G_GNUC_WARN_UNUSED_RESULT;
MkString       *mk_string_path_basename	    (MkString *self)
					    G_GNUC_WARN_UNUSED_RESULT;
gboolean	mk_string_has_suffix	    (MkString *self,
					     const gchar *suffix);
gboolean	mk_string_has_prefix	    (MkString *self,
					     const gchar *prefix);
guint		mk_string_hash		    (gconstpointer self);
gboolean	mk_string_equal		    (MkString *self,
					     MkString *other);
gchar		mk_string_char_at	    (MkString *self,
					     glong index);
gboolean	mk_set_string		    (MkString **location,
					     MkString *string);
void		mk_clear_string		    (MkString **location);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (MkString, mk_string_unref);

G_END_DECLS
