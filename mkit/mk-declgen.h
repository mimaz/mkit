#pragma once

#include "mk-string.h"

G_BEGIN_DECLS

typedef struct _MkDeclNamespace MkDeclNamespace;

#define MK_TYPE_DECL_CODEGEN (mk_declgen_get_type ())

G_DECLARE_DERIVABLE_TYPE (MkDeclgen, mk_declgen,
			  MK, DECL_CODEGEN, GObject);

struct _MkDeclgenClass
{
    GObjectClass parent_class;
};

MkDeclgen	   *mk_declgen_new	    (void);
void		    mk_declgen_generate	    (MkDeclgen *self,
					     MkDeclNamespace *namespace,
					     GError **error);
MkString	   *mk_declgen_get_header   (MkDeclgen *self);
MkString	   *mk_declgen_get_source   (MkDeclgen *self);

G_END_DECLS
