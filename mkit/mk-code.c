#include "mk-code.h"
#include "mk-array.h"
#include "mk-error.h"

#include <gio/gio.h>

typedef struct {
    GHashTable *code_table;
    GHashTable *used_table;
    MkString *bound;
} MkCodePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (MkCode, mk_code,
			    G_TYPE_OBJECT);

static void
finalize (GObject *gobj)
{
    MkCode *self = MK_CODE (gobj);
    MkCodePrivate *p = mk_code_get_instance_private (self);

    g_hash_table_unref (p->code_table);
    g_hash_table_unref (p->used_table);
    mk_clear_string (&p->bound);

    G_OBJECT_CLASS (mk_code_parent_class)->finalize (gobj);
}

static void
mk_code_init (MkCode *self)
{
    MkCodePrivate *p = mk_code_get_instance_private (self);

    p->code_table = g_hash_table_new_full (g_str_hash,
					   g_str_equal,
					   (GDestroyNotify)
					   mk_string_unref,
					   (GDestroyNotify)
					   mk_array_unref);
    p->used_table = g_hash_table_new (g_str_hash, g_str_equal);
}

static void
mk_code_class_init (MkCodeClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->finalize = finalize;
}

static MkString **
get_string (MkCode *self, const gchar *id)
{
    MkCodePrivate *p = mk_code_get_instance_private (self);
    MkArray *array;

    if (!id) {
	id = p->bound;
    }

    array = g_hash_table_lookup (p->code_table, id);

    if (!array) {
	array = mk_array_new_type (MK_TYPE_STRING);
	array = mk_array_append (array, mk_string_new (""));

	mk_string_unref (mk_array_get (array, 0));

	g_hash_table_insert (p->code_table, mk_string_new (id), array);
    }

    return mk_array_at (array, 0);
}

MkCode *
mk_code_new ()
{
    return g_object_new (MK_TYPE_CODE, NULL);
}

void
mk_code_put (MkCode *self, const gchar *id, const gchar *fmt, ...)
{
    va_list args;
    MkString **s = get_string (self, id);

    va_start (args, fmt);
    *s = mk_string_append_v (*s, fmt, args);
    va_end (args);
}

/**
 * mk_code_put_s: (rename-to mk_code_put)
 */
void
mk_code_put_s (MkCode *self, const gchar *id, const gchar *text)
{
    MkString **s = get_string (self, id);

    *s = mk_string_append_s (*s, text);
}

/**
 * mk_code_use:
 *
 * returns: (transfer none):
 */
MkString *
mk_code_use (MkCode *self, const gchar *id)
{
    MkCodePrivate *p = mk_code_get_instance_private (self);
    MkString *key, *value;
    gboolean ok;

    value = *get_string (self, id);

    if (!g_hash_table_lookup (p->used_table, id)) {
	ok = g_hash_table_lookup_extended (p->code_table, id,
					   (gpointer *) &key,
					   NULL);
	g_assert (ok);

	g_hash_table_add (p->used_table, key);
    }

    return value;
}

gboolean
mk_code_has (MkCode *self, const gchar *id)
{
    MkCodePrivate *p = mk_code_get_instance_private (self);
    MkString **code = g_hash_table_lookup (p->code_table, id);

    return !!code;
}

void
mk_code_bind (MkCode *self, const gchar *id)
{
    MkCodePrivate *p = mk_code_get_instance_private (self);

    p->bound = mk_string_assign (p->bound, id);
}

void
mk_code_emit (MkCode *self, const gchar *fmt, ...)
{
    MkString **s = get_string (self, NULL);
    va_list args;

    va_start (args, fmt);
    *s = mk_string_append_v (*s, fmt, args);
    va_end (args);
}

/**
 * mk_code_emit_s: (rename-to mk_code_emit)
 */
void
mk_code_emit_s (MkCode *self, const gchar *text)
{
    MkString **s = get_string (self, NULL);

    *s = mk_string_append_s (*s, text);
}

void
mk_code_clear (MkCode *self, const gchar *id)
{
    MkCodePrivate *p = mk_code_get_instance_private (self);

    g_hash_table_remove (p->code_table, id);
    g_hash_table_remove (p->used_table, id);
}

void
mk_code_clear_prefix (MkCode *self, const gchar *prefix)
{
    MkCodePrivate *p = mk_code_get_instance_private (self);
    GHashTableIter iter;
    MkString *key;

    g_hash_table_iter_init (&iter, p->code_table);

    while (g_hash_table_iter_next (&iter, (gpointer *) &key, NULL)) {
	if (g_str_has_prefix (key, prefix)) {
	    g_hash_table_remove (p->used_table, key);
	    g_hash_table_iter_remove (&iter);
	}
    }
}

static guint
check_tag (gchar *s, const gchar *t, GSList **pos)
{
    gchar *f;
    guint c;

    c = 0;

    while ((f = strstr (s, t))) {
	memmove (f, f + strlen (t), strlen (f + strlen (t)) + 1);
	c++;

	if (pos) {
	    *pos = g_slist_prepend (*pos, GUINT_TO_POINTER (f - s));
	}
    }

    return c;
}

/**
 * mk_code_use_format:
 * @out: (inout):
 */
void
mk_code_use_format (MkCode *self, const gchar *id,
		    MkString **out, GError **error)
{
    MkString *text = mk_code_use (self, id);

    if (!text) {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "no such key: %s", id);
	return;
    }

    mk_code_format (text, out, error);
}

/**
 * mk_code_patch:
 * @out: (inout):
 */
void
mk_code_patch (MkCode *self, MkString **out,
	       const gchar *mark, const gchar *template,
	       gssize len, GError **error)
{
    g_autoptr (MkString) slice = NULL;
    g_autoptr (MkString) patched = NULL;
    GError *local_error = NULL;
    const gchar *it, *sep;

    patched = mk_string_new (NULL);

    if (len < 0) {
	len = strlen (template);
    }

    it = strstr (template, mark);

    if (it) {
	patched = mk_string_assign_len (patched, template, it - template);
    } else {
	*out = mk_string_assign (*out, template);
	return;
    }

    while (it) {
	it += strlen (mark);
	sep = strstr (it, mark);

	if (!sep) {
	    patched = mk_string_append_s (patched, it);
	    patched = mk_string_append_s (patched, mark);
	    break;
	}

	slice = mk_string_assign_len (slice, it, sep - it);

	if (mk_code_has (self, slice)) {
	    mk_set_string (&slice, mk_code_use (self, slice));
	    /*mk_code_use_format (self, slice, &slice, &local_error);*/

	    if (local_error) {
		g_propagate_error (error, local_error);
		return;
	    }

	    patched = mk_string_append_s (patched, slice);

	    it = sep + strlen (mark);
	    sep = strstr (it, mark);

	    if (sep) {
		slice = mk_string_assign_len (slice, it, sep - it);
		patched = mk_string_append_s (patched, slice);

		it = sep;
	    } else {
		patched = mk_string_append_s (patched, it);
		break;
	    }
	} else {
	    it = sep;

	    patched = mk_string_append_s (patched, mark);
	    patched = mk_string_append_s (patched, slice);
	}
    }

    mk_code_format (patched, out, error);
}

/**
 * mk_code_patch_file:
 * @out: (inout):
 */
void
mk_code_patch_file (MkCode *self, MkString **out,
		    const gchar *mark, const gchar *path, GError **error)
{
    g_autofree gchar *contents = NULL;
    GError *local_error = NULL;
    gsize size;

    g_file_get_contents (path, &contents, &size, &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }

    mk_code_patch (self, out, mark, contents, size, error);
}

void
mk_code_check_usage (MkCode *self, GError **error)
{
    MkCodePrivate *p = mk_code_get_instance_private (self);
    g_autoptr (MkString) unused = NULL;
    GHashTableIter iter;
    MkString *key;

    g_hash_table_iter_init (&iter, p->code_table);

    while (g_hash_table_iter_next (&iter, (gpointer *) &key, NULL)) {
	if (!g_hash_table_lookup (p->used_table, key)) {
	    if (!unused) {
		unused = mk_string_append (unused,
					   "unused code ids:%s", key);
	    } else {
		unused = mk_string_append (unused,
					   ",%s", key);
	    }
	}
    }

    if (unused) {
	g_set_error (error, MK_ERROR, MK_ERROR_UNUSED,
		     "%s", unused);
    }
}

void
mk_code_format (MkString *text, MkString **out, GError **error)
{
    g_autoptr (MkString) root = NULL;
    g_autoptr (MkString) tab = NULL;
    g_autoptr (GSList) align_pos = NULL;
    gchar *line, *nl, *endl;
    guint indent, align_count, shift, i;

    root = mk_string_make_mutable (mk_string_ref (text), -1);
    line = root;
    indent = 0;

    *out = mk_string_assign (*out, "");

    while (*line) {
	nl = strchr (line, '\n');

	if (nl) {
	    endl = nl;
	    *endl = 0;
	} else {
	    endl = line + strlen (line);
	}

	g_assert (endl && !*endl);

	indent += check_tag (line, "<indent>", NULL);
	indent -= check_tag (line, "</indent>", NULL);

	shift = indent * 4 + (align_pos ? GPOINTER_TO_UINT (align_pos->data) : 0);

	check_tag (line, "<align>", &align_pos);
	align_count = check_tag (line, "</align>", NULL);

	for (i = 0; i < align_count; i++) {
	    if (align_pos) {
		align_pos = g_slist_remove_link (align_pos, align_pos);
	    } else {
		g_warning ("bad <align> - </align> pair");
	    }
	}

	if (shift) {
	    tab = mk_string_repeat_c (tab, ' ', shift);
	    *out = mk_string_append_s (*out, tab);
	}

	*out = mk_string_append_s (*out, line);
	*out = mk_string_append_c (*out, '\n');

	if (nl) {
	    line = nl + 1;
	} else {
	    break;
	}
    }
}
