#include "mk-xml-parser.h"
#include "mk-string.h"
#include "mk-array.h"

#include <gio/gio.h>

struct _MkXmlParser
{
    MkParser parent_instance;
    GMarkupParseContext *context;
    gchar *text;
    gsize textlen;

    MkParserTraverse traverse;
    gpointer user_data;
};

G_DEFINE_TYPE (MkXmlParser, mk_xml_parser, MK_TYPE_PARSER);

static void
xml_start_element (GMarkupParseContext *parser,
		   const gchar *element_name,
		   const gchar **attribute_names,
		   const gchar **attribute_values,
		   gpointer user_data,
		   GError **error)
{
    g_autoptr (MkString) name = NULL;
    g_auto (GValue) value = G_VALUE_INIT;
    MkXmlParser *self = MK_XML_PARSER (user_data);
    GError *local_error = NULL;
    guint num_attrs, i;

    num_attrs = g_strv_length ((GStrv) attribute_names);
    g_assert (num_attrs == g_strv_length ((GStrv) attribute_values));

    self->traverse (MK_PARSER_BEGIN_OBJECT, mk_string_newa (element_name),
		    NULL, self->user_data, &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }

    g_value_init (&value, MK_TYPE_STRING);

    for (i = 0; i < num_attrs; i++) {
	name = mk_string_assign (name, attribute_names[i]);

	g_value_take_boxed (&value,
			    mk_string_new_shared (attribute_values[i]));

	self->traverse (MK_PARSER_VALUE, name, &value,
			self->user_data, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}
    }
}

static void
xml_end_element (GMarkupParseContext *parser,
		 const gchar *element_name,
		 gpointer user_data,
		 GError **error)
{
    MkXmlParser *self = MK_XML_PARSER (user_data);

    self->traverse (MK_PARSER_END_OBJECT, mk_string_newa (element_name),
		    NULL, self->user_data, error);
}

static const GMarkupParser xml_parser = {
    .start_element = xml_start_element,
    .end_element = xml_end_element,
};

static void
load_text (MkParser *parser, const gchar *text, gssize len, GError **error)
{
    MkXmlParser *self = MK_XML_PARSER (parser);

    g_free (self->text);

    if (len < 0) {
	len = strlen (text);
    }

    self->text = g_malloc (len + 1);
    memcpy (self->text, text, self->textlen = len);
    self->text[len] = 0;
}

static void
load_file (MkParser *parser, const gchar *path, GError **error)
{
    g_autoptr (GFile) file = NULL;
    MkXmlParser *self = MK_XML_PARSER (parser);

    g_clear_pointer (&self->text, g_free);

    file = g_file_new_for_path (path);

    g_file_load_contents (file, NULL, &self->text, &self->textlen,
			  NULL, error);
}

static void
traverse (MkParser *parser, MkParserTraverse traverse,
	  gpointer user_data, GError **error)
{
    MkXmlParser *self = MK_XML_PARSER (parser);

    self->traverse = traverse;
    self->user_data = user_data;

    g_markup_parse_context_parse (self->context, self->text,
				  self->textlen, error);
}

static void
finalize (GObject *gobj)
{
    MkXmlParser *self = MK_XML_PARSER (gobj);

    g_markup_parse_context_free (self->context);
    g_free (self->text);

    G_OBJECT_CLASS (mk_xml_parser_parent_class)->finalize (gobj);
}

static void
mk_xml_parser_init (MkXmlParser *self)
{
    self->context = g_markup_parse_context_new (&xml_parser, 0,
						self, NULL);
}

static void
mk_xml_parser_class_init (MkXmlParserClass *cls)
{
    MkParserClass *pcls = MK_PARSER_CLASS (cls);
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    pcls->load_text = load_text;
    pcls->load_file = load_file;
    pcls->traverse = traverse;
    gcls->finalize = finalize;
}

MkParser *
mk_xml_parser_new ()
{
    return g_object_new (MK_TYPE_XML_PARSER, NULL);
}
