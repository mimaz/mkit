#include <mkit.h>
#ifndef MKIT_HAS_DECL
#include <mk-decl0.h>
#endif

static void
command_declgen (const gchar *descpath, const gchar *headerpath,
		 const gchar *sourcepath, GError **error)
{
    g_autoptr (MkDeclgen) declgen = NULL;
    g_autoptr (MkBuilder) builder = NULL;
    g_autoptr (MkParser) parser = NULL;
    g_autoptr (MkDeclNamespace) module = NULL;
    g_autoptr (MkString) header = NULL;
    g_autoptr (MkString) source = NULL;
    GError *local_error = NULL;
    
    if (descpath == NULL) {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "no descfile given\n");
	return;
    }

    parser = mk_json_parser_new ();

    mk_parser_load_file (parser, descpath, &local_error);

    if (local_error) {
	goto finalize;
    }

    mk_decl_init_types ();

    builder = mk_builder_new ();
    module = mk_builder_build (builder, parser,
			       MK_TYPE_DECL_NAMESPACE, &local_error);

    if (local_error) {
	goto finalize;
    }

    declgen = mk_declgen_new ();

    mk_declgen_generate (declgen, module, &local_error);

    if (local_error) {
	goto finalize;
    }

    header = mk_declgen_get_header (declgen);
    source = mk_declgen_get_source (declgen);

    if (headerpath) {
	g_file_set_contents (headerpath, header,
			     mk_string_len (header),
			     &local_error);

	if (local_error) {
	    goto finalize;
	}
    }

    if (sourcepath) {
	g_file_set_contents (sourcepath, source,
			     mk_string_len (source),
			     &local_error);

	if (local_error) {
	    goto finalize;
	}
    }

finalize:
    if (local_error) {
	g_propagate_error (error, local_error);
    }
}

gint
main (gint argc, gchar **argv)
{
    g_autoptr (GOptionContext) oc = NULL;
    g_autoptr (GError) error = NULL;
    g_autofree gchar *descpath = NULL;
    g_autofree gchar *headerpath = NULL;
    g_autofree gchar *sourcepath = NULL;
    g_auto (GStrv) paths = NULL;
    g_auto (GStrv) girs = NULL;

    mk_init_types ();

    GOptionEntry entries[] = {
	{ "desc", 'd', 0, G_OPTION_ARG_FILENAME, &descpath,
	    "description file", NULL },
	{ "header", 'h', 0, G_OPTION_ARG_FILENAME, &headerpath,
	    "header file", NULL },
	{ "source", 's', 0, G_OPTION_ARG_FILENAME, &sourcepath,
	    "source file", NULL },
	{ "path", 'p', 0, G_OPTION_ARG_FILENAME_ARRAY, &paths,
	    "gir paths", NULL},
	{ NULL },
    };

    oc = g_option_context_new (NULL);

    g_option_context_set_help_enabled (oc, TRUE);
    g_option_context_add_main_entries (oc, entries, NULL);
    g_option_context_parse (oc, &argc, &argv, &error);

    if (argc < 2) {
	g_print ("no command\n");
	return -1;
    }

    if (g_strcmp0 (argv[1], "declgen") == 0) {
	command_declgen (descpath, headerpath, sourcepath, &error);
	/*g_error ("declgen");*/
    } else {
	g_print ("unknown command: %s", argv[1]);
    }

    if (error) {
	g_error ("%s\n", error->message);
	return 1;
    }

    return 0;
}
