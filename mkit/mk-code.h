#pragma once

#include "mk-string.h"

G_BEGIN_DECLS

#define MK_TYPE_CODE (mk_code_get_type ())

G_DECLARE_DERIVABLE_TYPE (MkCode, mk_code,
			  MK, CODE, GObject);

struct _MkCodeClass
{
    GObjectClass parent_class;
};

MkCode	       *mk_code_new		(void);
void		mk_code_put		(MkCode *self,
					 const gchar *id,
					 const gchar *fmt,
					 ...);
void		mk_code_put_s		(MkCode *self,
					 const gchar *id,
					 const gchar *text);
void		mk_code_push		(MkCode *self,
					 const gchar *id);
MkString       *mk_code_use		(MkCode *self,
					 const gchar *id);
gboolean	mk_code_has		(MkCode *self,
					 const gchar *id);
void		mk_code_bind		(MkCode *self,
					 const gchar *id);
void		mk_code_emit		(MkCode *self,
					 const gchar *fmt,
					 ...);
void		mk_code_emit_s		(MkCode *self,
					 const gchar *text);
void		mk_code_clear		(MkCode *self,
					 const gchar *id);
void		mk_code_clear_prefix	(MkCode *self,
					 const gchar *prefix);
void		mk_code_use_format	(MkCode *self,
					 const gchar *id,
					 MkString **out,
					 GError **error);
void		mk_code_patch		(MkCode *self,
					 MkString **out,
					 const gchar *mark,
					 const gchar *template,
					 gssize len,
					 GError **error);
void		mk_code_patch_file	(MkCode *self,
					 MkString **out,
					 const gchar *mark,
					 const gchar *path,
					 GError **error);
void		mk_code_check_usage	(MkCode *self,
					 GError **error);
void		mk_code_format		(MkString *text,
					 MkString **out,
					 GError **error);

G_END_DECLS
