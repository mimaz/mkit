#pragma once

#include <mk-enum-gtype.h>

G_BEGIN_DECLS

typedef enum {
    MK_DIRECTION_IN,
    MK_DIRECTION_OUT,
    MK_DIRECTION_INOUT,
} MkGiDirection;

G_END_DECLS
