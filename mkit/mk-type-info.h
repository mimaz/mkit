#pragma once

#include "mk-string.h"
#include "mk-array.h"
#include "mk-type-tag.h"

G_BEGIN_DECLS

typedef struct _MkSpaceInfo MkSpaceInfo;

#define MK_TYPE_TYPE_INFO (mk_type_info_get_type ())

G_DECLARE_FINAL_TYPE (MkTypeInfo, mk_type_info,
		      MK, TYPE_INFO, GObject);

MkTypeInfo     *mk_type_info_new		    (MkSpaceInfo *space,
						     MkTypeInfo *parent,
						     MkTypeTag tag,
						     MkString *name,
						     MkString *c_name,
						     MkString *c_symbol,
						     MkString *c_macro,
						     MkString *glib_name,
						     MkString *glib_get_type,
						     MkString *glib_type_macro);
MkSpaceInfo    *mk_type_info_get_ns		    (MkTypeInfo *self);
MkTypeInfo     *mk_type_info_get_parent		    (MkTypeInfo *self);
MkTypeTag	mk_type_info_get_tag		    (MkTypeInfo *self);
MkString       *mk_type_info_get_name		    (MkTypeInfo *self);
MkString       *mk_type_info_get_c_name		    (MkTypeInfo *self);
MkString       *mk_type_info_get_c_symbol	    (MkTypeInfo *self);
MkString       *mk_type_info_get_c_macro	    (MkTypeInfo *self);
MkString       *mk_type_info_get_glib_name	    (MkTypeInfo *self);
MkString       *mk_type_info_get_glib_get_type	    (MkTypeInfo *self);
MkString       *mk_type_info_get_glib_type_macro    (MkTypeInfo *self);
MkString       *mk_type_info_get_ref_symbol	    (MkTypeInfo *self);
MkString       *mk_type_info_get_unref_symbol	    (MkTypeInfo *self);
gboolean	mk_type_info_is_value		    (MkTypeInfo *self);
gboolean	mk_type_info_is_pointer		    (MkTypeInfo *self);
gboolean	mk_type_info_is_utf8		    (MkTypeInfo *self);
MkString       *mk_type_info_c_arg		    (MkTypeInfo *self,
						     MkArray **strings,
						     MkString **identifier,
						     const gchar *arg_fmt,
						     ...);
MkString       *mk_type_info_split		    (MkTypeInfo *self,
						     MkArray **strings,
						     const gchar *prop,
						     const gchar *fmt);
GType		mk_type_info_load		    (MkTypeInfo *self,
						     GError **error);

G_END_DECLS
