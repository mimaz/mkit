#include "mk-ns-info.h"
#include "mk-type-info.h"
#include "mk-error.h"

#include <ctype.h>

struct _MkSpaceInfo
{
    GObject parent_instance;
    MkString *name;
    MkString *c_name;
    MkString *c_symbol;
    MkString *c_macro;

    GHashTable *info_table;
};

G_DEFINE_TYPE (MkSpaceInfo, mk_ns_info, G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_NAME,
    PROP_C_NAME,
    PROP_C_SYMBOL,
    PROP_C_MACRO,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject *gobj, guint id, const GValue *value, GParamSpec *spec)
{
    G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
}

static void
get_property (GObject *gobj, guint id, GValue *value, GParamSpec *spec)
{
    MkSpaceInfo *self = MK_SPACE_INFO (gobj);

    switch (id) {
    case PROP_NAME:
	g_value_set_boxed (value, mk_ns_info_get_name (self));
	break;

    case PROP_C_NAME:
	g_value_set_boxed (value, mk_ns_info_get_c_name (self));
	break;

    case PROP_C_SYMBOL:
	g_value_set_boxed (value, mk_ns_info_get_c_symbol (self));
	break;

    case PROP_C_MACRO:
	g_value_set_boxed (value, mk_ns_info_get_c_macro (self));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
finalize (GObject *gobj)
{
    MkSpaceInfo *self = MK_SPACE_INFO (gobj);

    mk_clear_string (&self->name);
    mk_clear_string (&self->c_name);
    mk_clear_string (&self->c_symbol);
    mk_clear_string (&self->c_macro);

    G_OBJECT_CLASS (mk_ns_info_parent_class)->finalize (gobj);
}

static void
dispose (GObject *gobj)
{
    MkSpaceInfo *self = MK_SPACE_INFO (gobj);

    g_clear_pointer (&self->info_table, g_hash_table_unref);

    G_OBJECT_CLASS (mk_ns_info_parent_class)->dispose (gobj);
}

static void
mk_ns_info_init (MkSpaceInfo *self)
{
    self->info_table = g_hash_table_new_full (g_str_hash, g_str_equal,
					      (GDestroyNotify)
					      mk_string_unref,
					      g_object_unref);
}

static void
mk_ns_info_class_init (MkSpaceInfoClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->finalize = finalize;
    gcls->dispose = dispose;

    props[PROP_NAME] = g_param_spec_boxed ("name", "name", "name",
					   MK_TYPE_STRING,
					   G_PARAM_READABLE |
					   G_PARAM_STATIC_STRINGS);

    props[PROP_C_NAME] = g_param_spec_boxed ("c-name", "c-name", "c-name",
					     MK_TYPE_STRING,
					     G_PARAM_READABLE |
					     G_PARAM_STATIC_STRINGS);

    props[PROP_C_SYMBOL] = g_param_spec_boxed ("c-symbol", "c-symbol", "c-symbol",
					       MK_TYPE_STRING,
					       G_PARAM_READABLE |
					       G_PARAM_STATIC_STRINGS);

    props[PROP_C_MACRO] = g_param_spec_boxed ("c-macro", "c-macro", "c-macro",
					      MK_TYPE_STRING,
					      G_PARAM_READABLE |
					      G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

MkSpaceInfo *
mk_ns_info_new (MkString *name, MkString *c_name,
		MkString *c_symbol, MkString *c_macro)
{
    MkSpaceInfo *self = g_object_new (MK_TYPE_SPACE_INFO, NULL);

    self->name = mk_string_ref (name);
    self->c_name = mk_string_ref (c_name);
    self->c_symbol = mk_string_ref (c_symbol);
    self->c_macro = mk_string_ref (c_macro);

    return self;
}

MkString *
mk_ns_info_get_name (MkSpaceInfo *self)
{
    return self->name;
}

MkString *
mk_ns_info_get_c_name (MkSpaceInfo *self)
{
    return self->c_name;
}

MkString *
mk_ns_info_get_c_symbol (MkSpaceInfo *self)
{
    return self->c_symbol;
}

MkString *
mk_ns_info_get_c_macro (MkSpaceInfo *self)
{
    return self->c_macro;
}
