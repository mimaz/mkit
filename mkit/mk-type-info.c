#include "mk-type-info.h"
#include "mk-ns-info.h"
#include "mk-error.h"

#include <ctype.h>

struct _MkTypeInfo
{
    GObject parent_instance;

    MkSpaceInfo *space;
    MkTypeInfo *parent;
    MkTypeTag tag;
    MkString *name;
    MkString *c_name;
    MkString *c_symbol;
    MkString *c_macro;
    MkString *glib_name;
    MkString *glib_get_type;
    MkString *glib_type_macro;

    MkString *ref_symbol;
    MkString *unref_symbol;

    MkString *c_name_ptr;

    gboolean checked_utf8;
    gboolean is_utf8;
};

G_DEFINE_TYPE (MkTypeInfo, mk_type_info, G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_PARENT,
    PROP_TAG,
    PROP_NAME,
    PROP_C_NAME,
    PROP_C_SYMBOL,
    PROP_C_MACRO,
    PROP_GLIB_NAME,
    PROP_GLIB_GET_TYPE,
    PROP_GLIB_TYPE_MACRO,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject *gobj, guint id, const GValue *value, GParamSpec *spec)
{
    G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
}

static void
get_property (GObject *gobj, guint id, GValue *value, GParamSpec *spec)
{
    MkTypeInfo *self = MK_TYPE_INFO (gobj);

    switch (id) {
    case PROP_PARENT:
	g_value_set_object (value, mk_type_info_get_parent (self));
	break;

    case PROP_TAG:
	g_value_set_enum (value, mk_type_info_get_tag (self));
	break;

    case PROP_NAME:
	g_value_set_boxed (value, mk_type_info_get_name (self));
	break;

    case PROP_C_NAME:
	g_value_set_boxed (value, mk_type_info_get_c_name (self));
	break;

    case PROP_C_SYMBOL:
	g_value_set_boxed (value, mk_type_info_get_c_symbol (self));
	break;

    case PROP_C_MACRO:
	g_value_set_boxed (value, mk_type_info_get_c_macro (self));
	break;

    case PROP_GLIB_NAME:
	g_value_set_boxed (value, mk_type_info_get_glib_name (self));
	break;

    case PROP_GLIB_GET_TYPE:
	g_value_set_boxed (value, mk_type_info_get_glib_get_type (self));
	break;

    case PROP_GLIB_TYPE_MACRO:
	g_value_set_boxed (value, mk_type_info_get_glib_type_macro (self));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
finalize (GObject *gobj)
{
    MkTypeInfo *self = MK_TYPE_INFO (gobj);

    g_clear_weak_pointer (&self->space);
    g_clear_object (&self->parent);

    mk_clear_string (&self->name);
    mk_clear_string (&self->c_name);
    mk_clear_string (&self->c_symbol);
    mk_clear_string (&self->c_macro);
    mk_clear_string (&self->glib_name);
    mk_clear_string (&self->glib_get_type);
    mk_clear_string (&self->glib_type_macro);

    mk_clear_string (&self->ref_symbol);
    mk_clear_string (&self->unref_symbol);

    mk_clear_string (&self->c_name_ptr);

    G_OBJECT_CLASS (mk_type_info_parent_class)->finalize (gobj);
}

static void
mk_type_info_init (MkTypeInfo *self)
{

}

static void
mk_type_info_class_init (MkTypeInfoClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->finalize = finalize;

    props[PROP_PARENT] = g_param_spec_object ("parent", "parent", "parent",
					      MK_TYPE_TYPE_INFO,
					      G_PARAM_READABLE |
					      G_PARAM_STATIC_STRINGS);

    props[PROP_TAG] = g_param_spec_enum ("tag", "tag", "tag",
					 MK_TYPE_TYPE_TAG,
					 MK_TYPE_TAG_VOID,
					 G_PARAM_READABLE |
					 G_PARAM_STATIC_STRINGS);

    props[PROP_NAME] = g_param_spec_boxed ("name", "name", "name",
					   MK_TYPE_STRING,
					   G_PARAM_READABLE |
					   G_PARAM_STATIC_STRINGS);

    props[PROP_C_NAME] = g_param_spec_boxed ("c-name", "c-name", "c-name",
					     MK_TYPE_STRING,
					     G_PARAM_READABLE |
					     G_PARAM_STATIC_STRINGS);

    props[PROP_C_SYMBOL] = g_param_spec_boxed ("c-symbol", "c-symbol", "c-symbol",
					       MK_TYPE_STRING,
					       G_PARAM_READABLE |
					       G_PARAM_STATIC_STRINGS);

    props[PROP_C_MACRO] = g_param_spec_boxed ("c-macro", "c-macro", "c-macro",
					      MK_TYPE_STRING,
					      G_PARAM_READABLE |
					      G_PARAM_STATIC_STRINGS);

    props[PROP_GLIB_NAME] = g_param_spec_boxed ("glib-name",
						"glib-name",
						"glib-name",
						MK_TYPE_STRING,
						G_PARAM_READABLE |
						G_PARAM_STATIC_STRINGS);

    props[PROP_GLIB_GET_TYPE] = g_param_spec_boxed ("glib-get-type",
						    "glib-get-type",
						    "glib-get-type",
						    MK_TYPE_STRING,
						    G_PARAM_READABLE |
						    G_PARAM_STATIC_STRINGS);

    props[PROP_GLIB_TYPE_MACRO] = g_param_spec_boxed ("glib-type-macro",
						      "glib-type-macro",
						      "glib-type-macro",
						      MK_TYPE_STRING,
						      G_PARAM_READABLE |
						      G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

MkTypeInfo *
mk_type_info_new (MkSpaceInfo *space,
		  MkTypeInfo *parent,
		  MkTypeTag tag,
		  MkString *name,
		  MkString *c_name,
		  MkString *c_symbol,
		  MkString *c_macro,
		  MkString *glib_name,
		  MkString *glib_get_type,
		  MkString *glib_type_macro)
{
    MkTypeInfo *self = g_object_new (MK_TYPE_TYPE_INFO, NULL);

    g_set_weak_pointer (&self->space, space);
    g_set_object (&self->parent, parent);

    self->tag = tag;
    self->name = mk_string_ref (name);
    self->c_name = mk_string_ref (c_name);
    self->c_symbol = mk_string_ref (c_symbol);
    self->c_macro = mk_string_ref (c_macro);
    self->glib_name = mk_string_ref (glib_name);
    self->glib_get_type = mk_string_ref (glib_get_type);
    self->glib_type_macro = mk_string_ref (glib_type_macro);

    return self;
}

/**
 * mk_type_info_get_ns:
 *
 * returns: (transfer none):
 */
MkSpaceInfo *
    mk_type_info_get_ns (MkTypeInfo *self)
{
    return self->space;
}

/**
 * mk_type_info_get_parent:
 *
 * returns: (transfer none):
 */
MkTypeInfo *
    mk_type_info_get_parent (MkTypeInfo *self)
{
    return self->parent;
}

MkTypeTag
mk_type_info_get_tag (MkTypeInfo *self)
{
    return self->tag;
}

/**
 * mk_type_info_get_name:
 *
 * returns: (transfer none):
 */
MkString *
    mk_type_info_get_name (MkTypeInfo *self)
{
    return self->name;
}

/**
 * mk_type_info_get_c_name:
 *
 * returns: (transfer none):
 */
MkString *
    mk_type_info_get_c_name (MkTypeInfo *self)
{
    return self->c_name;
}

/**
 * mk_type_info_get_c_symbol:
 *
 * returns: (transfer none):
 */
MkString *
    mk_type_info_get_c_symbol (MkTypeInfo *self)
{
    return self->c_symbol;
}

/**
 * mk_type_info_get_c_macro:
 *
 * returns: (transfer none):
 */
MkString *
    mk_type_info_get_c_macro (MkTypeInfo *self)
{
    return self->c_macro;
}

/**
 * mk_type_info_get_glib_name:
 *
 * returns: (transfer none):
 */
MkString *
    mk_type_info_get_glib_name (MkTypeInfo *self)
{
    return self->glib_name;
}

/**
 * mk_type_info_get_glib_get_type:
 *
 * returns: (transfer none):
 */
MkString *
    mk_type_info_get_glib_get_type (MkTypeInfo *self)
{
    return self->glib_get_type;
}

/**
 * mk_type_info_get_glib_type_macro:
 *
 * returns: (transfer none):
 */
MkString *
    mk_type_info_get_glib_type_macro (MkTypeInfo *self)
{
    return self->glib_type_macro;
}

MkString *
mk_type_info_get_ref_symbol (MkTypeInfo *self)
{
    if (!self->ref_symbol) {
	if (self->tag == MK_TYPE_TAG_OBJECT) {
	    self->ref_symbol = mk_string_new_shared ("g_object_ref");
	} else {
	    self->ref_symbol = mk_string_append (self->ref_symbol, "%s_ref",
						 mk_type_info_get_c_symbol (self));
	}
    }

    return self->ref_symbol;
}

MkString *
mk_type_info_get_unref_symbol (MkTypeInfo *self)
{
    if (!self->unref_symbol) {
	if (self->tag == MK_TYPE_TAG_OBJECT) {
	    self->unref_symbol = mk_string_new_shared ("g_object_unref");
	} else {
	    self->unref_symbol = mk_string_append (self->unref_symbol, "%s_unref",
						   mk_type_info_get_c_symbol (self));
	}
    }

    return self->unref_symbol;
}

gboolean
mk_type_info_is_value (MkTypeInfo *self)
{
    return mk_type_tag_is_value (mk_type_info_get_tag (self));
}

gboolean
mk_type_info_is_pointer (MkTypeInfo *self)
{
    return mk_type_tag_is_pointer (mk_type_info_get_tag (self));
}

gboolean
mk_type_info_is_utf8 (MkTypeInfo *self)
{
    if (!self->checked_utf8) {
	self->checked_utf8 = TRUE;

	self->is_utf8 = self->space &&
	    g_strcmp0 (self->name, "String") == 0 &&
	    g_strcmp0 (mk_ns_info_get_c_name (self->space), "Mk") == 0;
    }

    return self->is_utf8;
}

static gboolean
is_keyword (const gchar *s)
{
    static const gchar *keywords[] = {
	"default",
	"extern",
    };
    guint i;

    for (i = 0; i < G_N_ELEMENTS (keywords); i++) {
	if (g_strcmp0 (keywords[i], s) == 0) {
	    return TRUE;
	}
    }

    return FALSE;
}

/**
 * mk_type_info_c_arg:
 * @strings: (inout)
 *
 * returns: (transfer none)
 */
MkString *
mk_type_info_c_arg (MkTypeInfo *self, MkArray **strings,
		    MkString **identifier, const gchar *arg_fmt, ...)
{
    g_autoptr (MkString) string = NULL;
    va_list args;
    gchar *arg;
    guint arg_len;

    if (arg_fmt) {
	va_start (args, arg_fmt);
	arg_len = g_vsnprintf (NULL, 0, arg_fmt, args);
	va_end (args);

	arg = g_alloca (arg_len + 1);

	va_start (args, arg_fmt);
	g_vsnprintf (arg, arg_len + 1, arg_fmt, args);
	va_end (args);
    } else {
	arg = NULL;
    }

    string = mk_string_ref (mk_type_info_get_c_name (self));

    if (mk_type_info_is_pointer (self)) {
	string = mk_string_append_s (string, " *");
    } else {
	string = mk_string_append_c (string, ' ');
    }

    if (arg) {
	string = mk_string_append_s (string, arg);
    }

    if (is_keyword (arg)) {
	string = mk_string_append_c (string, 'x');
    }

    if (!*strings) {
	*strings = mk_array_new_type (MK_TYPE_STRING);
    }

    *strings = mk_array_append (*strings, string);

    if (identifier && arg) {
	*identifier = mk_string_assign (*identifier, arg);

	if (is_keyword (arg)) {
	    *identifier = mk_string_append_c (*identifier, 'x');
	}
    }

    return string;
}

MkString *
mk_type_info_split (MkTypeInfo *self, MkArray **strings,
		    const gchar *prop, const gchar *fmt)
{
    g_auto (GValue) space_value = G_VALUE_INIT;
    g_auto (GValue) info_value = G_VALUE_INIT;
    g_autoptr (MkString) string = NULL;
    const gchar *space_str, *info_str;

    g_return_val_if_fail (self->space, NULL);
    g_object_get_property (G_OBJECT (self->space), prop, &space_value);
    g_object_get_property (G_OBJECT (self), prop, &info_value);

    g_return_val_if_fail (G_VALUE_HOLDS (&space_value, MK_TYPE_STRING), NULL);
    g_return_val_if_fail (G_VALUE_HOLDS (&info_value, MK_TYPE_STRING), NULL);

    space_str = g_value_get_boxed (&space_value);
    info_str = g_value_get_boxed (&info_value);

    if (!space_str) {
	space_str = "";
    }

    if (!info_str) {
	info_str = "";
    }

    g_return_val_if_fail (strlen (info_str) >= strlen (space_str), NULL);

    info_str += strlen (space_str);

    if (!isalpha (*info_str)) {
	g_warn_if_fail (!isdigit (*info_str));
	info_str++;
    }

    if (!*strings) {
	*strings = mk_array_new_type (MK_TYPE_STRING);
    }

    string = mk_string_append (NULL, fmt, space_str, info_str);
    *strings = mk_array_append (*strings, string);

    return string;
}


GType
mk_type_info_load (MkTypeInfo *self, GError **error)
{
    GType type = g_type_from_name (mk_type_info_get_glib_name (self));

    if (!type) {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "cannot load type %s",
		     mk_type_info_get_glib_name (self));
    }

    return type;
}
