#include "mk-type-table.h"
#include "mk-type-info.h"
#include "mk-ns-info.h"
#include "mk-array.h"
#include "mk-error.h"

#include <ctype.h>

struct _MkTypeTable
{
    GObject parent_instance;

    MkString *entered;
    MkArray *includes_array;
    GHashTable *includes_table;

    GHashTable *space_table;
    GHashTable *info_table_full;
    GHashTable *info_table_direct;
};

G_DEFINE_TYPE (MkTypeTable, mk_type_table, G_TYPE_OBJECT);

static void
finalize (GObject *gobj)
{
    MkTypeTable *self = MK_TYPE_TABLE (gobj);

    mk_array_unref (self->includes_array);
    g_hash_table_unref (self->includes_table);
    g_hash_table_unref (self->space_table);
    g_hash_table_unref (self->info_table_full);
    g_clear_pointer (&self->info_table_direct, g_hash_table_unref);

    G_OBJECT_CLASS (mk_type_table_parent_class)->finalize (gobj);
}

static void
register_scalar (MkTypeTable *self, MkTypeTag tag)
{
    static const struct {
	const gchar *name;
	const gchar *c_name;
    } data[] = {
	[MK_TYPE_TAG_VOID] = { "void", "void" },
	[MK_TYPE_TAG_BOOLEAN] = { "bool", "gboolean" },
	[MK_TYPE_TAG_CHAR] = { "char", "gchar" },
	[MK_TYPE_TAG_INT8] = { "int8", "gint8" },
	[MK_TYPE_TAG_UINT8] = { "uint8", "guint8" },
	[MK_TYPE_TAG_INT16] = { "int16", "gint16" },
	[MK_TYPE_TAG_UINT16] = { "uint16", "guint16" },
	[MK_TYPE_TAG_INT32] = { "int32", "gint32" },
	[MK_TYPE_TAG_UINT32] = { "uint32", "guint32" },
	[MK_TYPE_TAG_INT64] = { "int64", "gint64" },
	[MK_TYPE_TAG_UINT64] = { "uint64", "guint64" },
	[MK_TYPE_TAG_FLOAT] = { "float", "gfloat" },
	[MK_TYPE_TAG_DOUBLE] = { "double", "gdouble" },
	[MK_TYPE_TAG_POINTER] = { "pointer", "gpointer" },
    };

    g_assert (tag < G_N_ELEMENTS (data));
    g_assert (data[tag].name);
    g_assert (data[tag].c_name);

    mk_type_table_register_type (self, tag, NULL, 0, NULL, "GLib",
				 data[tag].name,
				 data[tag].c_name,
				 NULL, NULL, NULL, NULL);
}

static void
register_common (MkTypeTable *self)
{
    static struct {
	MkTypeTag tag;
	const gchar *name;
    } mk_common[] = {
	{ MK_TYPE_TAG_OBJECT, "Decl-Namespace" },
	{ MK_TYPE_TAG_OBJECT, "Decl-Class" },
	{ MK_TYPE_TAG_OBJECT, "Decl-Interface" },
	{ MK_TYPE_TAG_OBJECT, "Decl-Field" },
	{ MK_TYPE_TAG_BOXED, "String" },
	{ MK_TYPE_TAG_BOXED, "Array" },
	{ MK_TYPE_TAG_BOXED, "Key-Value" },
	{ MK_TYPE_TAG_ENUM, "Gi-Transfer" },
	{ MK_TYPE_TAG_ENUM, "Gi-Scope" },
	{ MK_TYPE_TAG_ENUM, "Gi-Signal-When" },
	{ MK_TYPE_TAG_ENUM, "Gi-Direction" },
	{ MK_TYPE_TAG_ENUM, "Gi-When" },
    };

    g_autoptr (GError) error = NULL;
    MkTypeInfo *glib_object;
    guint i;

    mk_type_table_register_ns (self, "GLib", "G", NULL, NULL);
    mk_type_table_register_ns (self, "GObject", "G", NULL, NULL);
    mk_type_table_register_ns (self, "Mkit", "Mk", "mk", "MK");

    glib_object = mk_type_table_register_type (self,
					       MK_TYPE_TAG_OBJECT,
					       NULL,
					       0, NULL,
					       "GObject", "Object",
					       NULL, NULL, NULL, NULL, NULL);

    for (i = MK_TYPE_TAG_SCALAR_FIRST; i <= MK_TYPE_TAG_SCALAR_LAST; i++) {
	register_scalar (self, i);
    }

    for (i = 0; i < G_N_ELEMENTS (mk_common); i++) {
	mk_type_table_register_type (self,
				     mk_common[i].tag,
				     mk_common[i].tag == MK_TYPE_TAG_OBJECT ?
				     glib_object : NULL,
				     0, NULL,
				     "Mkit", mk_common[i].name,
				     NULL, NULL, NULL, NULL, NULL);
    }
}

static void
constructed (GObject *gobj)
{
    G_OBJECT_CLASS (mk_type_table_parent_class)->constructed (gobj);

    register_common (MK_TYPE_TABLE (gobj));
}

static void
mk_type_table_init (MkTypeTable *self)
{
    self->includes_array = mk_array_new_type (MK_TYPE_STRING);
    self->includes_table = g_hash_table_new_full (g_str_hash, g_str_equal,
						  (GDestroyNotify)
						  mk_string_unref,
						  NULL);
    self->space_table = g_hash_table_new_full (g_str_hash, g_str_equal,
					       (GDestroyNotify)
					       mk_string_unref,
					       g_object_unref);
    self->info_table_full = g_hash_table_new_full (g_str_hash, g_str_equal,
						   (GDestroyNotify)
						   mk_string_unref,
						   (GDestroyNotify)
						   g_hash_table_unref);
}

static void
mk_type_table_class_init (MkTypeTableClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->finalize = finalize;
    gcls->constructed = constructed;
}

MkTypeTable *
mk_type_table_new ()
{
    return g_object_new (MK_TYPE_TYPE_TABLE, NULL);
}

void
mk_type_table_enter (MkTypeTable *self, const gchar *namespace)
{
    mk_clear_string (&self->entered);

    self->entered = mk_string_new_shared (namespace);
}

static void
reset_info_table_direct (MkTypeTable *self)
{
    g_clear_pointer (&self->info_table_direct, g_hash_table_unref);
}

void
mk_type_table_include (MkTypeTable *self, const gchar *namespace)
{
    g_autoptr (MkString) shared = NULL;

    if (!g_hash_table_lookup (self->includes_table, namespace)) {
	shared = mk_string_new_shared (namespace);

	self->includes_array = mk_array_append (self->includes_array, shared);
	g_hash_table_add (self->includes_table,
			  g_steal_pointer (&shared));
	reset_info_table_direct (self);
    }
}

static MkString *
make_type_string (MkString *base, const gchar *in,
		  const gchar *prefixfmt, const gchar *prefix,
		  MkString *(*mod) (MkString *), ...)
{
    MkString *tmp;
    va_list args;

    if (in) {
	return mk_string_new_shared (in);
    }

    tmp = mk_string_ref (base);

    va_start (args, mod);

    while (mod) {
	tmp = mod (tmp);
	mod = va_arg (args, gpointer);
    }

    va_end (args);

    if (prefix) {
	tmp = mk_string_prepend (tmp, prefixfmt, prefix);
    }

    return tmp;
}

/**
 * mk_type_table_register_ns:
 *
 * returns: (transfer none):
 */
MkSpaceInfo *
mk_type_table_register_ns (MkTypeTable *self,
			      const gchar *name,
			      const gchar *c_name,
			      const gchar *c_symbol,
			      const gchar *c_macro)
{
    g_autoptr (MkString) name_string = NULL;
    g_autoptr (MkString) c_name_string = NULL;
    g_autoptr (MkString) c_symbol_string = NULL;
    g_autoptr (MkString) c_macro_string = NULL;
    MkSpaceInfo *space;

    g_assert (name);

    name_string = mk_string_new_shared (name);
    c_name_string = make_type_string (name_string, c_name, NULL, NULL,
				      mk_string_remove_dash, NULL);
    c_symbol_string = make_type_string (name_string, c_symbol, NULL, NULL,
					mk_string_underscorify,
					mk_string_tolower, NULL);
    c_macro_string = make_type_string (c_name_string, c_macro, NULL, NULL,
				       mk_string_underscorify,
				       mk_string_toupper, NULL);

    space = mk_ns_info_new (name_string,
			       c_name_string,
			       c_symbol_string,
			       c_macro_string);

    g_hash_table_insert (self->space_table,
			 mk_string_ref (name_string), space);

    reset_info_table_direct (self);

    return space;
}

/**
 * mk_type_table_get_ns:
 *
 * returns: (transfer none):
 */
MkSpaceInfo *
mk_type_table_get_ns (MkTypeTable *self, const gchar *name)
{
    return g_hash_table_lookup (self->space_table, name);
}

/**
 * mk_type_table_register_type:
 * @interfaces: (array length=num_interfaces):
 *
 * returns: (transfer none):
 */
MkTypeInfo *
mk_type_table_register_type (MkTypeTable *self,
			     MkTypeTag tag,
			     MkTypeInfo *parent,
			     guint num_interfaces,
			     MkTypeInfo **interfaces,
			     const gchar *namespace,
			     const gchar *name,
			     const gchar *c_name,
			     const gchar *c_symbol,
			     const gchar *c_macro,
			     const gchar *glib_name,
			     const gchar *glib_get_type)
{
    g_autoptr (MkString) name_string = NULL;
    g_autoptr (MkString) c_name_string = NULL;
    g_autoptr (MkString) c_symbol_string = NULL;
    g_autoptr (MkString) c_macro_string = NULL;
    g_autoptr (MkString) glib_name_string = NULL;
    g_autoptr (MkString) glib_get_type_string = NULL;
    g_autoptr (MkString) glib_type_macro_string = NULL;
    MkSpaceInfo *space;
    MkTypeInfo *info;
    GHashTable *direct;

    g_assert (namespace);
    g_assert (name);

    space = g_hash_table_lookup (self->space_table, namespace);
    g_assert (space);

    name_string = mk_string_new_shared (name);

    c_name_string = make_type_string (name_string, c_name, "%s",
				      mk_ns_info_get_c_name (space),
				      mk_string_remove_dash, NULL);
    c_symbol_string = make_type_string (name_string, c_symbol, "%s_",
					mk_ns_info_get_c_symbol (space),
					mk_string_underscorify,
					mk_string_tolower, NULL);
    c_macro_string = make_type_string (name_string, c_macro, "%s_",
				       mk_ns_info_get_c_macro (space),
				       mk_string_underscorify,
				       mk_string_toupper, NULL);
    glib_type_macro_string = make_type_string (name_string, c_macro,
					       "%s_TYPE_",
					       mk_ns_info_get_c_macro (space),
					       mk_string_underscorify,
					       mk_string_toupper, NULL);

    name_string = mk_string_remove_dash (name_string);

    if (glib_name) {
	glib_name_string = mk_string_new_shared (glib_name);
    } else {
	glib_name_string = mk_string_ref (c_name_string);
    }

    if (glib_get_type) {
	glib_get_type_string = mk_string_new_shared (glib_get_type);
    } else {
	glib_get_type_string = mk_string_append (glib_get_type_string,
						 "%s_get_type",
						 c_symbol_string);
    }

    direct = g_hash_table_lookup (self->info_table_full, namespace);

    if (!direct) {
	direct = g_hash_table_new_full (g_str_hash,
					g_str_equal,
					(GDestroyNotify)
					mk_string_unref,
					g_object_unref);

	g_hash_table_insert (self->info_table_full,
			     mk_string_new_shared (namespace),
			     direct);
    }

    info = mk_type_info_new (space, parent, tag, name_string,
			     c_name_string,
			     c_symbol_string,
			     c_macro_string,
			     glib_name_string,
			     glib_get_type_string,
			     glib_type_macro_string);

    g_hash_table_insert (direct, mk_string_ref (name_string), info);

    reset_info_table_direct (self);

    return info;
}

static GHashTable *
get_info_table_direct (MkTypeTable *self)
{
    g_autoptr (GHashTable) checked = NULL;
    g_autoptr (GHashTable) table = NULL;
    GHashTable *direct;
    GHashTableIter iter;
    MkTypeInfo *info;
    MkString *name;

    if (self->info_table_direct) {
	goto done;
    }

    checked = g_hash_table_new (g_str_hash, g_str_equal);
    table = g_hash_table_new_full (g_str_hash, g_str_equal,
				   (GDestroyNotify)
				   mk_string_unref,
				   g_object_unref);

    MK_ARRAY_FOREACH (self->includes_array, MkString *, namespace) {
	if (g_hash_table_lookup (checked, namespace)) {
	    continue;
	}

	g_hash_table_add (checked, namespace);

	direct = g_hash_table_lookup (self->info_table_full, namespace);

	if (!direct) {
	    continue;
	}

	g_hash_table_iter_init (&iter, direct);

	while (g_hash_table_iter_next (&iter, (gpointer *) &name,
				       (gpointer *) &info)) {
	    if (g_hash_table_lookup (table, name)) {
		continue;
	    }

	    g_hash_table_insert (table,
				 mk_string_ref (name),
				 g_object_ref (info));
	}
    }

    self->info_table_direct = g_steal_pointer (&table);

done:
    return self->info_table_direct;
}

/**
 * mk_type_table_resolve:
 *
 * returns: (transfer none):
 */
MkTypeInfo *
mk_type_table_resolve (MkTypeTable *self, const gchar *desc, GError **error)
{
    MkTypeInfo *info = NULL;
    GHashTable *direct;
    const gchar *ns, *cl;
    gchar *descdup;

    descdup = g_alloca (strlen (desc) + 1);
    strcpy (descdup, desc);

    if (strchr (descdup, '.')) {
	ns = descdup;
	cl = strchr (descdup, '.') + 1;
	*strchr (descdup, '.') = 0;

	if (strchr (cl, '.')) {
	    g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
			 "invalid type description: %s", desc);
	    return NULL;
	}
    } else {
	ns = NULL;
	cl = descdup;
    }

    if (ns) {
	direct = g_hash_table_lookup (self->info_table_full, ns);

	if (!direct) {
	    g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
			 "no such namespace \"%s\"", ns);
	    return NULL;
	}

	info = g_hash_table_lookup (direct, cl);
    } else {
	info = g_hash_table_lookup (get_info_table_direct (self), desc);
    }

    if (info) {
	return info;
    }

    if (ns) {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "no such type \"%s\" in namespace \"%s\"", cl, ns);
    } else {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "no such type \"%s\"", cl);
    }

    return NULL;
}
