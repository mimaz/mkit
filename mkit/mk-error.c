#include "mk-error.h"

G_DEFINE_QUARK (MK_ERROR, mk_error);

gboolean
mk_error_is (GError *error, MkError code)
{
    return mk_error_is_any (error, 1, &code);
}

gboolean
mk_error_is_any (GError *error, guint num_codes, const MkError *codes)
{
    guint i;

    if (!error) {
	return FALSE;
    }

    if (error->domain != MK_ERROR) {
	return FALSE;
    }

    for (i = 0; i < num_codes; i++) {
	if (error->code == codes[i]) {
	    return TRUE;
	}
    }

    return FALSE;
}

gboolean
mk_error_is_anyv (GError *error, MkError code, ...)
{
    guint count, i;
    MkError *codes;
    va_list args;

    count = 0;

    va_start (args, code);

    while (code != MK_ERROR_NONE) {
	count++;
	code = va_arg (args, MkError);
    }

    va_end (args);

    va_start (args, code);

    codes = g_newa (MkError, count);

    for (i = 0; i < count; i++) {
	codes[i] = code;
	code = va_arg (args, MkError);
    }

    va_end (args);

    return mk_error_is_any (error, count, codes);
}
