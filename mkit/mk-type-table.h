#pragma once

#include "mk-type-tag.h"
#include "mk-string.h"

G_BEGIN_DECLS

typedef struct _MkTypeInfo MkTypeInfo;
typedef struct _MkSpaceInfo MkSpaceInfo;

#define MK_TYPE_TYPE_TABLE (mk_type_table_get_type ())

G_DECLARE_FINAL_TYPE (MkTypeTable, mk_type_table,
		      MK, TYPE_TABLE, GObject);

MkTypeTable    *mk_type_table_new		    (void);
void		mk_type_table_enter		    (MkTypeTable *self,
						     const gchar *namespace);
void		mk_type_table_include		    (MkTypeTable *self,
						     const gchar *namespace);
MkSpaceInfo    *mk_type_table_register_ns	    (MkTypeTable *self,
						     const gchar *name,
						     const gchar *c_name,
						     const gchar *c_symbol,
						     const gchar *c_macro);
MkSpaceInfo    *mk_type_table_get_ns		    (MkTypeTable *self,
						     const gchar *name);
MkTypeInfo     *mk_type_table_register_type	    (MkTypeTable *self,
						     MkTypeTag tag,
						     MkTypeInfo *parent,
						     guint num_interfaces,
						     MkTypeInfo **interfaces,
						     const gchar *namespace,
						     const gchar *name,
						     const gchar *c_name,
						     const gchar *c_symbol,
						     const gchar *c_macro,
						     const gchar *glib_name,
						     const gchar *glib_get_type);
MkTypeInfo     *mk_type_table_resolve		    (MkTypeTable *self,
						     const gchar *desc,
						     GError **error);

G_END_DECLS
