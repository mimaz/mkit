#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

typedef void (*MkGenericCopy) (gpointer dst,
			       gconstpointer src,
			       gsize size,
			       GType type);
typedef void (*MkGenericFree) (gpointer item,
			       gsize size,
			       GType type);

G_END_DECLS
