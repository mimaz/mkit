#pragma once
#ifndef MKIT_NO_CONFIG
#include "mk-config.h"
#endif
#include "mk-array.h"
#include "mk-builder.h"
#include "mk-code.h"
#include "mk-declgen.h"
#include "mk-error.h"
#include "mk-generic.h"
#include "mk-gi-direction.h"
#include "mk-gi-scope.h"
#include "mk-gi-transfer.h"
#include "mk-gi-when.h"
#include "mk-init.h"
#include "mk-json-parser.h"
#include "mk-ns-info.h"
#include "mk-parser.h"
#include "mk-parse.h"
#include "mk-string.h"
#include "mk-type-info.h"
#include "mk-type-table.h"
#include "mk-type-tag.h"
#ifdef MKIT_HAS_DECL
#include "mk-decl.h"
#endif
#ifdef MKIT_HAS_GI
#include "mk-gi.h"
#include "mk-girepo.h"
#endif
