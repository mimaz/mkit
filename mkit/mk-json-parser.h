#include "mk-parser.h"

G_BEGIN_DECLS

#define MK_TYPE_JSON_PARSER (mk_json_parser_get_type ())

G_DECLARE_FINAL_TYPE (MkJsonParser, mk_json_parser,
		      MK, JSON_PARSER, MkParser);

MkParser   *mk_json_parser_new	    (void);

G_END_DECLS
