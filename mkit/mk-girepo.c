#include "mk-girepo.h"
#include "mk-parse.h"
#include "mk-error.h"
#include "mk-builder.h"
#include "mk-xml-parser.h"

struct _MkGirepo
{
    GObject parent_instance;
    MkParser *parser;
    MkBuilder *builder;
    GSList *paths;
    GHashTable *gir_table;
    GHashTable *fs_cache;
};

G_DEFINE_TYPE (MkGirepo, mk_girepo, G_TYPE_OBJECT);

static void
finalize (GObject *gobj)
{
    MkGirepo *self = MK_GIREPO (gobj);

    g_clear_object (&self->parser);
    g_clear_object (&self->builder);
    g_clear_slist (&self->paths, (GDestroyNotify) mk_string_unref);
    g_hash_table_unref (self->gir_table);
    g_hash_table_unref (self->fs_cache);

    G_OBJECT_CLASS (mk_girepo_parent_class)->finalize (gobj);
}

static void
mk_girepo_init (MkGirepo *self)
{
    self->parser = mk_xml_parser_new ();
    self->builder = mk_builder_new ();
    self->gir_table = g_hash_table_new_full (g_str_hash, g_str_equal,
					     (GDestroyNotify)
					     mk_string_unref,
					     g_object_unref);
    self->fs_cache = g_hash_table_new_full (g_str_hash, g_str_equal,
					    (GDestroyNotify)
					    mk_string_unref,
					    (GDestroyNotify)
					    g_hash_table_unref);
}

static void
mk_girepo_class_init (MkGirepoClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    mk_gi_init_types ();

    gcls->finalize = finalize;
}

MkGirepo *
mk_girepo_new ()
{
    MkGirepo *self = g_object_new (MK_TYPE_GIREPO, NULL);

    mk_girepo_prepend_default (self);

    return self;
}

void
mk_girepo_prepend_path (MkGirepo *self, const gchar *path)
{
    if (!g_slist_find_custom (self->paths, path, (GCompareFunc) g_strcmp0)) {
	self->paths = g_slist_prepend (self->paths,
				       mk_string_new_shared (path));
    }
}

void
mk_girepo_prepend_default (MkGirepo *self)
{
    static const gchar *paths[] = {
	"/usr/share/gir-1.0",
	"/usr/share/local/gir-1.0",
    };
    guint i;

    for (i = 0; i < G_N_ELEMENTS (paths); i++) {
	mk_girepo_prepend_path (self, paths[i]);
    }
}

static MkGiRepository *
try_load (MkGirepo *self, MkString *path, const gchar *filename, GError **error)
{
    g_autofree gchar *fullpath = NULL;
    g_autoptr (GDir) dir = NULL;
    GError *local_error = NULL;
    GHashTable *cache;
    const gchar *name;

    cache = g_hash_table_lookup (self->fs_cache, path);

    if (!cache) {
	dir = g_dir_open (path, 0, NULL);

	if (!dir) {
	    return NULL;
	}

	cache = g_hash_table_new_full (g_str_hash, g_str_equal,
				       (GDestroyNotify)
				       mk_string_unref,
				       NULL);

	g_hash_table_insert (self->fs_cache, mk_string_ref (path), cache);

	while ((name = g_dir_read_name (dir))) {
	    g_hash_table_add (cache, mk_string_new_shared (name));
	}
    }

    if (!g_hash_table_lookup (cache, filename)) {
	return NULL;
    }

    g_assert (!g_str_has_suffix (path, "/"));

    fullpath = g_strconcat (path, "/", filename, NULL);

    mk_parser_load_file (self->parser, fullpath, &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return NULL;
    }

    return mk_builder_build (self->builder, self->parser,
			     MK_TYPE_GI_REPOSITORY, error);
}

/**
 * mk_girepo_load:
 * return: (transfer none):
 */
MkGiRepository *
mk_girepo_load (MkGirepo *self, const gchar *name, GError **error)
{
    g_autofree gchar *filename = NULL;
    GError *local_error = NULL;
    MkGiRepository *gir;
    GSList *paths;

    gir = g_hash_table_lookup (self->gir_table, name);

    if (gir) {
	return gir;
    }

    filename = g_strconcat (name, ".gir", NULL);
    paths = self->paths;
    gir = NULL;

    while (!gir && paths) {
	gir = try_load (self, paths->data, filename,
			local_error ? NULL : &local_error);
	paths = paths->next;
    }

    if (gir) {
	g_clear_error (&local_error);
	g_hash_table_insert (self->gir_table,
			     mk_string_new_shared (name),
			     gir);
    } else {
	if (local_error) {
	    g_propagate_error (error, local_error);
	} else {
	    g_set_error (error, MK_ERROR, MK_ERROR_GIR_NOT_FOUND,
			 "no such gir file: %s", filename);
	}
    }

    return gir;
}
