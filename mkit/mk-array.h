#pragma once

#include "mk-generic.h"

G_BEGIN_DECLS

#define MK_TYPE_ARRAY (mk_array_get_type ())

#ifndef MkArray
typedef struct _MkArray MkArray;
#endif

typedef void (*MkArrayForeach) (gpointer element,
				gpointer user_data,
				GError **error);
typedef gboolean (*MkArrayRemoveIf) (gpointer element,
				     gpointer user_data,
				     GError **error);

extern const guint mk_array_meta_size;

GType		mk_array_get_type	(void);
MkArray	       *mk_array_new		(GType e_type,
					 gint e_size,
					 MkGenericCopy e_copy,
					 MkGenericFree e_destroy,
					 guint reserve)
					G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_new_type	(GType type)
					G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_new_value	(void)
					G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_ref		(MkArray *self);
void		mk_array_unref		(MkArray *self);
GType		mk_array_e_type		(MkArray *self)
					G_GNUC_CONST;
guint		mk_array_e_size		(MkArray *self)
					G_GNUC_CONST;
guint		mk_array_len		(MkArray *self)
					G_GNUC_CONST;
guint		mk_array_cap		(MkArray *self)
					G_GNUC_CONST;
gboolean	mk_array_holds_ptr	(MkArray *self)
					G_GNUC_CONST;
gboolean	mk_array_holds_type	(MkArray *self,
					 GType type)
					G_GNUC_CONST;
gboolean	mk_array_is_mutable	(MkArray *self);
MkArray	       *mk_array_make_mutable   (MkArray *self,
					 guint reserve)
					G_GNUC_WARN_UNUSED_RESULT;
gpointer	mk_array_index		(MkArray *self,
					 guint e_size,
					 gint index);
gpointer	mk_array_at_unsafe	(MkArray *self,
					 gint index);
gpointer	mk_array_at		(MkArray *self,
					 gint index);
gpointer	mk_array_get		(MkArray *self,
					 gint index);
gboolean	mk_array_get_value	(MkArray *self,
					 gint index,
					 GValue *value);
MkArray	       *mk_array_steal		(MkArray *self,
					 gint index,
					 gpointer element)
					G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_clear		(MkArray *self)
					G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_append		(MkArray *self,
					 gconstpointer element)
                                        G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_append_take	(MkArray *self,
					 gconstpointer element)
                                        G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_append_value   (MkArray *self,
					 const GValue *value)
                                        G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_remove		(MkArray *self,
					 gint index)
                                        G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_remove_if	(MkArray *self,
					 MkArrayRemoveIf cond,
					 gpointer user_data,
					 GError **error)
					G_GNUC_WARN_UNUSED_RESULT;
MkArray	       *mk_array_concat		(MkArray *self,
					 ...)
                                        G_GNUC_WARN_UNUSED_RESULT;
void		mk_array_qsort		(MkArray *self,
					 GCompareFunc compare);
gpointer	mk_array_bsearch	(MkArray *self,
					 gconstpointer item,
					 GCompareFunc compare);
gpointer	mk_array_find		(MkArray *self,
					 gconstpointer element,
					 GCompareFunc compare);
void		mk_array_foreach	(MkArray *self,
					 MkArrayForeach func,
					 gpointer user_data,
					 GError **error);
gboolean	mk_set_array		(MkArray **self,
					 MkArray *array);
void		mk_clear_array		(MkArray **self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (MkArray, mk_array_unref);

#define MK_ARRAY_OF_VALUE(v) (*(MkArray **) &(v)->data[0].v_pointer)

#define MK_ARRAY_FOREACH_INDEX(i) (__i##i - __b##i)

#define MK_ARRAY_FOREACH_FIRST(i) (__i##i == __b##i)

#define MK_ARRAY_FOREACH_LAST(i) (__i##i == __e##i - 1)

#define MK_ARRAY_FOREACH(a, t, i) \
    for (typeof (t) \
	 __z##i = { 0 }, \
	 *__b##i = ({ g_assert (!a || mk_array_e_size (a) == sizeof (t)); \
		    (gpointer) a; }), \
	 *__i##i = __b##i, \
	 *__e##i = __i##i + mk_array_len (a), \
	 i = __i##i < __e##i ? *__i##i : __z##i; \
	 __i##i < __e##i; i = (++__i##i < __e##i) ? *__i##i : __z##i)

#define MK_ARRAY_INDEX(a, t, i) (*(t *) mk_array_index (a, sizeof (t), i))

G_END_DECLS
