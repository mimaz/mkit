#pragma once

#include "mk-enum-gtype.h"

G_BEGIN_DECLS

#define MK_TYPE_TAG_SCALAR_FIRST  MK_TYPE_TAG_VOID
#define MK_TYPE_TAG_SCALAR_LAST   MK_TYPE_TAG_POINTER

typedef enum {
    MK_TYPE_TAG_VOID,
    MK_TYPE_TAG_BOOLEAN,
    MK_TYPE_TAG_CHAR,
    MK_TYPE_TAG_INT8,
    MK_TYPE_TAG_UINT8,
    MK_TYPE_TAG_INT16,
    MK_TYPE_TAG_UINT16,
    MK_TYPE_TAG_INT32,
    MK_TYPE_TAG_UINT32,
    MK_TYPE_TAG_INT64,
    MK_TYPE_TAG_UINT64,
    MK_TYPE_TAG_FLOAT,
    MK_TYPE_TAG_DOUBLE,
    MK_TYPE_TAG_POINTER,

    MK_TYPE_TAG_STRING,

    MK_TYPE_TAG_BOXED,
    MK_TYPE_TAG_OBJECT,
    MK_TYPE_TAG_ENUM,
} MkTypeTag;

gboolean    mk_type_tag_is_value	(MkTypeTag tag);
gboolean    mk_type_tag_is_pointer	(MkTypeTag tag);

G_END_DECLS
