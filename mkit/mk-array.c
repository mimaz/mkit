#include "mk-array.h"

#define RESERVE_DEFAULT 0

typedef struct {
    grefcount rc;
    GType e_type;
    guint e_size:31;
    guint holds_ptr:1;
    MkGenericCopy e_copy;
    gpointer e_data;
    MkGenericFree e_destroy;

    guint len;
    guint cap;
} Meta;

static Meta *
array_meta (MkArray *self)
{
    return &((Meta *) self)[-1];
}

const guint mk_array_meta_size = sizeof (Meta);

G_DEFINE_BOXED_TYPE (MkArray, mk_array,
		     mk_array_ref,
		     mk_array_unref);

static guint
p2min (guint v)
{
    guint i = 1;

    while (i < v) {
	i *= 2;
    }

    return i;
}

static void
e_copy_raw_v (gpointer dst_ptr, gconstpointer element,
	      gsize size, GType type)
{
    memcpy (dst_ptr, element, size);
}

static void
e_copy_raw_p (gpointer dst_ptr, gconstpointer element,
	      gsize size, GType type)
{
    *(gpointer *) dst_ptr = (gpointer) element;
}

static void
e_copy_object (gpointer dst_ptr, gconstpointer element,
	       gsize size, GType type)
{
    g_assert (size == sizeof (GObject *));
    g_assert (g_type_is_a (type, G_TYPE_OBJECT));
    g_assert (G_IS_OBJECT (element));

    *((GObject **) dst_ptr) = g_object_ref (G_OBJECT (element));
}

static void
e_copy_boxed (gpointer dst_ptr, gconstpointer element,
	      gsize size, GType type)
{
    g_assert (size == sizeof (gpointer));
    g_assert (g_type_is_a (type, G_TYPE_BOXED));

    *((gpointer *) dst_ptr) = g_boxed_copy (type, element);
}

static void
e_destroy_boxed (gpointer element, gsize size, GType type)
{
    g_assert (size == sizeof (gpointer));
    g_assert (g_type_is_a (type, G_TYPE_BOXED));

    g_boxed_free (type, element);
}

static void
e_copy_value (gpointer dst, gconstpointer src, gsize size, GType type)
{
    g_assert (size == sizeof (GValue));
    g_assert (type == G_TYPE_VALUE);

    *(GValue *) dst = (GValue) G_VALUE_INIT;

    g_value_init (dst, G_VALUE_TYPE (src));
    g_value_copy (src, dst);
}

static void
e_destroy_value (gpointer element, gsize size, GType type)
{
    g_assert (size == sizeof (GValue));
    g_assert (type == G_TYPE_VALUE);

    g_value_unset (element);
}

/**
 * mk_array_new: (skip)
 */
MkArray *
mk_array_new (GType e_type,
	      gint e_size,
	      MkGenericCopy e_copy,
	      MkGenericFree e_destroy,
	      guint reserve)
{
    MkArray *self;
    gboolean holds_ptr;

    g_assert (e_size);

    if (e_size < 0) {
	e_size = sizeof (gpointer);
	holds_ptr = TRUE;
    } else {
	holds_ptr = FALSE;
    }

    if (!e_copy) {
	e_copy = holds_ptr ? e_copy_raw_p : e_copy_raw_v;
    }

    self = g_malloc0 (reserve * e_size + sizeof (Meta)) +
	sizeof (Meta);

    g_ref_count_init (&array_meta (self)->rc);

    array_meta (self)->holds_ptr = holds_ptr;
    array_meta (self)->e_type = e_type;
    array_meta (self)->e_size = e_size;
    array_meta (self)->e_copy = e_copy;
    array_meta (self)->e_destroy = e_destroy;
    array_meta (self)->len = 0;
    array_meta (self)->cap = reserve;

    return self;
}

MkArray *
mk_array_new_type (GType type)
{
    if (g_type_is_a (type, G_TYPE_OBJECT)) {
	return mk_array_new (type, -1, e_copy_object,
			     (MkGenericFree)
			     g_object_unref,
			     RESERVE_DEFAULT);
    }

    if (g_type_is_a (type, G_TYPE_BOXED)) {
	return mk_array_new (type, -1, e_copy_boxed,
			     e_destroy_boxed,
			     RESERVE_DEFAULT);
    }

    g_error ("invalid type");
}

MkArray *
mk_array_new_value ()
{
    return mk_array_new (G_TYPE_VALUE,
			 sizeof (GValue),
			 e_copy_value,
			 e_destroy_value,
			 RESERVE_DEFAULT);
}

MkArray *
mk_array_ref (MkArray *self)
{
    g_ref_count_inc (&array_meta (self)->rc);

    return self;
}

void
mk_array_unref (MkArray *self)
{
    gpointer element;
    guint8 *mem;
    guint i;

    if (g_ref_count_dec (&array_meta (self)->rc)) {
	if (array_meta (self)->e_destroy) {
	    if (mk_array_holds_ptr (self)) {
		for (i = 0; i < mk_array_len (self); i++) {
		    element = ((gpointer *) self)[i];

		    if (element) {
			array_meta (self)->e_destroy (element,
						      array_meta (self)->e_size,
						      array_meta (self)->e_type);
		    }
		}
	    } else {
		mem = (guint8 *) self;

		for (i = 0; i < mk_array_len (self); i++) {
		    array_meta (self)->e_destroy (mem,
						  array_meta (self)->e_size,
						  array_meta (self)->e_type);
		    mem += array_meta (self)->e_size;
		}
	    }
	}

	g_free (array_meta (self));
    }
}

GType
mk_array_e_type (MkArray *self)
{
    return array_meta (self)->e_type;
}

guint
mk_array_e_size (MkArray *self)
{
    return array_meta (self)->e_size;
}

/**
 * mk_array_len:
 * @self: (nullable)
 */
guint
mk_array_len (MkArray *self)
{
    return self ? array_meta (self)->len : 0;
}

guint
mk_array_cap (MkArray *self)
{
    return array_meta (self)->cap;
}

gboolean
mk_array_holds_ptr (MkArray *self)
{
    return array_meta (self)->holds_ptr;
}

gboolean
mk_array_holds_type (MkArray *self, GType type)
{
    return g_type_is_a (mk_array_e_type (self), type);
}

gboolean
mk_array_is_mutable (MkArray *self)
{
    return g_ref_count_compare (&array_meta (self)->rc, 1);
}

/**
 * mk_array_make_mutable:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkArray *
mk_array_make_mutable (MkArray *self, guint reserve)
{
    MkArray *dropped;
    guint i;

    g_return_val_if_fail (self, NULL);

    if (!mk_array_is_mutable (self) || mk_array_cap (self) < reserve) {
	reserve = p2min (reserve);

	if (mk_array_is_mutable (self)) {
	    self = g_realloc (array_meta (self),
			      reserve *
			      array_meta (self)->e_size +
			      sizeof (Meta)) +
		sizeof (Meta);

	    g_assert (mk_array_cap (self) <= reserve);

	    memset (mk_array_at_unsafe (self, mk_array_cap (self)),
		    0, (reserve - mk_array_cap (self)) *
		    mk_array_e_size (self));

	    array_meta (self)->cap = reserve;
	} else {
	    dropped = self;
	    self = mk_array_new (array_meta (self)->e_type,
				 mk_array_holds_ptr (dropped) ?
				 -1 : array_meta (self)->e_size,
				 array_meta (self)->e_copy,
				 array_meta (self)->e_destroy,
				 reserve);

	    for (i = 0; i < mk_array_len (dropped); i++) {
		array_meta (self)->e_copy (mk_array_at_unsafe (self, i),
					   mk_array_get (dropped, i),
					   array_meta (self)->e_size,
					   array_meta (self)->e_type);
	    }

	    array_meta (self)->len = mk_array_len (dropped);

	    mk_array_unref (dropped);
	}
    }

    return self;
}

gpointer
mk_array_index (MkArray *self, guint e_size, gint index)
{
    g_assert (e_size == mk_array_e_size (self));

    return mk_array_at (self, index);
}

/**
 * mk_array_at_unsafe: (skip)
 */
gpointer
mk_array_at_unsafe (MkArray *self, gint index)
{
    if (index < 0) {
	index += mk_array_len (self);
    }

    return (guint8 *) self + index * array_meta (self)->e_size;
}

gpointer
mk_array_at (MkArray *self, gint index)
{
    g_return_val_if_fail (self, NULL);

    if (index < 0) {
	index += mk_array_len (self);
    }

    g_return_val_if_fail (index >= 0 && index < mk_array_len (self), NULL);

    /*g_message ("at %d / %u", index, mk_array_len (self));*/

    return mk_array_at_unsafe (self, index);
}

gpointer
mk_array_get (MkArray *self, gint index)
{
    if (index < 0) {
	index += mk_array_len (self);
    }

    g_return_val_if_fail (index >= 0 && index < mk_array_len (self), NULL);

    if (mk_array_holds_ptr (self)) {
	g_assert (mk_array_e_size (self) == sizeof (gpointer));

	return ((gpointer *) self)[index];
    }

    return mk_array_at (self, index);
}

gboolean
mk_array_get_value (MkArray *self, gint index, GValue *value)
{
    g_return_val_if_fail (mk_array_e_type (self), FALSE);

    g_value_init (value, mk_array_e_type (self));

    if (g_type_is_a (mk_array_e_type (self), G_TYPE_OBJECT)) {
	g_value_set_object (value, mk_array_get (self, index));
    } else if (g_type_is_a (mk_array_e_type (self), G_TYPE_BOXED)) {
	g_value_set_boxed (value, mk_array_get (self, index));
    } else {
	g_error ("unsupported type");
    }

    return TRUE;
}

static MkArray *
remove_element (MkArray *self, gint index, gpointer element)
{
    if (index < 0) {
	index += mk_array_len (self);
    }

    g_return_val_if_fail (index >= 0 && index < mk_array_len (self), self);

    self = mk_array_make_mutable (self, 0);

    if (element) {
	memcpy (element, mk_array_at (self, index),
		mk_array_e_size (self));
    } else {
	array_meta (self)->e_destroy (mk_array_get (self, index),
				      mk_array_e_size (self),
				      mk_array_e_type (self));
    }

    if (mk_array_len (self) - index - 1 > 0) {
	memmove (mk_array_at (self, index),
		 mk_array_at (self, index + 1),
		 (mk_array_len (self) - index - 1) *
		 mk_array_e_size (self));
    }

    array_meta (self)->len--;

    return self;
}

/**
 * mk_array_steal:
 * @self: (transfer full)
 * @element: (out) (transfer full)
 *
 * returns: (transfer full)
 */
MkArray *
mk_array_steal (MkArray *self, gint index, gpointer element)
{
    g_assert (element);

    return remove_element (self, index, element);
}

/**
 * mk_array_clear:
 * @self: (transfer full)
 *
 * returns: (transfer full)
 */
MkArray *
mk_array_clear (MkArray *self)
{
    guint i;

    g_return_val_if_fail (self, NULL);

    if (mk_array_len (self) < 1) {
	return self;
    }

    self = mk_array_make_mutable (self, 0);

    for (i = 0; i < mk_array_len (self); i++) {
	array_meta (self)->e_destroy (mk_array_get (self, i),
				      mk_array_e_size (self),
				      mk_array_e_type (self));
    }

    array_meta (self)->len = 0;

    return self;
}

static MkArray *
append_element (MkArray *self, gconstpointer element, gboolean copy)
{
    self = mk_array_make_mutable (self, mk_array_len (self) + 1);

    if (copy) {
	array_meta (self)->e_copy (mk_array_at_unsafe (self, mk_array_len (self)),
				   element, array_meta (self)->e_size,
				   array_meta (self)->e_type);
    } else {
	memcpy (mk_array_at_unsafe (self, mk_array_len (self)), element,
		mk_array_e_size (self));
    }

    array_meta (self)->len++;

    return self;
}

/**
 * mk_array_append:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkArray *
mk_array_append (MkArray *self, gconstpointer element)
{
    return append_element (self, element, TRUE);
}

/**
 * mk_array_append_take:
 * @self: (transfer full)
 * @element: (transfer full)
 *
 * returns: (transfer full)
 */
MkArray *
mk_array_append_take (MkArray *self, gconstpointer element)
{
    return append_element (self, element, FALSE);
}

/**
 * mk_array_append_value:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkArray *
mk_array_append_value (MkArray *self, const GValue *value)
{
    g_auto (GValue) transformed = G_VALUE_INIT;
    GType value_type, e_type;
    gpointer element;

    value_type = G_VALUE_TYPE (value);
    e_type = mk_array_e_type (self);

    if (value_type != e_type) {
	if (!g_value_type_transformable (value_type, e_type)) {
	    g_critical ("cannot transform %s to %s",
			g_type_name (value_type),
			g_type_name (e_type));
	    return self;
	}

	g_value_init (&transformed, e_type);
	g_value_transform (value, &transformed);

	value = &transformed;
    }

    if (g_type_is_a (e_type, G_TYPE_OBJECT)) {
	element = g_value_get_object (value);
    } else if (g_type_is_a (e_type, G_TYPE_BOXED)) {
	element = g_value_get_boxed (value);
    } else {
	g_error ("unsupported type: %s", g_type_name (e_type));
    }

    return mk_array_append (self, element);
}

/**
 * mk_array_remove:
 * @self: (transfer full)
 *
 * returns: (transfer full)
 */
MkArray *
mk_array_remove (MkArray *self, gint index)
{
    return remove_element (self, index, NULL);
}

/**
 * mk_array_remove_if:
 * @self: (transfer full)
 * @cond: (scope call)
 *
 * returns: (transfer full)
 */
MkArray *
mk_array_remove_if (MkArray *self, MkArrayRemoveIf cond,
		    gpointer user_data, GError **error)
{
    GError *local_error = NULL;
    gboolean rm;
    guint i;

    for (i = 0; i < mk_array_len (self); i++) {
	rm = cond (mk_array_get (self, i), user_data, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return self;
	}

	if (rm) {
	    self = mk_array_remove (self, i);
	}
    }

    return self;
}

/**
 * mk_array_concat:
 * @self: (transfer full)
 *
 * returns: (transfer full)
 */
MkArray *
mk_array_concat (MkArray *self, ...)
{
    MkArray *next;
    va_list args;
    guint off, i;

    va_start (args, self);

    next = va_arg (args, MkArray *);

    while (next) {
	if (!g_type_is_a (mk_array_e_type (next),
			  mk_array_e_type (self))) {
	    g_warning ("%s: type %s not compatible with %s",
		       __func__,
		       g_type_name (mk_array_e_type (next)),
		       g_type_name (mk_array_e_type (self)));
	    va_end (args);
	    return self;
	}

	if (mk_array_len (next)) {
	    self = mk_array_make_mutable (self,
					  mk_array_len (self) +
					  mk_array_len (next));
	    off = mk_array_len (self);

	    for (i = 0; i < mk_array_len (next); i++) {
		array_meta (self)->e_copy (mk_array_at_unsafe (self, off + i),
					   mk_array_get (next, i),
					   mk_array_e_size (self),
					   mk_array_e_type (self));
	    }

	    array_meta (self)->len += mk_array_len (next);
	}

	next = va_arg (args, MkArray *);
    }

    va_end (args);

    return self;
}

/**
 * mk_array_qsort:
 * @compare: (scope call):
 */
void
mk_array_qsort (MkArray *self, GCompareFunc compare)
{
    qsort (self, mk_array_len (self), mk_array_e_size (self), compare);
}

/**
 * mk_array_bsearch:
 * @compare: (scope call)
 *
 * returns: (transfer none)
 */
gpointer
mk_array_bsearch (MkArray *self, gconstpointer item, GCompareFunc compare)
{
    /* TODO it may not work */
    return self ? bsearch (item, self, mk_array_len (self),
			   mk_array_e_size (self), compare) : NULL;
}

/**
 * mk_array_find:
 * @compare: (scope call):
 */
gpointer
mk_array_find (MkArray *self, gconstpointer element,
	       GCompareFunc compare)
{
    guint i;

    for (i = 0; i < mk_array_len (self); i++) {
	if (!compare (mk_array_get (self, i), element)) {
	    return mk_array_get (self, i);
	}
    }

    return NULL;
}

/**
 * mk_array_foreach:
 * @func: (scope call):
 */
void
mk_array_foreach (MkArray *self, MkArrayForeach func,
		  gpointer user_data, GError **error)
{
    GError *local_error = NULL;
    guint i;

    if (!self) {
	return;
    }

    for (i = 0; i < mk_array_len (self); i++) {
	func (mk_array_get (self, i), user_data, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	}
    }
}

gboolean
mk_set_array (MkArray **self, MkArray *array)
{
    if (array != *self) {
	g_autoptr (MkArray) prev = *self;

	*self = array ? mk_array_ref (array) : NULL;

	return TRUE;
    }

    return FALSE;
}

void
mk_clear_array (MkArray **self)
{
    g_clear_pointer (self, mk_array_unref);
}
