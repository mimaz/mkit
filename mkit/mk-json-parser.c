#include "mk-json-parser.h"
#include "mk-string.h"
#include "mk-array.h"

#include <json-glib/json-glib.h>

struct _MkJsonParser
{
    GObject parent_instance;
    JsonParser *parser;
    MkArray *name_stack;

    MkParserTraverse traverse;
    gpointer user_data;
    GError **error;
    gboolean stop;
};

G_DEFINE_TYPE (MkJsonParser, mk_json_parser, MK_TYPE_PARSER);

static gboolean
emit (MkJsonParser *self, MkParserAction action, const GValue *data)
{
    GError *local_error = NULL;

    self->traverse (action, mk_array_len (self->name_stack) ?
		    mk_array_get (self->name_stack, -1) : NULL, data,
		    self->user_data, &local_error);

    if (local_error) {
	g_propagate_error (self->error, local_error);
	return self->stop = TRUE;
    }

    return self->stop = FALSE;
}

static void
push_name (MkJsonParser *self, const gchar *name)
{
    g_autoptr (MkString) s = mk_string_new_shared (name);

    self->name_stack = mk_array_append (self->name_stack, s);
}

static void
pop_name (MkJsonParser *self)
{
    self->name_stack = mk_array_remove (self->name_stack, -1);
}

static void parse_node (MkJsonParser *self, JsonNode *node);

static void
parse_object_iter (JsonObject *object, const gchar *name,
		   JsonNode *node, gpointer user_data)
{
    MkJsonParser *self = MK_JSON_PARSER (user_data);

    if (self->stop) {
	return;
    }

    push_name (self, name);
    parse_node (self, node);
    pop_name (self);
}

static void
parse_array_iter (JsonArray *array, guint index,
		  JsonNode *node, gpointer user_data)
{
    MkJsonParser *self = MK_JSON_PARSER (user_data);

    if (self->stop) {
	return;
    }

    parse_node (self, node);
}

static void
parse_node (MkJsonParser *self, JsonNode *node)
{
    g_auto (GValue) value = G_VALUE_INIT;
    const gchar *s;

    switch (json_node_get_node_type (node)) {
    case JSON_NODE_VALUE:
	if (json_node_get_value_type (node) == G_TYPE_STRING)  {
	    s = json_node_get_string (node);

	    g_value_init (&value, MK_TYPE_STRING);
	    g_value_take_boxed (&value, mk_string_new_shared (s));
	} else {
	    json_node_get_value (node, &value);
	}

	emit (self, MK_PARSER_VALUE, &value);
	break;

    case JSON_NODE_OBJECT:
	if (emit (self, MK_PARSER_BEGIN_OBJECT, NULL)) {
	    return;
	}

	json_object_foreach_member (json_node_get_object (node),
				    parse_object_iter, self);

	if (self->stop) {
	    return;
	}

	emit (self, MK_PARSER_END_OBJECT, NULL);
	break;

    case JSON_NODE_ARRAY:
	json_array_foreach_element (json_node_get_array (node),
				    parse_array_iter, self);
	break;

    case JSON_NODE_NULL:
	emit (self, MK_PARSER_VALUE, NULL);
	break;
    }
}

static void
load_file (MkParser *self, const gchar *path, GError **error)
{
    g_autoptr (JsonParser) parser = json_parser_new ();

    if (json_parser_load_from_file (parser, path, error)) {
	g_set_object (&MK_JSON_PARSER (self)->parser, parser);
    }
}

static void
load_text (MkParser *self, const gchar *text, gssize len, GError **error)
{
    g_autoptr (JsonParser) parser = json_parser_new ();

    if (json_parser_load_from_data (parser, text, len, error)) {
	g_set_object (&MK_JSON_PARSER (self)->parser, parser);
    }
}

static void
traverse (MkParser *parser, MkParserTraverse traverse,
	  gpointer user_data, GError **error)
{
    MkJsonParser *self = MK_JSON_PARSER (parser);

    self->name_stack = mk_array_clear (self->name_stack);
    self->traverse = traverse;
    self->user_data = user_data;
    self->error = error;
    self->stop = FALSE;

    if (json_parser_get_root (self->parser)) {
	parse_node (self, json_parser_get_root (self->parser));
    }
}

static void
finalize (GObject *gobj)
{
    MkJsonParser *self = MK_JSON_PARSER (gobj);

    g_clear_object (&self->parser);
    mk_array_unref (self->name_stack);

    G_OBJECT_CLASS (mk_json_parser_parent_class)->finalize (gobj);
}

static void
mk_json_parser_init (MkJsonParser *self)
{
    self->name_stack = mk_array_new_type (MK_TYPE_STRING);
}

static void
mk_json_parser_class_init (MkJsonParserClass *cls)
{
    MkParserClass *pcls = MK_PARSER_CLASS (cls);
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    pcls->load_file = load_file;
    pcls->load_text = load_text;
    pcls->traverse = traverse;
    gcls->finalize = finalize;
}

MkParser *
mk_json_parser_new ()
{
    return g_object_new (MK_TYPE_JSON_PARSER, NULL);
}
