#define _GNU_SOURCE
#include "mk-string.h"

#include <ctype.h>

/*
 * Hack to allow using field .s as a cast to string type in Vala.
 * It requies compilation with -DMK_STRING_STRUCT option
 */

/**
 * MkString:
 * @s: (type utf8)
 */

#define TRACE 1

typedef struct {
    grefcount rc;
    guint len;
    guint cap;
    gboolean shared;
    gboolean alloca;
} Meta;

const guint mk_string_meta_size = sizeof (Meta);

G_DEFINE_BOXED_TYPE (MkString, mk_string,
		     mk_string_ref,
		     mk_string_unref);

static gint count;
static GHashTable *shared_table;

#if TRACE
static GHashTable *trace_table;

static void
trace_table_free ()
{
    GHashTableIter iter;
    MkString *string;

    if (g_hash_table_size (trace_table)) {
	g_message ("not freed strings: %u", g_hash_table_size (trace_table));
    }

    g_hash_table_iter_init (&iter, trace_table);

    while (g_hash_table_iter_next (&iter, (gpointer *) &string, NULL)) {
	g_message ("\"%s\"", string);
    }

    g_hash_table_unref (trace_table);
}
#endif

static Meta *
string_meta (MkString *self)
{
    return &((Meta *) self)[-1];
}

MkString *
mk_string_new (const gchar *str)
{
    return mk_string_new_length (str, -1);
}

MkString *
mk_string_new_shared (const gchar *str)
{
    MkString *self;

    if (!str) {
	return NULL;
    }

    if (shared_table) {
	self = g_hash_table_lookup (shared_table, str);

	if (self) {
	    return mk_string_ref (self);
	}
    }

    return mk_string_make_shared (mk_string_new (str));
}

MkString *
mk_string_new_shared_len (const gchar *str, gssize len)
{
    if (len >= 0) {
	str = strndupa (str, len);
    }

    return mk_string_new_shared (str);
}

MkString *
mk_string_new_length (const gchar *str, gssize len)
{
    return mk_string_new_full (str, len, -1, NULL);
}

static void
string_to_uint (const GValue *src, GValue *dst)
{
    unsigned long long num;
    gchar *endptr;

    num = strtoull (g_value_get_boxed (src), &endptr, 0);

    if (g_value_get_boxed (src) == endptr) {
	g_warning ("bad number: %llu", num);
    }

    g_value_set_uint (dst, num);
}

static void
string_to_enum (const GValue *src, GValue *dst)
{
    g_autoptr (GEnumClass) cls = NULL;
    MkString *text;
    GEnumValue *value;

    cls = g_type_class_ref (G_VALUE_TYPE (dst));
    text = g_value_get_boxed (src);

    value = g_enum_get_value_by_name (cls, text);

    if (!value) {
	value = g_enum_get_value_by_nick (cls, text);
    }

    if (!value) {
	g_warning ("bad value %s for enum %s",
		   text, G_VALUE_TYPE_NAME (dst));
    }

    g_value_set_enum (dst, value ? value->value : 0);
}

static void
string_to_boolean (const GValue *src, GValue *dst)
{
    static const gchar *true_literals[] = {
	"true",
	"yes",
	"1"
    };

    static const gchar *false_literals[] = {
	"false",
	"no",
	"0"
    };

    const gchar *s = g_value_get_boxed (src);
    guint i;

    for (i = 0; i < G_N_ELEMENTS (true_literals); i++) {
	if (g_strcmp0 (true_literals[i], s) == 0) {
	    g_value_set_boolean (dst, TRUE);
	    return;
	}
    }

    for (i = 0; i < G_N_ELEMENTS (false_literals); i++) {
	if (g_strcmp0 (false_literals[i], s) == 0) {
	    g_value_set_boolean (dst, FALSE);
	    return;
	}
    }

    g_warning ("unknown boolean literal: %s, assuming it's FALSE", s);

    g_value_set_boolean (dst, FALSE);
    return;
}

static gpointer
init (gpointer user_data)
{
    g_value_register_transform_func (MK_TYPE_STRING,
				     G_TYPE_UINT,
				     string_to_uint);
    g_value_register_transform_func (MK_TYPE_STRING,
				     G_TYPE_ENUM,
				     string_to_enum);
    g_value_register_transform_func (MK_TYPE_STRING,
				     G_TYPE_BOOLEAN,
				     string_to_boolean);

    return NULL;
}

MkString *
mk_string_new_full (const gchar *str, gssize len, gssize cap,
		    gpointer alloca_mem)
{
    static GOnce init_once = G_ONCE_INIT;
    MkString *self;

    g_once (&init_once, init, NULL);

    if (!str) {
	str = "";

	if (len > 0) {
	    g_warning ("len > 0 with str NULL");
	    len = -1;
	}
    }

    if (len < 0) {
	len = strlen (str);
    }

    if (cap < 0) {
	cap = len;
    } else {
	g_assert (cap >= len);
    }

    self = alloca_mem ? (alloca_mem + sizeof (Meta)) :
	g_malloc (sizeof (Meta) + cap + 1) + sizeof (Meta);

    g_ref_count_init (&string_meta (self)->rc);

    string_meta (self)->len = len;
    string_meta (self)->cap = cap;
    string_meta (self)->shared = FALSE;
    string_meta (self)->alloca = !!alloca_mem;

    memcpy (self, str, len);
    self[len] = 0;

    count++;

#if TRACE
    if (!alloca_mem) {
	if (!trace_table) {
	    trace_table = g_hash_table_new (g_direct_hash, g_direct_equal);
	    atexit (trace_table_free);
	}

	g_hash_table_add (trace_table, self);
    }
#endif

    return self;
}

MkString *
mk_string_ref (MkString *self)
{
    if (!self) {
	return NULL;
    }

    if (mk_string_is_alloca (self)) {
	return mk_string_new_length (self, mk_string_len (self));
    }

    g_ref_count_inc (&string_meta (self)->rc);

    return self;
}

void
mk_string_unref (MkString *self)
{
    gboolean removed;

    if (self && g_ref_count_dec (&string_meta (self)->rc)) {
	if (mk_string_is_shared (self)) {
	    g_assert (shared_table);
	    removed = g_hash_table_remove (shared_table, self);
	    g_assert (removed);

	    if (!g_hash_table_size (shared_table)) {
		g_clear_pointer (&shared_table, g_hash_table_unref);
	    }
	}

#if TRACE
	g_hash_table_remove (trace_table, self);
#endif

	count--;
	g_free (string_meta (self));
    }
}

/**
 * mk_string_repeat_c:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_repeat_c (MkString *self, gchar c, guint n)
{
    self = mk_string_make_mutable (self, n);
    memset (self, c, n);
    self[n] = 0;

    return self;
}

/**
 * mk_string_data:
 * @len: (out) (optional):
 *
 * returns: (transfer none) (array length=len):
 */
const guint8 *
mk_string_data (MkString *self, gsize *len)
{
    if (len) {
	*len = mk_string_len (self);
    }

    return (const guint8 *) self;
}

const gchar *
mk_string_str (MkString *self)
{
    return self;
}

/**
 * mk_string_set_len:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_set_len (MkString *self, gsize len)
{
    if (len < mk_string_len (self)) {
	self = mk_string_make_mutable (self, -1);
	string_meta (self)->len = len;
	self[len] = 0;
    } else if (len > mk_string_len (self)) {
	self = mk_string_make_mutable (self, len);
	memset (self + mk_string_len (self), ' ',
		len - mk_string_len (self));
	self[len] = 0;
    }

    return self;
}

guint
mk_string_len (MkString *self)
{
    return self ? string_meta (self)->len : 0;
}

guint
mk_string_cap (MkString *self)
{
    return self ? string_meta (self)->cap : 0;
}

gboolean
mk_string_is_mutable (MkString *self)
{
    return self &&
	mk_string_is_single (self);
    /* TODO mutability & sharability */
}

gboolean
mk_string_is_shared (MkString *self)
{
    return self && string_meta (self)->shared;
}

gboolean
mk_string_is_alloca (MkString *self)
{
    return self && string_meta (self)->alloca;
}

gboolean
mk_string_is_single (MkString *self)
{
    return g_ref_count_compare (&string_meta (self)->rc, 1);
}

/**
 * mk_string_is_empty:
 * @self: (nullable)
 */
gboolean
mk_string_is_empty (MkString *self)
{
    return !self || mk_string_len (self) < 1;
}

static gssize
p2min (gssize v)
{
    gssize p2 = 1;

    while (p2 < v) {
	p2 *= 2;
    }

    return v;
}

/**
 * mk_string_make_mutable:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_make_mutable (MkString *self, gssize cap)
{
    MkString *release;
    gboolean removed;

    if (!self) {
	self = mk_string_new_full ("", 0, cap, NULL);
    } else if (mk_string_is_single (self)) {
	if (mk_string_is_shared (self)) {
	    removed = g_hash_table_remove (shared_table, self);
	    g_assert (removed);

	    string_meta (self)->shared = FALSE;
	} else {
	    g_assert (mk_string_is_mutable (self));
	}

	if (mk_string_cap (self) < cap) {
#if TRACE
	    g_hash_table_remove (trace_table, self);
#endif
	    self = g_realloc ((gpointer) self - sizeof (Meta),
			      sizeof (Meta) + p2min (cap) + 1) +
		sizeof (Meta);
#if TRACE
	    g_hash_table_add (trace_table, self);
#endif
	}
    } else {
	release = self;
	self = mk_string_new_full (self, mk_string_len (self),
				   MAX (cap, mk_string_len (self)),
				   NULL);
	mk_string_unref (release);
    }

    g_assert (!mk_string_is_shared (self));
    g_assert (mk_string_is_mutable (self));

    return self;
}

/**
 * mk_string_make_shared:
 * @self: (transfer full):
 *
 * returns: (transfer full)
 */
MkString *
mk_string_make_shared (MkString *self)
{
    MkString *shared;

    if (!self || mk_string_is_shared (self)) {
	return self;
    }

    if (shared_table) {
	shared = g_hash_table_lookup (shared_table, self);

	if (shared) {
	    g_assert (shared != self);

	    mk_string_unref (self);

	    return mk_string_ref (shared);
	}
    } else {
	shared_table = g_hash_table_new (g_str_hash, g_str_equal);
    }

    if (!mk_string_is_single (self)) {
	self = mk_string_make_mutable (self, -1);
    }

    g_assert (mk_string_is_mutable (self));

    string_meta (self)->shared = TRUE;
    g_hash_table_add (shared_table, self);

    return self;
}

/**
 * mk_string_assign:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_assign (MkString *self, const gchar *str)
{
    return mk_string_assign_len (self, str, -1);
}

/**
 * mk_string_assign_len:
 * @self: (inout):
 */
MkString *
mk_string_assign_len (MkString *self, const gchar *str, gssize len)
{
    if (len < 0) {
	len = strlen (str);
    }

    self = mk_string_make_mutable (self, len);
    memcpy (self, str, len);
    self[len] = 0;
    string_meta (self)->len = len;

    return self;
}

/**
 * mk_string_tr_char:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_tr_char (MkString *self, gint (*tr) (gint))
{
    guint i;

    if (!self) {
	return self;
    }

    if (mk_string_is_mutable (self)) {
change:
	for (i = 0; i < mk_string_len (self); i++) {
	    self[i] = tr (self[i]);
	}
    } else {
	for (i = 0; i < mk_string_len (self); i++) {
	    if (self[i] != tr (self[i])) {
		self = mk_string_make_mutable (self, -1);
		goto change;
	    }
	}
    }

    return self;
}

static MkString *
del_char_full (MkString *self,
	       gint (*predicate) (gint, gpointer),
	       gint (*after) (gint),
	       gint (*other) (gint),
	       gboolean force_modify,
	       gpointer user_data)
{
    gboolean begin;
    guint i;

    if (!self) {
	return self;
    }

    if (mk_string_is_mutable (self) || force_modify) {
modify:
	self = mk_string_make_mutable (self, -1);
	begin = TRUE;

	for (i = 0; i < mk_string_len (self); i++) {
	    if (predicate (self[i], user_data)) {
		memmove (&self[i], &self[i + 1],
			 mk_string_len (self) - i + 1);
		string_meta (self)->len--;
		i--;
		begin = TRUE;
		continue;
	    }

	    if (begin ? after : other) {
		self[i] = (begin ? after : other) (self[i]);
	    }

	    begin = FALSE;
	}
    } else {
	for (i = 0; i < mk_string_len (self); i++) {
	    if (predicate (self[i], user_data)) {
		goto modify;
	    }
	}
    }

    return self;
}

static gint
equal_predicate (gint c, gpointer user_data)
{
    return c == GPOINTER_TO_INT (user_data);
}

/**
 * mk_string_del_char:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_del_char (MkString *self, gint c)
{
    return del_char_full (self, equal_predicate,
			  NULL, NULL, FALSE, GINT_TO_POINTER (c));
}

static gint
tr_underscorify (gint c)
{
    return isalpha (c) || isdigit (c) ? c : '_';
}

/**
 * mk_string_underscorify:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_underscorify (MkString *self)
{
    return mk_string_tr_char (self, tr_underscorify);
}

static gint
tolower_under (gint c)
{
    if (isalpha (c)) {
	return tolower (c);
    }

    if (isdigit (c)) {
	return c;
    }

    return '_';
}

static gint
toupper_under (gint c)
{
    if (isalpha (c)) {
	return toupper (c);
    }

    if (isdigit (c)) {
	return c;
    }

    return '_';
}

/**
 * mk_string_tolower:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_tolower (MkString *self)
{
    return mk_string_tr_char (self, tolower_under);
}

/**
 * mk_string_toupper:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_toupper (MkString *self)
{
    return mk_string_tr_char (self, toupper_under);
}

static gint
underscorify_predicate (gint c, gpointer user_data)
{
    return !isalpha (c) && !isdigit (c);
}

/**
 * mk_string_tocamel:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_tocamel (MkString *self)
{
    return del_char_full (self, underscorify_predicate,
			  toupper, tolower, TRUE, NULL);
}

static gint
tocanon (gint c)
{
    if (isalpha (c)) {
	return tolower (c);
    }

    if (isdigit (c)) {
	return c;
    }

    return '-';
}

/**
 * mk_string_tocanon:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_tocanon (MkString *self)
{
    return mk_string_tr_char (self, tocanon);
}

/**
 * mk_string_remove_dash:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_remove_dash (MkString *self)
{
    return del_char_full (self, underscorify_predicate,
			  NULL, NULL, FALSE, NULL);
}

/**
 * mk_string_append_s:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_append_s (MkString *self, const gchar *str)
{
    self = mk_string_make_mutable (self, mk_string_len (self) + strlen (str));

    memcpy (self + mk_string_len (self), str, strlen (str) + 1);
    string_meta (self)->len += strlen (str);

    return self;
}

static MkString *
append_c (MkString *self, gchar c)
{
    self = mk_string_make_mutable (self, mk_string_len (self) + 1);

    self[mk_string_len (self) + 0] = c;
    self[mk_string_len (self) + 1] = 0;

    string_meta (self)->len++;

    return self;
}

/**
 * mk_string_append_c:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_append_c (MkString *self, gchar c)
{
    if (c) {
	return append_c (self, c);
    }

    return self;
}

/**
 * mk_string_append_0:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_append_0 (MkString *self)
{
    return append_c (self, 0);
}

/**
 * mk_string_append_v:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_append_v (MkString *self, const gchar *fmt, va_list args)
{
    gssize measure, written;
    va_list argm;

    va_copy (argm, args);

    measure = g_vsnprintf (NULL, 0, fmt, argm);

    if (measure < 1) {
	return self;
    }

    self = mk_string_make_mutable (self, mk_string_len (self) + measure);

    written = g_vsnprintf (self +
			   mk_string_len (self),
			   measure + 1, fmt, args);

    g_warn_if_fail (written == measure);

    string_meta (self)->len += written;

    return self;
}

/**
 * mk_string_append:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_append (MkString *self, const gchar *fmt, ...)
{
    va_list args;

    va_start (args, fmt);
    self = mk_string_append_v (self, fmt, args);
    va_end (args);

    return self;
}

/**
 * mk_string_prepend_s:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_prepend_s (MkString *self, const gchar *str)
{
    self = mk_string_make_mutable (self, mk_string_len (self) + strlen (str));

    memmove (self + strlen (str), self, mk_string_len (self) + 1);
    memcpy (self, str, strlen (str));

    string_meta (self)->len += strlen (str);

    return self;
}

/**
 * mk_string_prepend_c:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_prepend_c (MkString *self, gchar c)
{
    if (!c) {
	return self;
    }

    self = mk_string_make_mutable (self, mk_string_len (self) + 1);

    memmove (self + 1, self, mk_string_len (self) + 1);
    *self = c;

    string_meta (self)->len++;

    return self;
}

/**
 * mk_string_prepend:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_prepend (MkString *self, const gchar *fmt, ...)
{
    gssize measure, written;
    va_list args;
    gchar first;

    va_start (args, fmt);
    measure = g_vsnprintf (NULL, 0, fmt, args);
    va_end (args);

    g_warn_if_fail (measure > 0);

    self = mk_string_make_mutable (self, mk_string_len (self) + measure);

    va_start (args, fmt);
    memmove (self + measure, self, mk_string_len (self) + 1);
    first = *self;
    written = g_vsnprintf (self, measure + 1, fmt, args);
    self[measure] = first;
    va_end (args);

    g_warn_if_fail (written == measure);

    string_meta (self)->len += written;

    return self;
}

/**
 * mk_string_chomp:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_chomp (MkString *self)
{
    self = mk_string_make_mutable (self, -1);

    g_strchomp (self);

    string_meta (self)->len = strlen (self);

    return self;
}

/**
 * mk_string_substring:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_substring (MkString *self, glong begin, glong length)
{
    g_autoptr (MkString) release = NULL;

    if (begin < 0) {
	begin = mk_string_len (self) - begin;
    }

    if (length < 0) {
	length = mk_string_len (self);
    }

    g_return_val_if_fail (begin >= 0, NULL);

    length = MIN (length, mk_string_len (self) - begin);

    if (begin == 0 && length == mk_string_len (self)) {
	return self;
    }

    if (mk_string_is_single (self)) {
	self = mk_string_make_mutable (self, -1);

	memmove (self, self + begin, length);
	string_meta (self)->len = length;

	return self;
    }

    release = self;

    return mk_string_new_length (self + begin, length);
}

/**
 * mk_string_trunc:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_trunc (MkString *self, glong size)
{
    g_return_val_if_fail (self, NULL);

    if (size < 0) {
	size = MAX (mk_string_len (self) + size, 0);
    }

    g_assert (size >= 0);

    size = MIN (mk_string_len (self), size);
    self = mk_string_make_mutable (self, -1);
    self[size] = 0;

    string_meta (self)->len = size;

    return self;
}

/**
 * mk_string_cut:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_cut (MkString *self, gulong begin, gulong length)
{
    g_return_val_if_fail (self, NULL);

    if (begin >= mk_string_len (self)) {
	return self;
    }

    length = MIN (length, mk_string_len (self) - begin);

    if (!length) {
	return self;
    }

    self = mk_string_make_mutable (self, -1);

    memmove (self + begin, self + begin + length,
	     mk_string_len (self) - begin - length);
    self[string_meta (self)->len -= length] = 0;

    return self;
}

/**
 * mk_string_path_basename:
 * @self: (transfer full):
 *
 * returns: (transfer full):
 */
MkString *
mk_string_path_basename (MkString *self)
{
    gchar *sep;

    g_return_val_if_fail (self, NULL);

    sep = g_strrstr (self, "/");

    if (sep) {
	return mk_string_cut (self, 0, (sep - self) + 1);
    }

    return self;
}

static gboolean
has_prefix_suffix (MkString *self, const gchar *needle, gboolean is_suffix)
{
    const gchar *begin;
    guint len;

    if (!self) {
	return FALSE;
    }

    g_return_val_if_fail (needle, FALSE);

    len = strlen (needle);

    if (mk_string_len (self) < len) {
	return FALSE;
    }

    if (is_suffix) {
	begin = self + mk_string_len (self) - len;
    } else {
	begin = self;
    }

    return memcmp (begin, needle, len) == 0;
}

gboolean
mk_string_has_suffix (MkString *self, const gchar *suffix)
{
    return has_prefix_suffix (self, suffix, TRUE);
}

gboolean
mk_string_has_prefix (MkString *self, const gchar *prefix)
{
    return has_prefix_suffix (self, prefix, FALSE);
}

guint
mk_string_hash (gconstpointer self_p)
{
    MkString *self = (MkString *) self_p;
    const gchar *it;
    guint64 word64;
    guint32 word32, hash;
    guint left, i;

    if (!self) {
	return 0;
    }

    left = mk_string_len (self);
    it = self_p;
    hash = 5381;

    while (left >= 8) {
	word64 = *(const guint64 *) it;
	it += 8;
	left -= 8;

	for (i = 0; i < 8; i++) {
	    hash = (hash << 5) + hash + (word64 & 0xff);
	    word64 >>= 8;
	}
    }

    if (left >= 4) {
	word32 = *(const guint32 *) it;
	it += 4;
	left -= 4;

	for (i = 0; i < 4; i++) {
	    hash = (hash << 5) + hash + (word32 & 0xff);
	    word32 >>= 8;
	}
    }

    if (left > 0) {
	for (i = 0; i < left; i++) {
	    hash = (hash << 5) + hash + *it++;
	    word32 >>= 8;
	}
    }

    return hash;
}

/**
 * mk_string_equal:
 * @self: (nullable)
 * @other: (nullable)
 */
gboolean
mk_string_equal (MkString *self, MkString *other)
{
    if (self == other) {
	return TRUE;
    }

    if (!self || !other) {
	return FALSE;
    }

    if (mk_string_len (self) != mk_string_len (other)) {
	return FALSE;
    }

    return memcmp (self, other, mk_string_len (self)) == 0;
}

gchar
mk_string_char_at (MkString *self, glong index)
{
    if (index < 0) {
	index += mk_string_len (self);
    }

    g_return_val_if_fail (index >= 0, 0);

    return self[index];
}

/**
 * mk_set_string:
 * @location: (inout):
 */
gboolean
mk_set_string (MkString **location, MkString *string)
{
    MkString *prev;

    if (string != *location) {
	prev = *location;

	if (string) {
	    *location = mk_string_ref (string);
	}

	if (prev) {
	    mk_string_unref (prev);
	}

	return TRUE;
    }

    return FALSE;
}

void
mk_clear_string (MkString **location)
{
    g_assert (location);

    if (*location) {
	mk_string_unref (*location);
	*location = NULL;
    }
}
