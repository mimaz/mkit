#pragma once

#include "mk-parser.h"

G_BEGIN_DECLS

#define MK_TYPE_XML_PARSER (mk_xml_parser_get_type ())

G_DECLARE_FINAL_TYPE (MkXmlParser, mk_xml_parser,
		      MK, XML_PARSER, MkParser);

MkParser   *mk_xml_parser_new	(void);

G_END_DECLS
