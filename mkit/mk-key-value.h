#pragma once

#include "mk-string.h"

G_BEGIN_DECLS

#define MK_TYPE_KEY_VALUE (mk_key_value_get_type ())

typedef struct _MkKeyValue MkKeyValue;

GType		mk_key_value_get_type	    (void)
					    G_GNUC_CONST;
MkKeyValue     *mk_key_value_new	    (void);
MkKeyValue     *mk_key_value_ref	    (MkKeyValue *self);
void		mk_key_value_unref	    (MkKeyValue *self);
MkKeyValue     *mk_key_value_clear	    (MkKeyValue *self)
					    G_GNUC_WARN_UNUSED_RESULT;
MkKeyValue     *mk_key_value_add	    (MkKeyValue *self,
					     MkString *key,
					     const GValue *value)
					    G_GNUC_WARN_UNUSED_RESULT;
MkKeyValue     *mk_key_value_add_string	    (MkKeyValue *self,
					     MkString *key,
					     MkString *value)
					    G_GNUC_WARN_UNUSED_RESULT;
gboolean	mk_key_value_is_mutable	    (MkKeyValue *self)
					    G_GNUC_CONST;
void		mk_key_value_qsort	    (MkKeyValue *self);
GValue         *mk_key_value_bsearch	    (MkKeyValue *self,
					     const gchar *key);
GValue         *mk_key_value_find	    (MkKeyValue *self,
					     const gchar *key);
guint		mk_key_value_get_count	    (MkKeyValue *self);
MkString       *mk_key_value_key_at	    (MkKeyValue *self,
					     gint index);
GValue         *mk_key_value_value_at	    (MkKeyValue *self,
					     gint index);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (MkKeyValue, mk_key_value_unref);

#define MK_KEY_VALUE_FOREACH(kv, pair) \
    MK_ARRAY_FOREACH ((MkArray *) kv, \
		      typeof (struct { MkString *key; GValue value; }), \
		      pair)

G_END_DECLS
