#include "mk-key-value.h"
#include "mk-string.h"
#include "mk-array.h"

G_DEFINE_BOXED_TYPE (MkKeyValue, mk_key_value,
		     mk_key_value_ref,
		     mk_key_value_unref);

typedef struct {
    MkString *key;
    GValue value;
} Entry;

static void
entry_copy (Entry *dst, Entry *src)
{
    dst->key = mk_string_ref (src->key);
    dst->value = (GValue) G_VALUE_INIT;

    g_value_init (&dst->value, G_VALUE_TYPE (&src->value));
    g_value_copy (&src->value, &dst->value);
}

static void
entry_unset (Entry *e)
{
    mk_string_unref (e->key);
    g_value_unset (&e->value);
}

MkKeyValue *
mk_key_value_new ()
{
    return (MkKeyValue *) mk_array_new (G_TYPE_NONE,
					sizeof (Entry),
					(MkGenericCopy) entry_copy,
					(MkGenericFree) entry_unset, 0);
}

MkKeyValue *
mk_key_value_ref (MkKeyValue *self)
{
    return (MkKeyValue *) mk_array_ref ((MkArray *) self);
}

void
mk_key_value_unref (MkKeyValue *self)
{
    mk_array_unref ((MkArray *) self);
}

static gint
compare_entries (gconstpointer p0, gconstpointer p1)
{
    return g_strcmp0 (*(const gchar **) p0,
		      *(const gchar **) p1);
}

/**
 * mk_key_value_clear:
 * @self: (transfer full)
 *
 * returns: (transfer full)
 */
MkKeyValue *
mk_key_value_clear (MkKeyValue *self)
{
    return (MkKeyValue *) mk_array_clear ((MkArray *) self);
}

/**
 * mk_key_value_add:
 * @self: (transfer full)
 *
 * returns: (transfer full)
 */
MkKeyValue *
mk_key_value_add (MkKeyValue *self, MkString *key, const GValue *value)
{
    Entry e = {
	key,
	*value,
    };

    return (MkKeyValue *) mk_array_append ((MkArray *) self, &e);
}

/**
 * mk_key_value_add_string:
 * @self: (transfer full)
 *
 * returns: (transfer full)
 */
MkKeyValue *
mk_key_value_add_string (MkKeyValue *self, MkString *key, MkString *value)
{
    g_auto (GValue) v = G_VALUE_INIT;

    g_value_init (&v, MK_TYPE_STRING);
    g_value_set_boxed (&v, value);

    return mk_key_value_add (self, key, &v);
}

gboolean
mk_key_value_is_mutable (MkKeyValue *self)
{
    return mk_array_is_mutable ((MkArray *) self);
}

void
mk_key_value_qsort (MkKeyValue *self)
{
    mk_array_qsort ((MkArray *) self, compare_entries);
}

GValue *
mk_key_value_bsearch (MkKeyValue *self, const gchar *key)
{
    Entry *e = (Entry *) mk_array_bsearch ((MkArray *) self,
					   key, compare_entries);

    return NULL;
    return e ? &e->value : NULL;
}

GValue *
mk_key_value_find (MkKeyValue *self, const gchar *key)
{
    Entry *e;
    guint i;

    for (i = 0; i < mk_array_len ((MkArray *) self); i++) {
	e = mk_array_at ((MkArray *) self, i);

	if (g_strcmp0 (key, e->key) == 0) {
	    return &e->value;
	}
    }

    return NULL;
}

guint
mk_key_value_get_count (MkKeyValue *self)
{
    return mk_array_len ((MkArray *) self);
}

/**
 * mk_key_value_key_at:
 *
 * returns: (transfer none)
 */
MkString *
mk_key_value_key_at (MkKeyValue *self, gint index)
{
    return MK_ARRAY_INDEX ((MkArray *) self, Entry, index).key;
}

/**
 * mk_key_value_value_at:
 *
 * returns: (transfer none)
 */
GValue *
mk_key_value_value_at (MkKeyValue *self, gint index)
{
    return &MK_ARRAY_INDEX ((MkArray *) self, Entry, index).value;
}
