#pragma once

#include "mk-gi.h"

G_BEGIN_DECLS

#define MK_TYPE_GIREPO (mk_girepo_get_type ())

G_DECLARE_FINAL_TYPE (MkGirepo, mk_girepo,
		      MK, GIREPO, GObject);

MkGirepo       *mk_girepo_new			(void);
void		mk_girepo_prepend_path		(MkGirepo *self,
						 const gchar *path);
void		mk_girepo_prepend_default	(MkGirepo *self);
MkGiRepository *mk_girepo_load			(MkGirepo *self,
						 const gchar *name,
						 GError **error);

G_END_DECLS
