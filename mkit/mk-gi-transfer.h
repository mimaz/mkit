#pragma once

#include <mk-enum-gtype.h>

G_BEGIN_DECLS

typedef enum {
    MK_GI_TRANSFER_NONE,
    MK_GI_TRANSFER_FULL,
    MK_GI_TRANSFER_CONTAINER,
} MkGiTransfer;

G_END_DECLS
