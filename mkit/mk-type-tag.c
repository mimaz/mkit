#include "mk-type-tag.h"

gboolean
mk_type_tag_is_value (MkTypeTag tag)
{
    return !mk_type_tag_is_pointer (tag);
}

gboolean
mk_type_tag_is_pointer (MkTypeTag tag)
{
    switch (tag) {
    case MK_TYPE_TAG_BOXED:
    case MK_TYPE_TAG_OBJECT:
    case MK_TYPE_TAG_STRING:
	return TRUE;

    default:
	return FALSE;
    }
}
