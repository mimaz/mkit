#include "mk-decl0.h"
#include "mk-builder.h"

void
mk_decl_init_types ()
{
    (void) g_type_name (MK_TYPE_DECL_CLASS);
    (void) g_type_name (MK_TYPE_DECL_NAMESPACE);
    (void) g_type_name (MK_TYPE_DECL_FIELD);
}
