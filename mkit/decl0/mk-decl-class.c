#include "mk-decl-class.h"
#include "mk-decl-field.h"
#include "mk-array.h"

struct _MkDeclClass
{
    GObject parent_instance;
    MkString *name;
    MkArray *field_array;
};

G_DEFINE_TYPE (MkDeclClass, mk_decl_class, G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_NAME,
    PROP_FIELD_ARRAY,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject *gobj, guint propid, const GValue *value, GParamSpec *spec)
{
    MkDeclClass *self = MK_DECL_CLASS (gobj);

    switch (propid) {
    case PROP_NAME:
	mk_decl_class_set_name (self, g_value_get_boxed (value));
	break;

    case PROP_FIELD_ARRAY:
	mk_decl_class_set_field_array (self, g_value_get_boxed (value));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

static void
get_property (GObject *gobj, guint propid, GValue *value, GParamSpec *spec)
{
    MkDeclClass *self = MK_DECL_CLASS (gobj);

    switch (propid) {
    case PROP_NAME:
	g_value_set_boxed (value, mk_decl_class_get_name (self));
	break;

    case PROP_FIELD_ARRAY:
	g_value_set_boxed (value, mk_decl_class_get_field_array (self));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

static void
finalize (GObject *gobj)
{
    MkDeclClass *self = MK_DECL_CLASS (gobj);

    mk_clear_string (&self->name);
    mk_clear_array (&self->field_array);

    G_OBJECT_CLASS (mk_decl_class_parent_class)->finalize (gobj);
}

static void
mk_decl_class_init (MkDeclClass *self)
{
}

static void
mk_decl_class_class_init (MkDeclClassClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->finalize = finalize;

    props[PROP_NAME] =
	g_param_spec_boxed ("name",
			    "name",
			    "name",
			    MK_TYPE_STRING,
			    G_PARAM_READWRITE |
			    G_PARAM_CONSTRUCT |
			    G_PARAM_STATIC_STRINGS |
			    G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_FIELD_ARRAY] =
	g_param_spec_boxed ("field",
			    "field",
			    "field [tag=fields] "
			    "[element=MkDeclField]",
			    MK_TYPE_ARRAY,
			    G_PARAM_READWRITE |
			    G_PARAM_CONSTRUCT |
			    G_PARAM_STATIC_STRINGS |
			    G_PARAM_EXPLICIT_NOTIFY);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

void
mk_decl_class_set_name (MkDeclClass *self, MkString *name)
{
    if (mk_set_string (&self->name, name)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_NAME]);
    }
}

MkString *
mk_decl_class_get_name (MkDeclClass *self)
{
    return self->name;
}

void
mk_decl_class_set_field_array (MkDeclClass *self, MkArray *field_array)
{
    if (mk_set_array (&self->field_array, field_array)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_FIELD_ARRAY]);
    }
}

MkArray *
mk_decl_class_get_field_array (MkDeclClass *self)
{
    return self->field_array;
}
