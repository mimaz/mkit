#pragma once

#include "mk-string.h"

G_BEGIN_DECLS

#define MK_TYPE_DECL_FIELD (mk_decl_field_get_type ())

G_DECLARE_FINAL_TYPE (MkDeclField, mk_decl_field,
		      MK, DECL_FIELD, GObject);

void		mk_decl_field_set_type	    (MkDeclField *self,
					     MkString *type);
MkString       *mk_decl_field_get_typex	    (MkDeclField *self);
void		mk_decl_field_set_name	    (MkDeclField *self,
					     MkString *name);
MkString       *mk_decl_field_get_name	    (MkDeclField *self);
void		mk_decl_field_set_element   (MkDeclField *self,
					     MkString *element);
MkString       *mk_decl_field_get_element   (MkDeclField *self);
void		mk_decl_field_set_tag	    (MkDeclField *self,
					     MkString *tag);
MkString       *mk_decl_field_get_tag	    (MkDeclField *self);

G_END_DECLS
