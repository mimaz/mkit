#include "mk-decl-field.h"

struct _MkDeclField
{
    GObject parent_instance;
    MkString *type;
    MkString *name;
    MkString *element;
    MkString *tag;
};

G_DEFINE_TYPE (MkDeclField, mk_decl_field, G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_TYPE_NAME,
    PROP_NAME,
    PROP_ELEMENT,
    PROP_TAG,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject *gobj, guint propid, const GValue *value, GParamSpec *spec)
{
    MkDeclField *self = MK_DECL_FIELD (gobj);

    switch (propid) {
    case PROP_TYPE_NAME:
	mk_decl_field_set_type (self, g_value_get_boxed (value));
	break;

    case PROP_NAME:
	mk_decl_field_set_name (self, g_value_get_boxed (value));
	break;

    case PROP_ELEMENT:
	mk_decl_field_set_element (self, g_value_get_boxed (value));
	break;

    case PROP_TAG:
	mk_decl_field_set_tag (self, g_value_get_boxed (value));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

static void
get_property (GObject *gobj, guint propid, GValue *value, GParamSpec *spec)
{
    MkDeclField *self = MK_DECL_FIELD (gobj);

    switch (propid) {
    case PROP_TYPE_NAME:
	g_value_set_boxed (value, mk_decl_field_get_typex (self));
	break;

    case PROP_NAME:
	g_value_set_boxed (value, mk_decl_field_get_name (self));
	break;

    case PROP_ELEMENT:
	g_value_set_boxed (value, mk_decl_field_get_element (self));
	break;

    case PROP_TAG:
	g_value_set_boxed (value, mk_decl_field_get_tag (self));

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

static void
finalize (GObject *gobj)
{
    MkDeclField *self = MK_DECL_FIELD (gobj);

    mk_clear_string (&self->type);
    mk_clear_string (&self->name);
    mk_clear_string (&self->element);
    mk_clear_string (&self->tag);

    G_OBJECT_CLASS (mk_decl_field_parent_class)->finalize (gobj);
}

static void
mk_decl_field_init (MkDeclField *self)
{

}

static void
mk_decl_field_class_init (MkDeclFieldClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->finalize = finalize;

    props[PROP_TYPE_NAME] = g_param_spec_boxed ("type",
						"type",
						"type",
						MK_TYPE_STRING,
						G_PARAM_READWRITE |
						G_PARAM_CONSTRUCT |
						G_PARAM_STATIC_STRINGS |
						G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_NAME] = g_param_spec_boxed ("name",
					   "name",
					   "name",
					   MK_TYPE_STRING,
					   G_PARAM_READWRITE |
					   G_PARAM_CONSTRUCT |
					   G_PARAM_STATIC_STRINGS |
					   G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_ELEMENT] = g_param_spec_boxed ("element",
					      "element",
					      "element",
					      MK_TYPE_STRING,
					      G_PARAM_READWRITE |
					      G_PARAM_CONSTRUCT |
					      G_PARAM_STATIC_STRINGS |
					      G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_TAG] = g_param_spec_boxed ("tag",
					  "tag",
					  "tag",
					  MK_TYPE_STRING,
					  G_PARAM_READWRITE |
					  G_PARAM_CONSTRUCT |
					  G_PARAM_STATIC_STRINGS |
					  G_PARAM_EXPLICIT_NOTIFY);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

void
mk_decl_field_set_type (MkDeclField *self, MkString *type)
{
    if (mk_set_string (&self->type, type)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_TYPE_NAME]);
    }
}

MkString *
mk_decl_field_get_typex (MkDeclField *self)
{
    return self->type;
}

void
mk_decl_field_set_name (MkDeclField *self, MkString *name)
{
    if (mk_set_string (&self->name, name)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_NAME]);
    }
}

MkString *
mk_decl_field_get_name (MkDeclField *self)
{
    return self->name;
}

void
mk_decl_field_set_element (MkDeclField *self, MkString *element)
{
    if (mk_set_string (&self->element, element)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_ELEMENT]);
    }
}

MkString *
mk_decl_field_get_element (MkDeclField *self)
{
    return self->element;
}

void
mk_decl_field_set_tag (MkDeclField *self, MkString *tag)
{
    if (mk_set_string (&self->tag, tag)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_TAG]);
    }
}

MkString *
mk_decl_field_get_tag (MkDeclField *self)
{
    return self->tag;
}
