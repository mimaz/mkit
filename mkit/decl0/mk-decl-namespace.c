#include "mk-decl-namespace.h"
#include "mk-decl-class.h"
#include "mk-array.h"

struct _MkDeclNamespace
{
    GObject parent_instance;
    MkString *name;
    MkString *identifier;
    MkString *symbol;
    MkString *macro;
    MkString *module;
    MkString *version;
    MkString *header;
    MkArray *class_array;
    MkArray *using_array;
};

G_DEFINE_TYPE (MkDeclNamespace, mk_decl_namespace, G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_NAME,
    PROP_IDENTIFIER,
    PROP_SYMBOL,
    PROP_MACRO,
    PROP_MODULE,
    PROP_VERSION,
    PROP_HEADER,
    PROP_CLASS_ARRAY,
    PROP_USING_ARRAY,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject *gobj, guint id, const GValue *value, GParamSpec *spec)
{
    MkDeclNamespace *self = MK_DECL_NAMESPACE (gobj);

    switch (id) {
    case PROP_NAME:
	mk_decl_namespace_set_name (self, g_value_get_boxed (value));
	break;

    case PROP_IDENTIFIER:
	mk_decl_namespace_set_identifier (self, g_value_get_boxed (value));
	break;

    case PROP_SYMBOL:
	mk_decl_namespace_set_symbol (self, g_value_get_boxed (value));
	break;

    case PROP_MACRO:
	mk_decl_namespace_set_macro (self, g_value_get_boxed (value));
	break;

    case PROP_MODULE:
	mk_decl_namespace_set_module (self, g_value_get_boxed (value));
	break;

    case PROP_VERSION:
	mk_decl_namespace_set_version (self, g_value_get_boxed (value));
	break;

    case PROP_HEADER:
	mk_decl_namespace_set_header (self, g_value_get_boxed (value));
	break;

    case PROP_CLASS_ARRAY:
	mk_decl_namespace_set_class_array (self, g_value_get_boxed (value));
	break;

    case PROP_USING_ARRAY:
	mk_decl_namespace_set_using_array (self, g_value_get_boxed (value));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
get_property (GObject *gobj, guint id, GValue *value, GParamSpec *spec)
{
    MkDeclNamespace *self = MK_DECL_NAMESPACE (gobj);

    switch (id) {
    case PROP_NAME:
	g_value_set_boxed (value, mk_decl_namespace_get_name (self));
	break;

    case PROP_IDENTIFIER:
	g_value_set_boxed (value, mk_decl_namespace_get_identifier (self));
	break;

    case PROP_SYMBOL:
	g_value_set_boxed (value, mk_decl_namespace_get_symbol (self));
	break;

    case PROP_MACRO:
	g_value_set_boxed (value, mk_decl_namespace_get_macro (self));
	break;

    case PROP_MODULE:
	g_value_set_boxed (value, mk_decl_namespace_get_module (self));
	break;

    case PROP_VERSION:
	g_value_set_boxed (value, mk_decl_namespace_get_version (self));
	break;

    case PROP_HEADER:
	g_value_set_boxed (value, mk_decl_namespace_get_header (self));
	break;

    case PROP_CLASS_ARRAY:
	g_value_set_object (value, mk_decl_namespace_get_class_array (self));
	break;

    case PROP_USING_ARRAY:
	g_value_set_object (value, mk_decl_namespace_get_using_array (self));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
finalize (GObject *gobj)
{
    MkDeclNamespace *self = MK_DECL_NAMESPACE (gobj);

    mk_clear_string (&self->name);
    mk_clear_string (&self->identifier);
    mk_clear_string (&self->symbol);
    mk_clear_string (&self->macro);
    mk_clear_string (&self->module);
    mk_clear_string (&self->version);
    mk_clear_string (&self->header);
    mk_clear_array (&self->class_array);
    mk_clear_array (&self->using_array);

    G_OBJECT_CLASS (mk_decl_namespace_parent_class)->finalize (gobj);
}

static void
mk_decl_namespace_init (MkDeclNamespace *self)
{
}

static void
mk_decl_namespace_class_init (MkDeclNamespaceClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->finalize = finalize;

    props[PROP_NAME] = g_param_spec_boxed ("name",
					   "name",
					   "name",
					   MK_TYPE_STRING,
					   G_PARAM_READWRITE |
					   G_PARAM_CONSTRUCT |
					   G_PARAM_STATIC_STRINGS |
					   G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_IDENTIFIER] = g_param_spec_boxed ("identifier",
						 "identifier",
						 "identifier",
						 MK_TYPE_STRING,
						 G_PARAM_READWRITE |
						 G_PARAM_CONSTRUCT |
						 G_PARAM_STATIC_STRINGS |
						 G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_SYMBOL] = g_param_spec_boxed ("symbol",
					     "symbol",
					     "symbol",
					     MK_TYPE_STRING,
					     G_PARAM_READWRITE |
					     G_PARAM_CONSTRUCT |
					     G_PARAM_STATIC_STRINGS |
					     G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_MACRO] = g_param_spec_boxed ("macro",
					    "macro",
					    "macro",
					    MK_TYPE_STRING,
					    G_PARAM_READWRITE |
					    G_PARAM_CONSTRUCT |
					    G_PARAM_STATIC_STRINGS |
					    G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_MODULE] = g_param_spec_boxed ("module",
					     "module",
					     "module",
					     MK_TYPE_STRING,
					     G_PARAM_READWRITE |
					     G_PARAM_CONSTRUCT |
					     G_PARAM_STATIC_STRINGS |
					     G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_VERSION] = g_param_spec_boxed ("version",
					      "version",
					      "version",
					      MK_TYPE_STRING,
					      G_PARAM_READWRITE |
					      G_PARAM_CONSTRUCT |
					      G_PARAM_STATIC_STRINGS |
					      G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_HEADER] = g_param_spec_boxed ("header",
					     "header",
					     "header",
					     MK_TYPE_STRING,
					     G_PARAM_READWRITE |
					     G_PARAM_CONSTRUCT |
					     G_PARAM_STATIC_STRINGS |
					     G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_CLASS_ARRAY] = g_param_spec_boxed ("class-array",
						  "class-array",
						  "class [tag=classes]"
						  " [element=MkDeclClass]",
						  MK_TYPE_ARRAY,
						  G_PARAM_READWRITE |
						  G_PARAM_CONSTRUCT |
						  G_PARAM_STATIC_STRINGS |
						  G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_USING_ARRAY] = g_param_spec_boxed ("using-array",
						  "using-array",
						  "using [tag=using] "
						  "[element=MkString]",
						  MK_TYPE_ARRAY,
						  G_PARAM_READWRITE |
						  G_PARAM_CONSTRUCT |
						  G_PARAM_STATIC_STRINGS |
						  G_PARAM_EXPLICIT_NOTIFY);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

void
mk_decl_namespace_set_name (MkDeclNamespace *self, MkString *name)
{
    if (mk_set_string (&self->name, name)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_NAME]);
    }
}

MkString *
mk_decl_namespace_get_name (MkDeclNamespace *self)
{
    return self->name;
}

void
mk_decl_namespace_set_identifier (MkDeclNamespace *self, MkString *identifier)
{
    if (mk_set_string (&self->identifier, identifier)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_IDENTIFIER]);
    }
}

MkString *
mk_decl_namespace_get_identifier (MkDeclNamespace *self)
{
    return self->identifier;
}

void
mk_decl_namespace_set_symbol (MkDeclNamespace *self, MkString *symbol)
{
    if (mk_set_string (&self->symbol, symbol)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_SYMBOL]);
    }
}

MkString *
mk_decl_namespace_get_symbol (MkDeclNamespace *self)
{
    return self->symbol;
}

void
mk_decl_namespace_set_macro (MkDeclNamespace *self, MkString *macro)
{
    if (mk_set_string (&self->macro, macro)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_MACRO]);
    }
}

MkString *
mk_decl_namespace_get_macro (MkDeclNamespace *self)
{
    return self->macro;
}

void
mk_decl_namespace_set_module (MkDeclNamespace *self, MkString *module)
{
    if (mk_set_string (&self->module, module)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_MODULE]);
    }
}

MkString *
mk_decl_namespace_get_module (MkDeclNamespace *self)
{
    return self->module;
}

void
mk_decl_namespace_set_version (MkDeclNamespace *self, MkString *version)
{
    if (mk_set_string (&self->version, version)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_VERSION]);
    }
}

MkString *
mk_decl_namespace_get_version (MkDeclNamespace *self)
{
    return self->version;
}

void
mk_decl_namespace_set_header (MkDeclNamespace *self, MkString *header)
{
    if (mk_set_string (&self->header, header)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_HEADER]);
    }
}

MkString *
mk_decl_namespace_get_header (MkDeclNamespace *self)
{
    return self->header;
}

void
mk_decl_namespace_set_class_array (MkDeclNamespace *self, MkArray *class_array)
{
    if (mk_set_array (&self->class_array, class_array)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_CLASS_ARRAY]);
    }
}

MkArray *
mk_decl_namespace_get_class_array (MkDeclNamespace *self)
{
    return self->class_array;
}

void
mk_decl_namespace_set_using_array (MkDeclNamespace *self, MkArray *using_array)
{
    if (mk_set_array (&self->using_array, using_array)) {
	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_USING_ARRAY]);
    }
}

MkArray *
mk_decl_namespace_get_using_array (MkDeclNamespace *self)
{
    return self->using_array;
}
