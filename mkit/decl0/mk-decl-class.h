#pragma once

#include "mk-string.h"

G_BEGIN_DECLS

typedef struct _MkDeclField MkDeclField;
typedef struct _MkDeclItem MkDeclItem;
typedef struct _MkArray MkArray;

#define MK_TYPE_DECL_CLASS (mk_decl_class_get_type ())

G_DECLARE_FINAL_TYPE (MkDeclClass, mk_decl_class,
		      MK, DECL_CLASS, GObject);

void		mk_decl_class_set_name		    (MkDeclClass *self,
						     MkString *name);
MkString       *mk_decl_class_get_name		    (MkDeclClass *self);
void		mk_decl_class_set_field_array	    (MkDeclClass *self,
						     MkArray *field_array);
MkArray	       *mk_decl_class_get_field_array	    (MkDeclClass *self);

G_END_DECLS
