#pragma once

#include "mk-string.h"

G_BEGIN_DECLS

typedef struct _MkArray MkArray;
typedef struct _MkDeclClass MkDeclClass;

#define MK_TYPE_DECL_NAMESPACE (mk_decl_namespace_get_type ())

G_DECLARE_FINAL_TYPE (MkDeclNamespace, mk_decl_namespace,
		      MK, DECL_NAMESPACE, GObject);

void		mk_decl_namespace_set_name		(MkDeclNamespace *self,
							 MkString *name);
MkString       *mk_decl_namespace_get_name		(MkDeclNamespace *self);
void		mk_decl_namespace_set_identifier	(MkDeclNamespace *self,
							 MkString *identifier);
MkString       *mk_decl_namespace_get_identifier	(MkDeclNamespace *self);
void		mk_decl_namespace_set_symbol		(MkDeclNamespace *self,
							 MkString *symbol);
MkString       *mk_decl_namespace_get_symbol		(MkDeclNamespace *self);
void		mk_decl_namespace_set_macro		(MkDeclNamespace *self,
							 MkString *macro);
MkString       *mk_decl_namespace_get_macro		(MkDeclNamespace *self);
void		mk_decl_namespace_set_module		(MkDeclNamespace *self,
							 MkString *module);
MkString       *mk_decl_namespace_get_module		(MkDeclNamespace *self);
void		mk_decl_namespace_set_version		(MkDeclNamespace *self,
							 MkString *version);
MkString       *mk_decl_namespace_get_version		(MkDeclNamespace *self);
void		mk_decl_namespace_set_header		(MkDeclNamespace *self,
							 MkString *header);
MkString       *mk_decl_namespace_get_header		(MkDeclNamespace *self);
void		mk_decl_namespace_set_class_array	(MkDeclNamespace *self,
							 MkArray *class_array);
MkArray        *mk_decl_namespace_get_class_array	(MkDeclNamespace *self);
void		mk_decl_namespace_set_using_array	(MkDeclNamespace *self,
							 MkArray *using_array);
MkArray        *mk_decl_namespace_get_using_array	(MkDeclNamespace *self);

G_END_DECLS
