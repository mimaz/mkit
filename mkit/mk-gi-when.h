#pragma once

#include <mk-enum-gtype.h>

G_BEGIN_DECLS

typedef enum {
    MK_GI_WHEN_FIRST,
    MK_GI_WHEN_CLEANUP,
    MK_GI_WHEN_LAST,
} MkGiWhen;

G_END_DECLS

