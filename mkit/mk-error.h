#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define MK_ERROR (mk_error_quark ())

typedef enum {
    MK_ERROR_NONE,
    MK_ERROR_UNKNOWN,
    MK_ERROR_PARSE,
    MK_ERROR_BAD_FORMAT,
    MK_ERROR_BAD_TYPE,
    MK_ERROR_BAD_ARGUMENT,
    MK_ERROR_BAD_VALUE,
    MK_ERROR_NO_SUCH_ATTRIBUTE,
    MK_ERROR_MISSING,
    MK_ERROR_NOT_OVERRIDEN,
    MK_ERROR_INVALID_VALUE,
    MK_ERROR_INDEX_OUT_OF_SCOPE,
    MK_ERROR_ALREADY_EXISTS,
    MK_ERROR_NO_SUCH_VALUE,
    MK_ERROR_NO_SUCH_FILE,
    MK_ERROR_NO_SUCH_PACKAGE,
    MK_ERROR_NO_SUCH_PROPERTY,
    MK_ERROR_SUCCESS,
    MK_ERROR_GIR_NOT_FOUND,
    MK_ERROR_UNUSED,
} MkError;

GQuark	    mk_error_quark	(void) G_GNUC_CONST;
gboolean    mk_error_is	(GError *error,
				 MkError code);
gboolean    mk_error_is_any	(GError *error,
				 guint num_codes,
				 const MkError *codes);
gboolean    mk_error_is_anyv	(GError *eror,
				 MkError code,
				 ...);

G_END_DECLS
