#include "mk-init.h"
#include "mkit.h"

void
mk_init_types ()
{
    (void) g_type_name (MK_TYPE_ARRAY);
    (void) g_type_name (MK_TYPE_GI_TRANSFER);
    (void) g_type_name (MK_TYPE_GI_DIRECTION);
}
