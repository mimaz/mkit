#pragma once

#include <mk-enum-gtype.h>

G_BEGIN_DECLS

typedef enum {
    MK_SCOPE_CALL,
    MK_SCOPE_ASYNC,
    MK_SCOPE_NOTIFIED,
} MkGiScope;

G_END_DECLS
