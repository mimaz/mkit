#pragma once

#include "mk-string.h"

G_BEGIN_DECLS

typedef enum {
    MK_PARSER_BEGIN_OBJECT,
    MK_PARSER_END_OBJECT,
    MK_PARSER_VALUE,
} MkParserAction;

typedef struct _MkParser MkParser;

typedef void (*MkParserTraverse) (MkParserAction action,
				  MkString *name,
				  const GValue *data,
				  gpointer user_data,
				  GError **error);

#define MK_TYPE_PARSER (mk_parser_get_type ())

G_DECLARE_DERIVABLE_TYPE (MkParser, mk_parser,
			  MK, PARSER, GObject);

struct _MkParserClass
{
    GObjectClass parent_class;

    void (*load_file)	(MkParser *self, const gchar *path,
			 GError **error);
    void (*load_text)	(MkParser *self, const gchar *text, gssize len,
			 GError **error);
    void (*traverse)	(MkParser *self, MkParserTraverse traverse,
			 gpointer user_data, GError **error);
};

void		mk_parser_load_file	(MkParser *self,
					 const gchar *path,
					 GError **error);
void		mk_parser_load_text	(MkParser *self,
					 const gchar *text,
					 gssize len,
					 GError **error);
void		mk_parser_traverse	(MkParser *self,
					 MkParserTraverse traverse,
					 gpointer user_data,
					 GError **error);

G_END_DECLS
