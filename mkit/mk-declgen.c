#include "mk-declgen.h"

#include "mk-type-table.h"
#include "mk-type-info.h"

#include "mk-error.h"
#include "mk-code.h"
#include "mk-array.h"

#ifndef MKIT_NO_CONFIG
#include "mk-config.h"
#endif

#ifdef MKIT_HAS_DECL
#include "mk-decl.h"
#else
#include "mk-decl0.h"
#endif

#include <ctype.h>

#define clear() *code = mk_string_assign (*code, "");
#define emit(fmt, ...) *code = mk_string_append (*code, fmt, ##__VA_ARGS__)

static void
get_string_property (MkString **value, gpointer object,
		     const gchar *prop, GError **error)
{
    g_auto (GValue) read = G_VALUE_INIT;

    if (!G_IS_OBJECT (object)) {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "given object is not GObject");
	return;
    }

    g_object_get_property (object, prop, &read);

    mk_clear_string (value);

    if (G_VALUE_HOLDS (&read, MK_TYPE_STRING)) {
	*value = g_value_dup_boxed (&read);
    } else if (G_VALUE_HOLDS_STRING (&read)) {
	*value = mk_string_new (g_value_get_string (&read));
    } else {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "property %s is not string", prop);
    }
}

#define get_string_property_or(value, object, prop, error, expr) \
    do { \
	GError *local_error = NULL; \
	get_string_property (value, object, prop, &local_error); \
	if (local_error) { \
	    g_propagate_error (error, local_error); \
	    expr; \
	} \
    } while (0)

typedef struct {
    MkString *header_code;
    MkString *source_code;
    MkTypeTable *type_table;
    GHashTable *lower_cache;
    GHashTable *upper_cache;
} MkDeclgenPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (MkDeclgen, mk_declgen,
			    G_TYPE_OBJECT);

static void
finalize (GObject *gobj)
{
    MkDeclgen *self = MK_DECL_CODEGEN (gobj);
    MkDeclgenPrivate *p = mk_declgen_get_instance_private (self);

    mk_clear_string (&p->header_code);
    mk_clear_string (&p->source_code);
    g_object_unref (p->type_table);
    g_hash_table_unref (p->lower_cache);
    g_hash_table_unref (p->upper_cache);

    G_OBJECT_CLASS (mk_declgen_parent_class)->finalize (gobj);
}

static void
mk_declgen_init (MkDeclgen *self)
{
    MkDeclgenPrivate *p = mk_declgen_get_instance_private (self);

    p->type_table = mk_type_table_new ();
    p->lower_cache = g_hash_table_new_full (mk_string_hash,
					    (GEqualFunc)
					    mk_string_equal,
					    (GDestroyNotify)
					    mk_string_unref,
					    (GDestroyNotify)
					    mk_string_unref);
    p->upper_cache = g_hash_table_new_full (mk_string_hash,
					    (GEqualFunc)
					    mk_string_equal,
					    (GDestroyNotify)
					    mk_string_unref,
					    (GDestroyNotify)
					    mk_string_unref);
}

static void
mk_declgen_class_init (MkDeclgenClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->finalize = finalize;
}

MkDeclgen *
mk_declgen_new ()
{
    return g_object_new (MK_TYPE_DECL_CODEGEN, NULL);
}

typedef struct {
    MkDeclgen *self;
    MkDeclgenPrivate *p;

    MkDeclNamespace *namespace;
    MkDeclClass *class;
#ifndef MKIT_BOOTSTRAP_DECL
    MkDeclInterface *iface;
#endif
    MkDeclField *property;

    GHashTable *class_data_table;
    GHashTable *info_data_table;
} GenerateContext;

static MkString *
to_lowercase (GenerateContext *gc, MkString *s)
{
    MkString *l = g_hash_table_lookup (gc->p->lower_cache, s);

    if (!l) {
	/*g_message ("miss");*/
	l = mk_string_tolower (mk_string_ref (s));

	g_hash_table_insert (gc->p->lower_cache,
			     mk_string_ref (s), l);
    } else {
	/*g_message ("hit!");*/
    }

    return l;
}

static MkString *
to_uppercase (GenerateContext *gc, MkString *s)
{
    MkString *l = g_hash_table_lookup (gc->p->upper_cache, s);

    if (!l) {
	/*g_message ("miss");*/
	l = mk_string_toupper (mk_string_ref (s));

	g_hash_table_insert (gc->p->upper_cache,
			     mk_string_ref (s), l);
    } else {
	/*g_message ("hit!");*/
    }

    return l;
}

typedef struct {
    grefcount rc;
    GObject *class;
    MkTypeInfo *info;
    MkTypeInfo *parent;
    MkArray *implements;
    MkArray *fields;
    GHashTable *owner_table;
} ClassData;

static ClassData *
class_data_new (GenerateContext *gc, GObject *class,
		MkTypeInfo *info, MkTypeInfo *parent)
{
    ClassData *cd;

    cd = g_slice_new (ClassData);

    g_ref_count_init (&cd->rc);

    cd->class = g_object_ref (class);
    cd->info = g_object_ref (info);
    cd->parent = g_object_ref (parent);
    cd->implements = mk_array_new_type (MK_TYPE_STRING);
    cd->fields = mk_array_new_type (MK_TYPE_DECL_FIELD);
    cd->owner_table = g_hash_table_new (g_direct_hash, g_direct_equal);

    return cd;
}

static ClassData *
class_data_ref (ClassData *cd)
{
    g_ref_count_inc (&cd->rc);

    return cd;
}

static void
class_data_unref (ClassData *cd)
{
    if (g_ref_count_dec (&cd->rc)) {
	g_object_unref (cd->class);
	g_object_unref (cd->info);
	g_object_unref (cd->parent);
	mk_array_unref (cd->implements);
	mk_array_unref (cd->fields);
	g_hash_table_unref (cd->owner_table);
	g_slice_free (ClassData, cd);
    }
}

static gboolean
remove_field_name (gpointer element, gpointer user_data, GError **error)
{
    g_assert (MK_IS_DECL_FIELD (element));

    return mk_string_equal (mk_decl_field_get_name (MK_DECL_FIELD (element)),
			    user_data);
}

static void
class_data_add_field (ClassData *cd, MkDeclField *field)
{
    cd->fields = mk_array_remove_if (cd->fields, remove_field_name,
				     mk_decl_field_get_name (field),
				     NULL);
    cd->fields = mk_array_append (cd->fields, field);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ClassData, class_data_unref);

static GenerateContext *
generate_context_new (MkDeclgen *self)
{
    GenerateContext *gc = g_slice_new0 (GenerateContext);

    g_set_object (&gc->self, self);
    gc->p = mk_declgen_get_instance_private (self);

    gc->class_data_table = g_hash_table_new_full (g_direct_hash,
						  g_direct_equal,
						  g_object_unref,
						  (GDestroyNotify)
						  class_data_unref);
    gc->info_data_table = g_hash_table_new_full (g_direct_hash,
						 g_direct_equal,
						 g_object_unref,
						 (GDestroyNotify)
						 class_data_unref);

    return gc;
}

static void
generate_context_free (GenerateContext *gc)
{
    g_clear_object (&gc->self);
    g_clear_object (&gc->namespace);
    g_clear_object (&gc->class);
#ifndef MKIT_BOOTSTRAP_DECL
    g_clear_object (&gc->iface);
#endif
    g_clear_object (&gc->property);

    g_hash_table_unref (gc->class_data_table);
    g_hash_table_unref (gc->info_data_table);

    g_slice_free (GenerateContext, gc);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (GenerateContext, generate_context_free);

static const gchar *
resolve_g_value_suffix (MkTypeInfo *info, GError **error)
{
    switch (mk_type_info_get_tag (info)) {
    case MK_TYPE_TAG_BOXED:
	return "boxed";

    case MK_TYPE_TAG_OBJECT:
	return "object";

    case MK_TYPE_TAG_BOOLEAN:
	return "boolean";

    case MK_TYPE_TAG_INT32:
	return "int";

    case MK_TYPE_TAG_UINT32:
	return "uint";

    case MK_TYPE_TAG_FLOAT:
	return "float";

    case MK_TYPE_TAG_DOUBLE:
	return "double";

    case MK_TYPE_TAG_ENUM:
	return "enum";

    default:
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "bad tag: %d", mk_type_info_get_tag (info));
	return "";
    }
}

#ifndef MKIT_BOOTSTRAP_DECL
static void
forward_iface (GenerateContext *gc, MkString **code,
	       MkDeclInterface *iface, GError **error)
{
    g_autoptr (MkString) ns_name = NULL;
    g_autoptr (MkString) cl_name = NULL;
    g_autoptr (MkArray) tmp = NULL;
    g_autoptr (ClassData) cd = NULL;
    const gchar *parent_name = NULL;
    GError *local_error = NULL;
    MkTypeInfo *class_info, *parent_info;

    g_set_object (&gc->iface, iface);

    get_string_property_or (&ns_name, gc->namespace, "name", error, return);
    get_string_property_or (&cl_name, gc->iface, "name", error, return);

    parent_name = "GObject.Object";
    parent_info = mk_type_table_resolve (gc->p->type_table,
					 parent_name,
					 &local_error);
    g_assert (!local_error);

    class_info = mk_type_table_register_type (gc->p->type_table,
					      MK_TYPE_TAG_OBJECT,
					      parent_info,
					      0, NULL,
					      ns_name, cl_name,
					      NULL, NULL, NULL, NULL, NULL);

    cd = class_data_new (gc, G_OBJECT (iface), class_info, parent_info);

    MK_ARRAY_FOREACH (mk_decl_interface_get_field_array (iface),
		      MkDeclField *, field) {
	class_data_add_field (cd, field);

	g_hash_table_insert (cd->owner_table, field, iface);
    }

    g_hash_table_insert (gc->class_data_table,
			 g_object_ref (iface),
			 class_data_ref (cd));
    g_hash_table_insert (gc->info_data_table,
			 g_object_ref (class_info),
			 class_data_ref (cd));

    emit ("typedef struct _%s %s;\n",
	  mk_type_info_get_c_name (class_info),
	  mk_type_info_get_c_name (class_info));
}
#endif

static void
forward_class (GenerateContext *gc, MkString **code,
	       MkDeclClass *class, GError **error)
{
    g_autoptr (MkString) ns_name = NULL;
    g_autoptr (MkString) cl_name = NULL;
    g_autoptr (MkArray) tmp = NULL;
    g_autoptr (ClassData) cd = NULL;
    const gchar *parent_name = NULL;
    GError *local_error = NULL;
    MkTypeInfo *class_info, *parent_info;
    MkDeclClass *parent_class;
    ClassData *parent_data;

    g_set_object (&gc->class, class);

    get_string_property_or (&ns_name, gc->namespace, "name", error, return);
    get_string_property_or (&cl_name, gc->class, "name", error, return);

#ifndef MKIT_BOOTSTRAP_DECL
    parent_name = mk_decl_class_get_extends (class);
#else
    parent_name = NULL;
#endif

    if (!parent_name) {
	parent_name = "GObject.Object";
    }

    parent_info = mk_type_table_resolve (gc->p->type_table,
					 parent_name,
					 &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }

    parent_data = g_hash_table_lookup (gc->info_data_table, parent_info);

    class_info = mk_type_table_register_type (gc->p->type_table,
					      MK_TYPE_TAG_OBJECT,
					      parent_info,
					      0, NULL,
					      ns_name, cl_name,
					      NULL, NULL, NULL, NULL, NULL);

    cd = class_data_new (gc, G_OBJECT (class), class_info, parent_info);

    if (parent_data) {
	parent_class = MK_DECL_CLASS (parent_data->class);

	MK_ARRAY_FOREACH (parent_data->fields, MkDeclField *, field) {
	    class_data_add_field (cd, field);

	    g_hash_table_insert (cd->owner_table, field, parent_class);
	}
    }

    MK_ARRAY_FOREACH (mk_decl_class_get_field_array (class),
		      MkDeclField *, field) {
	class_data_add_field (cd, field);

	g_hash_table_insert (cd->owner_table, field, class);
    }

#ifndef MKIT_BOOTSTRAP_DECL
    MK_ARRAY_FOREACH (mk_decl_class_get_implement_array (class),
		      MkString *, implement) {
	MkTypeInfo *iface_info;
	ClassData *icd;

	iface_info = mk_type_table_resolve (gc->p->type_table, implement,
					    &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	icd = g_hash_table_lookup (gc->info_data_table,
				   iface_info);
	g_assert (icd);
	g_assert (MK_IS_DECL_INTERFACE (icd->class));

	MK_ARRAY_FOREACH (icd->fields, MkDeclField *, field) {
	    class_data_add_field (cd, field);

	    g_hash_table_insert (cd->owner_table, field,
				 MK_DECL_INTERFACE (icd->class));
	}
    }
#endif

    g_hash_table_insert (gc->class_data_table,
			 g_object_ref (class),
			 class_data_ref (cd));
    g_hash_table_insert (gc->info_data_table,
			 g_object_ref (class_info),
			 class_data_ref (cd));

    emit ("typedef struct _%s %s;\n",
	  mk_type_info_get_c_name (class_info),
	  mk_type_info_get_c_name (class_info));
}

#ifndef MKIT_BOOTSTRAP_DECL
static void
override_iface_property (GenerateContext *gc,
			 MkString **code,
			 ClassData *cd,
			 MkString *name,
			 MkString *name_lower,
			 MkString *name_upper)
{
    emit ("g_object_class_override_property (<align>gcls,\n"
	  "%s_PROP_%s,\n"
	  "\"%s\"</align>);\n",
	  mk_type_info_get_c_macro (cd->info),
	  name_upper, name);

    emit ("%s_prop_%s =\n"
	  "<indent>"
	  "g_object_class_find_property "
	  "(<align>gcls,\n\"%s\"</align>);\n"
	  "</indent>",
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower, name);
}

static void
override_class_property (GenerateContext *gc)
{

}
#endif

static void
define_property (GenerateContext *gc,
		 MkString **out_code,
		 ClassData *cd,
		 GObject *object,
		 MkDeclField *field,
		 MkTypeInfo *info,
		 MkString **blurb,
		 gboolean with_enum,
		 MkString *name,
		 MkString *name_lower,
		 MkString *name_upper,
		 GError **error)
{
    GError *local_error = NULL;
    MkTypeInfo *element_info;
    MkString *element_name, **code;
    const gchar *suffix;

    suffix = resolve_g_value_suffix (info, &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }

    code = blurb;

    clear ();
    emit ("\"%s\"", name);

    if (mk_decl_field_get_tag (field)) {
	emit ("\n\" [tag=%s]\"",
	      mk_decl_field_get_tag (field));
    }

    element_name = mk_decl_field_get_element (field);

    if (element_name) {
	element_info = mk_type_table_resolve (gc->p->type_table,
					      element_name,
					      &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	emit ("\n\" [element=%s]\"",
	      mk_type_info_get_glib_name (element_info));
    }

#ifndef MKIT_BOOTSTRAP_DECL
    if (mk_key_value_get_count (mk_decl_field_get_classes (field))) {
	emit ("\n\" [classes=");

	MK_KEY_VALUE_FOREACH (mk_decl_field_get_classes (field), tag_class) {
	    if (!G_VALUE_HOLDS (&tag_class.value, MK_TYPE_STRING)) {
		g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
			     "classes table must contain only strings");
		return;
	    }

	    element_name = g_value_get_boxed (&tag_class.value);
	    element_info = mk_type_table_resolve (gc->p->type_table,
						  element_name,
						  &local_error);

	    if (local_error) {
		g_propagate_error (error, local_error);
		return;
	    }

	    emit ("%s%s=%s", MK_ARRAY_FOREACH_FIRST (tag_class) ? "" : ",",
		  tag_class.key, mk_type_info_get_glib_name (element_info));
	}

	emit ("]\"");
    }
#endif

    code = out_code;

    emit ("%s_prop_%s =\n"
	  "<indent>"
	  "g_param_spec_%s (<align>\"%s\",\n"
	  "\"%s\",\n"
	  "%s,\n",
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower, suffix,
	  name, name, *blurb);

    switch (mk_type_info_get_tag (info)) {
    case MK_TYPE_TAG_BOXED:
    case MK_TYPE_TAG_OBJECT:
	emit ("%s,\n",
	      mk_type_info_get_glib_type_macro (info));
	break;

    case MK_TYPE_TAG_INT8:
	emit ("G_MININT8, G_MAXINT8, 0,\n");
	break;

    case MK_TYPE_TAG_UINT8:
	emit ("0, G_MAXINT8, 0,\n");
	break;

    case MK_TYPE_TAG_INT32:
	emit ("G_MININT32, G_MAXINT32, 0,\n");
	break;

    case MK_TYPE_TAG_UINT32:
	emit ("0, G_MAXUINT32, 0,\n");
	break;

    case MK_TYPE_TAG_BOOLEAN:
	emit ("FALSE,\n");
	break;

    case MK_TYPE_TAG_ENUM:
	emit ("%s, 0,\n",
	      mk_type_info_get_glib_type_macro (info));
	break;

    default:
	g_warning ("unhandled tag");
    }

    emit ("G_PARAM_READWRITE |\n"
	  "G_PARAM_CONSTRUCT |\n"
	  "G_PARAM_STATIC_STRINGS |\n"
	  "G_PARAM_EXPLICIT_NOTIFY</align>);\n"
	  "</indent>");

#ifndef MKIT_BOOTSTRAP_DECL
    if (MK_IS_DECL_INTERFACE (object)) {
	emit ("g_object_interface_install_property (iface,\n"
	      "<indent>");
    } else {
#endif
	emit ("g_object_class_install_property (gcls,\n"
	      "<indent>");
#ifndef MKIT_BOOTSTRAP_DECL
    }
#endif

    if (with_enum) {
	emit ("%s_PROP_%s,\n",
	      mk_type_info_get_c_macro (cd->info),
	      name_upper);
    }

    emit ("%s_prop_%s);\n"
	  "</indent>",
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower);
}

static void
generate_param_specs (GenerateContext *gc,
		      GObject *object,
		      MkArray *array_prop,
		      const gchar *cls,
		      gboolean with_enum,
		      MkString **spec_code,
		      MkString **install_code,
		      GError **error)
{
    g_autoptr (MkString) blurb = NULL;
    GError *local_error = NULL;
    MkTypeInfo *info;
    MkString *name, *name_lower, *name_upper, **code;
    ClassData *cd;
    gpointer owner;

    cd = g_hash_table_lookup (gc->class_data_table, object);
    g_assert (cd);

    code = install_code;

    MK_ARRAY_FOREACH (array_prop, MkDeclField *, field) {
	name = mk_decl_field_get_name (field);
	owner = g_hash_table_lookup (cd->owner_table, field);

	if (!owner) {
	    g_error ("no owner for prop %s", name);
	}

	if (owner != object && MK_IS_DECL_CLASS (owner)) {
	    continue;
	}

	name_lower = to_lowercase (gc, name);
	name_upper = to_uppercase (gc, name);

	code = spec_code;

	emit ("static GParamSpec *%s_prop_%s;\n",
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower);

	code = install_code;

	info = mk_type_table_resolve (gc->p->type_table,
				      mk_decl_field_get_typex (field),
				      &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

#ifndef MKIT_BOOTSTRAP_DECL
	if (owner != object) {
	    if (MK_IS_DECL_CLASS (object) &&
		MK_IS_DECL_INTERFACE (owner)) {
		override_iface_property (gc, code, cd, name,
					 name_lower, name_upper);
	    } else if (MK_IS_DECL_CLASS (object) &&
		       MK_IS_DECL_CLASS (owner)) {
		override_class_property (gc);
	    } else {
		g_error ("owner object: %s %s",
			 g_type_name (G_TYPE_FROM_INSTANCE (owner)),
			 g_type_name (G_TYPE_FROM_INSTANCE (object)));
	    }
	} else {
	    define_property (gc, code, cd, object, field, info, &blurb,
			     with_enum, name, name_lower, name_upper,
			     &local_error);
	}
#else
	g_assert (owner == object);

	define_property (gc, code, cd, object, field, info, &blurb,
			 with_enum, name, name_lower, name_upper,
			 &local_error);
#endif

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}
    }
}

#ifndef MKIT_BOOTSTRAP_DECL
static void
generate_iface (GenerateContext *gc, MkString **h_code, MkString **c_code,
		MkDeclInterface *iface, GError **error)
{
    g_autoptr (MkArray) tmp = NULL;
    g_autoptr (MkArray) base_fields = NULL;
    g_autoptr (MkString) arg_id = NULL;
    g_autoptr (MkString) prop_spec_code = NULL;
    g_autoptr (MkString) prop_install_code = NULL;
    GError *local_error = NULL;
    MkTypeInfo *parent_info, *prop_info;
    ClassData *cd;
    MkString **code, *name, *name_lower;

    g_set_object (&gc->iface, iface);

    cd = g_hash_table_lookup (gc->class_data_table, iface);
    g_assert (cd);

    parent_info = mk_type_info_get_parent (cd->info);
    g_assert (parent_info);

    generate_param_specs (gc, G_OBJECT (iface),
			  mk_decl_interface_get_field_array (iface),
			  "iface", FALSE,
			  &prop_spec_code,
			  &prop_install_code,
			  &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }

    code = h_code;

    emit ("#define %s (%s_get_type ())\n",
	  mk_type_info_split (cd->info, &tmp,
			      "c-macro", "%s_TYPE_%s"),
	  mk_type_info_get_c_symbol (cd->info));

    emit ("G_DECLARE_INTERFACE (<align>%s, %s,\n"
	  "%s, GObject</align>);\n",
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_split (cd->info, &tmp,
			      "c-macro", "%s, %s"));

    emit ("struct _%sInterface\n"
	  "{\n"
	  "<indent>"
	  "GTypeInterface parent_iface;\n",
	  mk_type_info_get_c_name (cd->info));

    MK_ARRAY_FOREACH (mk_decl_interface_get_field_array (iface),
		      MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name_lower = to_lowercase (gc, name);

	emit ("void (*set_%s) (<align>GObject *self,\n%s</align>);\n"
	      "%s(*get_%s) (GObject *self);\n",
	      name_lower,
	      mk_type_info_c_arg (prop_info, &tmp, NULL, name_lower),
	      mk_type_info_c_arg (prop_info, &tmp, NULL, NULL),
	      name_lower);
    }

    emit ("</indent>"
	  "};\n");


    MK_ARRAY_FOREACH (mk_decl_interface_get_field_array (iface),
		      MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name_lower = to_lowercase (gc, name);

	emit ("void %s_set_%s (<align>%s *self,\n%s</align>);\n"
	      "%s%s_get_%s%s (%s *self);\n",
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower,
	      mk_type_info_get_c_name (cd->info),
	      mk_type_info_c_arg (prop_info, &tmp, NULL, name_lower),
	      mk_type_info_c_arg (prop_info, &tmp, NULL, NULL),
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower,
	      g_strcmp0 (name_lower, "type") ? "" : "x",
	      mk_type_info_get_c_name (cd->info));
    }

    code = c_code;

    emit ("G_DEFINE_INTERFACE (%s, %s, G_TYPE_OBJECT);\n",
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info));

    MK_ARRAY_FOREACH (mk_decl_interface_get_field_array (iface),
		      MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name_lower = to_lowercase (gc, name);

	emit ("static GParamSpec *%s_prop_%s;\n",
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower);
    }

    emit ("static void\n"
	  "%s_default_init (%sInterface *iface)\n"
	  "{\n"
	  "<indent>"
	  "%s"
	  "</indent>"
	  "}\n",
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  prop_install_code ? prop_install_code : "");

    MK_ARRAY_FOREACH (mk_decl_interface_get_field_array (iface),
		      MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name_lower = to_lowercase (gc, name);

	emit ("void\n"
	      "%s_set_%s (<align>%s *self,\n%s</align>)\n"
	      "{\n"
	      "<indent>"
	      "%s_GET_IFACE (self)->set_%s (G_OBJECT (self), %s);\n"
	      "</indent>"
	      "}\n"
	      "%s\n"
	      "%s_get_%s%s (%s *self)\n"
	      "{\n"
	      "<indent>"
	      "return %s_GET_IFACE (self)->get_%s (G_OBJECT (self));\n"
	      "</indent>"
	      "}\n",
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower,
	      mk_type_info_get_c_name (cd->info),
	      mk_type_info_c_arg (prop_info, &tmp, NULL, name_lower),
	      mk_type_info_get_c_macro (cd->info),
	      name_lower, name_lower,
	      mk_type_info_c_arg (prop_info, &tmp, NULL, NULL),
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower,
	      g_strcmp0 (name_lower, "type") ? "" : "x",
	      mk_type_info_get_c_name (cd->info),
	      mk_type_info_get_c_macro (cd->info),
	      name_lower);
    }
}
#endif

static void
implement_accessors (GenerateContext *gc,
		     MkString **code,
		     ClassData *cd,
		     MkTypeInfo *prop_info,
		     MkString *name,
		     MkString *name_lower,
		     MkString *name_upper,
		     MkString **arg_id,
		     MkArray **tmp)
{
    emit ("void\n"
	  "%s_set_%s (<align>%s *self,\n"
	  "%s</align>)\n"
	  "{\n"
	  "<indent>"
	  "%sPrivate *p = %s_get_instance_private (self);\n"
	  "if (",
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower,
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_c_arg (prop_info, tmp, arg_id, name_lower),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info));

    if (mk_type_info_is_utf8 (prop_info)) {
	emit ("g_strcmp0 (p->p_%s, %s)",
	      name_lower, *arg_id);
    } else {
	emit ("p->p_%s != %s",
	      name_lower, *arg_id);
    }

    emit (") {\n"
	  "<indent>");

    if (mk_type_info_is_pointer (prop_info)) {
	emit ("g_clear_pointer (&p->p_%s, %s);\n",
	      name_lower,
	      mk_type_info_get_unref_symbol (prop_info));
    }

    emit ("p->p_%s = ", name_lower); 

    if (mk_type_info_is_pointer (prop_info)) {
	emit ("%s ? %s (%s) : %s;\n",
	      *arg_id,
	      mk_type_info_get_ref_symbol (prop_info),
	      *arg_id,
	      *arg_id);
    } else {
	emit ("%s;\n", *arg_id);
    }

    emit ("g_object_notify_by_pspec (<align>G_OBJECT (self),\n"
	  "%s_prop_%s</align>);\n"
	  "</indent>"
	  "}\n",
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower);

    emit ("</indent>"
	  "}\n");

    if (mk_type_info_is_pointer (prop_info)) {
	emit ("/**\n"
	      " * %s_get_%s:\n"
	      " *\n"
	      " * returns: (transfer none)\n"
	      " */\n",
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower);
    }

    emit ("%s\n"
	  "%s_get_%s%s (%s *self)\n"
	  "{\n"
	  "<indent>"
	  "%sPrivate *p = %s_get_instance_private (self);\n"
	  "return p->p_%s;\n"
	  "</indent>"
	  "}\n",
	  mk_type_info_c_arg (prop_info, tmp, NULL, NULL),
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower,
	  g_strcmp0 (name_lower, "type") ? "" : "x",
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower);

}

static void
implement_forward_accessors (GenerateContext *gc,
			     MkString **code,
			     MkTypeInfo *prop_info,
			     MkTypeInfo *parent_info,
			     ClassData *cd,
			     MkString *name,
			     MkString *name_lower,
			     MkString **arg_id,
			     MkArray **tmp)
{
    emit ("void\n"
	  "%s_set_%s (<align>%s *self,\n"
	  "%s</align>)\n"
	  "{\n"
	  "<indent>"
	  "%s_set_%s (%s (self), %s);\n"
	  "</indent>"
	  "}\n",
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower,
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_c_arg (prop_info, tmp, arg_id, name_lower),
	  mk_type_info_get_c_symbol (parent_info),
	  name_lower,
	  mk_type_info_get_c_macro (parent_info),
	  name_lower);

    if (mk_type_info_is_pointer (prop_info)) {
	emit ("/**\n"
	      " * %s_get_%s:\n"
	      " *\n"
	      " * returns: (transfer none)\n"
	      " */\n",
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower);
    }

    emit ("%s\n"
	  "%s_get_%s%s (%s *self)\n"
	  "{\n"
	  "<indent>"
	  "return %s_get_%s (%s (self));\n"
	  "</indent>"
	  "}\n",
	  mk_type_info_c_arg (prop_info, tmp, NULL, NULL),
	  mk_type_info_get_c_symbol (cd->info),
	  name_lower,
	  g_strcmp0 (name_lower, "type") ? "" : "x",
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (parent_info),
	  name_lower,
	  mk_type_info_get_c_macro (parent_info));
}

static void
generate_class (GenerateContext *gc, MkString **h_code, MkString **c_code,
		MkDeclClass *class, GError **error)
{
    g_autoptr (MkArray) tmp = NULL;
    g_autoptr (MkArray) base_fields = NULL;
    g_autoptr (MkString) arg_id = NULL;
    g_autoptr (MkString) prop_spec_code = NULL;
    g_autoptr (MkString) prop_install_code = NULL;
    GError *local_error = NULL;
    MkTypeInfo *parent_info, *prop_info;
    ClassData *cd;
    gpointer owner;
    MkString **code, *name, *name_lower, *name_upper;
#ifndef MKIT_BOOTSTRAP_DECL
    MkTypeInfo *iface_info;
#endif

    g_set_object (&gc->class, class);

    cd = g_hash_table_lookup (gc->class_data_table, class);
    g_assert (cd);

    parent_info = mk_type_info_get_parent (cd->info);
    g_assert (parent_info);

    generate_param_specs (gc, G_OBJECT (class),
			  cd->fields,
			  "gcls", TRUE,
			  &prop_spec_code,
			  &prop_install_code,
			  &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }

    code = h_code;

    emit ("#define %s (%s ())\n"
	  "G_DECLARE_DERIVABLE_TYPE (<align>%s, %s,\n"
	  "%s, %s</align>);\n",
	  mk_type_info_get_glib_type_macro (cd->info),
	  mk_type_info_get_glib_get_type (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_split (cd->info, &tmp,
			      "c-macro", "%s, %s"),
	  mk_type_info_get_c_name (parent_info));

    emit ("struct _%sClass\n{\n"
	  "<indent>"
	  "GObjectClass parent_class;\n"
	  "</indent>"
	  "};\n",
	  mk_type_info_get_c_name (cd->info));

    MK_ARRAY_FOREACH (cd->fields, MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name_lower = to_lowercase (gc, name);

	emit ("void %s_set_%s (<align>%s *self,\n%s</align>);\n"
	      "%s%s_get_%s%s (%s *self);\n",
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower,
	      mk_type_info_get_c_name (cd->info),
	      mk_type_info_c_arg (prop_info, &tmp, NULL, name_lower),
	      mk_type_info_c_arg (prop_info, &tmp, NULL, NULL),
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower,
	      g_strcmp0 (name_lower, "type") ? "" : "x",
	      mk_type_info_get_c_name (cd->info));
    }

    code = c_code;

    emit ("typedef struct {\n"
	  "<indent>");

    MK_ARRAY_FOREACH (cd->fields, MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name_lower = to_lowercase (gc, name);

	emit ("%s;\n",
	      mk_type_info_c_arg (prop_info, &tmp, NULL,
				  "p_%s", name_lower));
    }

    if (!mk_array_len (cd->fields)) {
	emit ("gint dummy;\n");
    }

    emit ("</indent>"
	  "} %sPrivate;\n",
	  mk_type_info_get_c_name (cd->info));

#ifndef MKIT_BOOTSTRAP_DECL
    MK_ARRAY_FOREACH (mk_decl_class_get_implement_array (class),
		      MkString *, implement) {
	ClassData *icd;

	iface_info = mk_type_table_resolve (gc->p->type_table,
					    implement,
					    &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	icd = g_hash_table_lookup (gc->info_data_table, iface_info);

	emit ("static void\n"
	      "%s_init_%s (%sInterface *iface)\n"
	      "{\n"
	      "<indent>",
	      mk_type_info_get_c_symbol (cd->info),
	      mk_type_info_get_c_symbol (iface_info),
	      mk_type_info_get_c_name (iface_info));

	MK_ARRAY_FOREACH (icd->fields, MkDeclField *, prop) {
	    name = mk_decl_field_get_name (prop);
	    name_lower = to_lowercase (gc, name);

	    emit ("iface->set_%s = (gpointer) %s_set_%s;\n",
		  name_lower,
		  mk_type_info_get_c_symbol (cd->info),
		  name_lower);

	    emit ("iface->get_%s = (gpointer) %s_get_%s;\n",
		  name_lower,
		  mk_type_info_get_c_symbol (cd->info),
		  name_lower);
	}

	emit ("</indent>"
	      "}\n");
    }
#endif

    emit ("G_DEFINE_TYPE_WITH_CODE (<align>%s, %s,\n"
	  "%s,\n"
	  "G_ADD_PRIVATE (%s)",
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_glib_type_macro (parent_info),
	  mk_type_info_get_c_name (cd->info));

#ifndef MKIT_BOOTSTRAP_DECL
    MK_ARRAY_FOREACH (mk_decl_class_get_implement_array (class),
		      MkString *, implement) {
	iface_info = mk_type_table_resolve (gc->p->type_table,
					    implement,
					    &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	emit ("\nG_IMPLEMENT_INTERFACE (%s,\n"
	      "    %s_init_%s)",
	      mk_type_info_get_glib_type_macro (iface_info),
	      mk_type_info_get_c_symbol (cd->info),
	      mk_type_info_get_c_symbol (iface_info));
    }
#endif

    emit ("</align>);\n");

    emit ("enum\n"
	  "{\n"
	  "<indent>"
	  "%s_PROP_0,\n",
	  mk_type_info_get_c_macro (cd->info));

    MK_ARRAY_FOREACH (cd->fields, MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	name_upper = to_uppercase (gc, name);

	emit ("%s_PROP_%s,\n",
	      mk_type_info_get_c_macro (cd->info),
	      name_upper);
    }

    emit ("</indent>"
	  "};\n");

    emit ("%s", prop_spec_code ? prop_spec_code : "");

    emit ("static void\n"
	  "v_%s_set_property (<align>GObject *gobj, guint id,\n"
	  "const GValue *value, GParamSpec *spec</align>)\n"
	  "{\n"
	  "<indent>"
	  "%s *self = %s (gobj);\n"
	  "(void) self;\n"
	  "switch (id) {\n",
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_macro (cd->info));

    MK_ARRAY_FOREACH (cd->fields, MkDeclField *, prop) {
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name = mk_decl_field_get_name (prop);
	name_lower = to_lowercase (gc, name);
	name_upper = to_uppercase (gc, name);

	emit ("case %s_PROP_%s:\n"
	      "<indent>"
	      "%s_set_%s (self, g_value_get_%s (value));\n",
	      mk_type_info_get_c_macro (cd->info),
	      name_upper,
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower,
	      resolve_g_value_suffix (prop_info, &local_error));

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	emit ("break;\n"
	      "</indent>");
    }

    emit ("default:\n"
	  "<indent>"
	  "G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);\n"
	  "break;\n"
	  "</indent>"
	  "}\n"
	  "</indent>"
	  "}\n");

    emit ("static void\n"
	  "v_%s_get_property (<align>GObject *gobj, guint id,\n"
	  "GValue *value, GParamSpec *spec</align>)\n"
	  "{\n"
	  "<indent>"
	  "%s *self = %s (gobj);\n"
	  "(void) self;\n"
	  "switch (id) {\n",
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_macro (cd->info));

    MK_ARRAY_FOREACH (cd->fields, MkDeclField *, prop) {
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name = mk_decl_field_get_name (prop);
	name_lower = to_lowercase (gc, name);
	name_upper = to_uppercase (gc, name);

	emit ("case %s_PROP_%s:\n"
	      "<indent>"
	      "g_value_set_%s (value, %s_get_%s%s (self));\n",
	      mk_type_info_get_c_macro (cd->info),
	      name_upper,
	      resolve_g_value_suffix (prop_info, &local_error),
	      mk_type_info_get_c_symbol (cd->info),
	      name_lower,
	      g_strcmp0 (name_lower, "type") ? "" : "x");

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	emit ("break;\n"
	      "</indent>");
    }

    emit ("default:\n"
	  "<indent>"
	  "G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);\n"
	  "break;\n"
	  "</indent>"
	  "}\n"
	  "</indent>"
	  "}\n");

    emit ("static void\n"
	  "v_%s_finalize (GObject *gobj)\n"
	  "{\n"
	  "<indent>"
	  "%s *self = %s (gobj);\n"
	  "%sPrivate *p = %s_get_instance_private (self);\n"
	  "(void) p;\n",
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_macro (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info));

    MK_ARRAY_FOREACH (cd->fields, MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name_lower = to_lowercase (gc, name);

	if (mk_type_info_is_pointer (prop_info)) {
	    emit ("g_clear_pointer (&p->p_%s, %s);\n",
		  name_lower,
		  mk_type_info_get_unref_symbol (prop_info));
	}
    }

    emit ("G_OBJECT_CLASS (%s_parent_class)->finalize (gobj);\n"
	  "</indent>"
	  "}\n",
	  mk_type_info_get_c_symbol (cd->info));

    emit ("static void\n"
	  "%s_init (%s *self)\n"
	  "{}\n"
	  "static void\n"
	  "%s_class_init (%sClass *cls)\n"
	  "{\n"
	  "<indent>"
	  "GObjectClass *gcls = G_OBJECT_CLASS (cls);\n"
	  "gcls->set_property = v_%s_set_property;\n"
	  "gcls->get_property = v_%s_get_property;\n"
	  "gcls->finalize = v_%s_finalize;\n"
	  "%s"
	  "</indent>"
	  "}\n",
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_c_name (cd->info),
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_c_symbol (cd->info),
	  mk_type_info_get_c_symbol (cd->info),
	  prop_install_code ? prop_install_code : "");

    MK_ARRAY_FOREACH (cd->fields, MkDeclField *, prop) {
	name = mk_decl_field_get_name (prop);
	prop_info = mk_type_table_resolve (gc->p->type_table,
					   mk_decl_field_get_typex (prop),
					   &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	name_lower = to_lowercase (gc, name);
	name_upper = to_uppercase (gc, name);

	owner = g_hash_table_lookup (cd->owner_table, prop);

#ifndef MKIT_BOOTSTRAP_DECL
	if (owner == class || MK_IS_DECL_INTERFACE (owner)) {
#else
	if (owner == class) {
#endif
	    implement_accessors (gc, code, cd, prop_info,
				 name, name_lower, name_upper,
				 &arg_id, &tmp);
	} else if (MK_IS_DECL_CLASS (owner)) {
	    implement_forward_accessors (gc, code, prop_info, parent_info,
					 cd, name, name_lower,
					 &arg_id, &tmp);
	} else {
	    g_error ("bad owner: %p", owner);
	}
    }
}

void
mk_declgen_generate (MkDeclgen *self,
		     MkDeclNamespace *namespace,
		     GError **error)
{
    MkDeclgenPrivate *p = mk_declgen_get_instance_private (self);
    g_autoptr (GenerateContext) gc = NULL;
    g_autoptr (MkString) h_code = NULL;
    g_autoptr (MkString) c_code = NULL;
    g_autoptr (MkString) header_code = NULL;
    g_autoptr (MkString) source_code = NULL;
    g_autoptr (MkString) module_part = NULL;
    g_autoslist (MkString) tmp_strings = NULL;
    g_autofree gchar *namespace_lower = NULL;
    GError *local_error = NULL;
    MkString *name, *identifier, *symbol, *macro, *module, **code;
    MkSpaceInfo *si;
    ClassData *cd;

    gc = generate_context_new (self);

    g_set_object (&gc->namespace, namespace);

    MK_ARRAY_FOREACH (mk_decl_namespace_get_using_array (namespace),
		      MkString *, using) {
	mk_type_table_include (gc->p->type_table, using);
    }

    name = mk_decl_namespace_get_name (namespace);

    g_assert (name);

    si = mk_type_table_get_ns (gc->p->type_table, name);
    identifier = mk_decl_namespace_get_identifier (namespace);
    macro = mk_decl_namespace_get_macro (namespace);
    symbol = mk_decl_namespace_get_symbol (namespace);
    module = mk_decl_namespace_get_module (namespace);

    if (!si) {
	if (!identifier) {
	    identifier = mk_string_tocamel (mk_string_ref (name));
	    tmp_strings = g_slist_prepend (tmp_strings, identifier);
	}

	if (!symbol) {
	    symbol = to_lowercase (gc, name);
	}

	if (!macro) {
	    macro = to_uppercase (gc, name);
	}

	si = mk_type_table_register_ns (gc->p->type_table, name,
					identifier, symbol, macro);
    }

    module_part = mk_string_new ("");

    if (module) {
	module_part = mk_string_append (module_part, "_%s", module);
	module_part = mk_string_tolower (module_part);
    }

    h_code = mk_string_new (NULL);
    c_code = mk_string_new (NULL);

    code = &h_code;

    emit ("#pragma once\n"
	  "#include <mk-decl.h>\n"
	  "#include <mk-string.h>\n"
	  "#include <mk-key-value.h>\n"
	  "#include <mk-array.h>\n");

#ifndef MKIT_BOOTSTRAP_DECL
    MK_ARRAY_FOREACH (mk_decl_namespace_get_include_array (namespace),
		      MkString *, include) {
	emit ("#include \"%s\"\n", include);
    }

    MK_ARRAY_FOREACH (mk_decl_namespace_get_interface_array (namespace),
		      MkDeclInterface *, iface) {
	forward_iface (gc, &h_code, iface, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}
    }
#endif

    MK_ARRAY_FOREACH (mk_decl_namespace_get_class_array (namespace),
		      MkDeclClass *, class) {
	forward_class (gc, &h_code, class, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}
    }

    emit ("void %s%s_init_types (void);\n",
	  symbol, module_part);

    code = &c_code;

    emit ("#include \"%s\"\n",
	  mk_decl_namespace_get_header (namespace));

    emit ("void\n"
	  "%s%s_init_types (void)\n"
	  "{\n"
	  "<indent>",
	  symbol, module_part);

#ifndef MKIT_BOOTSTRAP_DECL
    MK_ARRAY_FOREACH (mk_decl_namespace_get_interface_array (namespace),
		      MkDeclInterface *, iface) {
	cd = g_hash_table_lookup (gc->class_data_table, iface);

	emit ("(void) %s_get_type ();\n",
	      mk_type_info_get_c_symbol (cd->info));
    }
#endif

    MK_ARRAY_FOREACH (mk_decl_namespace_get_class_array (namespace),
		      MkDeclClass *, class) {
	cd = g_hash_table_lookup (gc->class_data_table, class);

	emit ("(void) %s_get_type ();\n",
	      mk_type_info_get_c_symbol (cd->info));
    }

    emit ("</indent>"
	  "}\n");

#ifndef MKIT_BOOTSTRAP_DECL
    MK_ARRAY_FOREACH (mk_decl_namespace_get_interface_array (namespace),
		      MkDeclInterface *, iface) {
	generate_iface (gc, &h_code, &c_code, iface, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}
    }
#endif

    MK_ARRAY_FOREACH (mk_decl_namespace_get_class_array (namespace),
		      MkDeclClass *, class) {
	generate_class (gc, &h_code, &c_code, class, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}
    }

    mk_code_format (h_code, &p->header_code, &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }

    mk_code_format (c_code, &p->source_code, &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }
}

MkString *
mk_declgen_get_header (MkDeclgen *self)
{
    MkDeclgenPrivate *p = mk_declgen_get_instance_private (self);

    return mk_string_ref (p->header_code);
}

MkString *
mk_declgen_get_source (MkDeclgen *self)
{
    MkDeclgenPrivate *p = mk_declgen_get_instance_private (self);

    return mk_string_ref (p->source_code);
}
