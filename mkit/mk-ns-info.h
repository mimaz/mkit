#pragma once

#include "mk-string.h"

G_BEGIN_DECLS

#define MK_TYPE_SPACE_INFO (mk_ns_info_get_type ())

G_DECLARE_FINAL_TYPE (MkSpaceInfo, mk_ns_info,
		      MK, SPACE_INFO, GObject);

MkSpaceInfo  *mk_ns_info_new		(MkString *name,
					 MkString *c_name,
					 MkString *c_symbol,
					 MkString *c_macro);
MkString     *mk_ns_info_get_name	(MkSpaceInfo *self);
MkString     *mk_ns_info_get_c_name	(MkSpaceInfo *self);
MkString     *mk_ns_info_get_c_symbol   (MkSpaceInfo *self);
MkString     *mk_ns_info_get_c_macro	(MkSpaceInfo *self);

G_END_DECLS
