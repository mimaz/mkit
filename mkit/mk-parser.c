#include "mk-parser.h"

G_DEFINE_TYPE (MkParser, mk_parser, G_TYPE_OBJECT);

static void
mk_parser_init (MkParser *self)
{

}

static void
mk_parser_class_init (MkParserClass *cls)
{

}

void
mk_parser_load_file (MkParser *self, const gchar *path,
		     GError **error)
{
    MK_PARSER_GET_CLASS (self)->load_file (self, path, error);
}

void
mk_parser_load_text (MkParser *self, const gchar *text, gssize len,
		     GError **error)
{
    MK_PARSER_GET_CLASS (self)->load_text (self, text, len, error);
}

void
mk_parser_traverse (MkParser *self, MkParserTraverse traverse,
		    gpointer user_data, GError **error)
{
    MK_PARSER_GET_CLASS (self)->traverse (self, traverse, user_data, error);
}
