#pragma once

#include "mk-parser.h"

G_BEGIN_DECLS

#define MK_TYPE_BUILDER (mk_builder_get_type ())

G_DECLARE_DERIVABLE_TYPE (MkBuilder, mk_builder,
			  MK, BUILDER, GObject);

struct _MkBuilderClass
{
    GObjectClass parent_class;
};

MkBuilder  *mk_builder_new	    (void);
void	    mk_builder_include	    (MkBuilder *self,
				     const gchar *namespace);
gpointer    mk_builder_build	    (MkBuilder *self,
				     MkParser *parser,
				     GType type,
				     GError **error);

G_END_DECLS
