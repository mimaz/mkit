#include "mk-builder.h"
#include "mk-error.h"
#include "mk-array.h"
#include "mk-type-table.h"
#include "mk-type-info.h"
#include "mk-key-value.h"
#include "mk-parse.h"

typedef struct {
    GHashTable *ti_table;
    GHashTable *ai_cache;
    MkTypeTable *type_table;
    MkArray *params_cache;
    MkArray *scope_stack;
    GType root_type;
    GValue *root_object;
} MkBuilderPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (MkBuilder, mk_builder, G_TYPE_OBJECT);

static void
find_attribute (const gchar *text, const gchar *key,
		const gchar **begin, const gchar **end)
{
    gchar seq[strlen (key) + 3];
    const gchar *sep, *stop;

    g_snprintf (seq, sizeof (seq), "[%s=", key);

    sep = strstr (text, seq);

    if (!sep) {
	*begin = NULL;
	*end = NULL;
	return;
    }

    sep += strlen (key) + 2;
    stop = strchr (sep, ']');

    if (!stop) {
	*begin = NULL;
	*end = NULL;
	return;
    }

    *begin = sep;
    *end = stop;
}

typedef struct {
    grefcount rc;
    MkString *tag;
    MkString *name;
    MkString *prop;
    MkKeyValue *classes;
    gboolean is_array;
    GType type;
    GType element_type;
} AttrInfo;

static AttrInfo *
attr_info_ref (AttrInfo *ai)
{
    g_ref_count_inc (&ai->rc);

    return ai;
}

static AttrInfo *
attr_info_new (MkString *tag, MkString *name,
	       MkString *prop, MkKeyValue *classes,
	       gboolean is_array, GType type, GType element_type)
{
    AttrInfo *ai;

    ai = g_slice_new (AttrInfo);

    g_ref_count_init (&ai->rc);

    ai->tag = mk_string_ref (tag);
    ai->name = mk_string_ref (name);
    ai->prop = mk_string_ref (prop);
    ai->classes = classes ? mk_key_value_ref (classes) : NULL;
    ai->is_array = is_array;
    ai->type = type;
    ai->element_type = element_type;

    return ai;
}

static MkKeyValue *
make_classes (const gchar *begin, const gchar *end, GError **error)
{
    g_autoptr (MkKeyValue) classes = NULL;
    g_autoptr (MkString) tag = NULL;
    g_autoptr (MkString) class = NULL;
    const gchar *eq, *co;
    guint left;

    classes = mk_key_value_new ();
    left = end - begin;

    do {
	eq = memchr (begin, '=', left);
	g_assert (eq);
	left -= eq - begin;
	co = memchr (eq + 1, ',', left - 1);

	if (co) {
	    left -= co - eq;
	}

	mk_clear_string (&tag);
	mk_clear_string (&class);

	tag = mk_string_new_shared_len (begin, eq - begin);
	class = mk_string_new_shared_len (eq + 1, (co ? co :
						   eq + strlen (eq) - 1)
					  - eq - 1);

	begin = co + 1;
	classes = mk_key_value_add_string (classes, tag, class);
    } while (co);

    mk_key_value_qsort (classes);

    return g_steal_pointer (&classes);
}

static AttrInfo *
attr_info_new_from_spec (GParamSpec *spec,
			 MkTypeTable *type_table,
			 GError **error)
{
    g_autoptr (MkKeyValue) classes = NULL;
    g_autoptr (MkString) prop = NULL;
    GError *local_error = NULL;
    MkTypeInfo *element_info;
    GType element_type;
    const gchar *blurb, *element_begin, *element_end;
    const gchar *name_begin, *name_end;
    const gchar *tag_begin, *tag_end;
    const gchar *classes_begin, *classes_end;
    MkString *element = NULL, *name = NULL, *tag = NULL;

    if (!(spec->flags & G_PARAM_CONSTRUCT)) {
	return NULL;
    }

    prop = mk_string_new_shared (g_param_spec_get_name (spec));
    blurb = g_param_spec_get_blurb (spec);

    find_attribute (blurb, "element", &element_begin, &element_end);
    find_attribute (blurb, "name", &name_begin, &name_end);
    find_attribute (blurb, "tag", &tag_begin, &tag_end);
    find_attribute (blurb, "classes", &classes_begin, &classes_end);

    if (element_end > element_begin) {
	element = mk_string_newa_len (element_begin, element_end - element_begin);
    }

    if (name_end > name_begin) {
	name = mk_string_newa_len (name_begin, name_end - name_begin);
    }

    if (tag_end > tag_begin) {
	tag = mk_string_newa_len (tag_begin, tag_end - tag_begin);
    }

    if (!name) {
	name = prop;
    }

    if (!tag) {
	tag = name;
    }

    if (classes_end > classes_begin) {
	classes = make_classes (classes_begin, classes_end, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return NULL;
	}
    }

    if (g_type_is_a (spec->value_type, MK_TYPE_ARRAY)) {
	g_assert (element);

	element_type = g_type_from_name (element);

	if (!element_type) {
	    element_info = mk_type_table_resolve (type_table, element,
						  &local_error);

	    if (local_error) {
		g_propagate_error (error, local_error);
		return NULL;
	    }

	    element_type = mk_type_info_load (element_info, &local_error);

	    if (local_error) {
		g_propagate_error (error, local_error);
		return NULL;
	    }
	}

	return attr_info_new (tag, name, prop, classes, TRUE,
			      MK_TYPE_ARRAY, element_type);
    }

    return attr_info_new (tag, name, prop, classes, FALSE,
			  spec->value_type, G_TYPE_NONE);
}

static void
attr_info_free (AttrInfo *ai)
{
    if (g_ref_count_dec (&ai->rc)) {
	mk_string_unref (ai->tag);
	mk_string_unref (ai->name);
	mk_string_unref (ai->prop);
	g_clear_pointer (&ai->classes, mk_key_value_unref);
	g_slice_free (AttrInfo, ai);
    }
}

typedef struct {
    GType type;
    GHashTable *ai_table;
} TypeInfo;

static void
type_info_free (TypeInfo *ti)
{
    g_hash_table_unref (ti->ai_table);
    g_slice_free (TypeInfo, ti);
}

typedef struct {
    TypeInfo *ti;
    MkKeyValue *params;
    MkBuilder *self;
} Scope;

static void
scope_unset (Scope *scope)
{
    MkBuilderPrivate *p = mk_builder_get_instance_private (scope->self);

    if (mk_key_value_is_mutable (scope->params)) {
	scope->params = mk_key_value_clear (scope->params);
	p->params_cache = mk_array_append (p->params_cache, scope->params);
    }

    mk_key_value_unref (scope->params);
}

static TypeInfo *get_type_info (MkBuilder *self, GType type, GError **error);

static void
insert_attr_info (TypeInfo *ti, AttrInfo *ai)
{
    g_hash_table_insert (ti->ai_table,
			 mk_string_ref (ai->tag),
			 attr_info_ref (ai));

    MK_KEY_VALUE_FOREACH (ai->classes, pair) {
	g_hash_table_insert (ti->ai_table,
			     mk_string_ref (pair.key),
			     attr_info_ref (ai));
    }
}

static TypeInfo *
create_type_info (MkBuilder *self, GType type, GError **error)
{
    MkBuilderPrivate *p = mk_builder_get_instance_private (self);
    g_autofree GParamSpec **specs = NULL;
    g_autoptr (GTypeClass) class = NULL;
    GError *local_error = NULL;
    GTypeClass *parent_class;
    TypeInfo *ti;
    AttrInfo *ai;
    guint num_specs, i;

    ti = g_slice_new (TypeInfo);

    ti->type = type;
    ti->ai_table = g_hash_table_new_full (g_str_hash,
					  g_str_equal,
					  (GDestroyNotify)
					  mk_string_unref,
					  (GDestroyNotify)
					  attr_info_free);

    if (g_type_is_a (type, G_TYPE_OBJECT)) {
	class = g_type_class_ref (type);
	parent_class = g_type_class_peek_parent (class);

	if (parent_class) {
	    get_type_info (self, G_TYPE_FROM_CLASS (parent_class),
			   &local_error);

	    if (local_error) {
		g_propagate_error (error, local_error);
		return NULL;
	    }
	}

	specs = g_object_class_list_properties (G_OBJECT_CLASS (class),
						&num_specs);

	for (i = 0; i < num_specs; i++) {
	    ai = g_hash_table_lookup (p->ai_cache, specs[i]);

	    if (!ai) {
		ai = attr_info_new_from_spec (specs[i], p->type_table,
					      &local_error);

		g_hash_table_insert (p->ai_cache, specs[i], ai);
	    }

	    if (local_error) {
		g_propagate_error (error, local_error);
		return NULL;
	    }

	    if (ai) {
		insert_attr_info (ti, ai);
	    }
	}
    } else {
	/*g_error ("not object: %s", g_type_name (type));*/
    }

    g_hash_table_insert (p->ti_table, (gpointer) type, ti);

    return ti;
}

static TypeInfo *
get_type_info (MkBuilder *self, GType type, GError **error)
{
    MkBuilderPrivate *p = mk_builder_get_instance_private (self);
    TypeInfo *ti;

    ti = g_hash_table_lookup (p->ti_table, (gpointer) type);

    if (!ti) {
	ti = create_type_info (self, type, error);
    }

    return ti;
}

static void
finalize (GObject *gobj)
{
    MkBuilder *self = MK_BUILDER (gobj);
    MkBuilderPrivate *p = mk_builder_get_instance_private (self);

    g_hash_table_unref (p->ti_table);
    g_hash_table_unref (p->ai_cache);
    g_object_unref (p->type_table);
    mk_array_unref (p->params_cache);
    mk_array_unref (p->scope_stack);

    G_OBJECT_CLASS (mk_builder_parent_class)->finalize (gobj);
}

static void
mk_builder_init (MkBuilder *self)
{
    MkBuilderPrivate *p = mk_builder_get_instance_private (self);

    p->ti_table = g_hash_table_new_full (g_direct_hash,
					 g_direct_equal,
					 NULL,
					 (GDestroyNotify)
					 type_info_free);
    p->ai_cache = g_hash_table_new_full (g_direct_hash,
					 g_direct_equal,
					 NULL,
					 (GDestroyNotify)
					 attr_info_free);
    p->type_table = mk_type_table_new ();
    p->params_cache = mk_array_new_type (MK_TYPE_KEY_VALUE);
    p->scope_stack = mk_array_new (G_TYPE_NONE, sizeof (Scope),
				   NULL, (MkGenericFree) scope_unset, 0);
}

static void
mk_builder_class_init (MkBuilderClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->finalize = finalize;
}

MkBuilder *
mk_builder_new ()
{
    return g_object_new (MK_TYPE_BUILDER, NULL);
}

void
mk_builder_include (MkBuilder *self, const gchar *namespace)
{
    MkBuilderPrivate *p = mk_builder_get_instance_private (self);

    mk_type_table_include (p->type_table, namespace);
}

static void
create_object (TypeInfo *ti, MkKeyValue *params,
	       GValue *out_value, GError **error)
{
    GObject *object;
    gchar **names;
    GValue *props;
    guint i;

    if (ti->type == MK_TYPE_KEY_VALUE) {
	g_value_init (out_value, MK_TYPE_KEY_VALUE);
	g_value_set_boxed (out_value, params);
	return;
    }

    g_assert (g_type_is_a (ti->type, G_TYPE_OBJECT));

    names = g_newa (gchar *, mk_key_value_get_count (params));
    props = g_newa (GValue, mk_key_value_get_count (params));

    for (i = 0; i < mk_key_value_get_count (params); i++) {
	names[i] = mk_key_value_key_at (params, i);
	props[i] = *mk_key_value_value_at (params, i);
    }

    object = g_object_new_with_properties (ti->type,
					   mk_key_value_get_count (params),
					   (const gchar **) names, props);

    g_assert (object);

    g_value_init (out_value, ti->type);
    g_value_take_object (out_value, object);
}

static void
append_param (Scope *scope, MkString *name, const GValue *object, GError **error)
{
    g_auto (GValue) tmp_value = G_VALUE_INIT;
    GType dst_type;
    AttrInfo *ai;
    GValue *av;

    if (scope->ti->type == MK_TYPE_KEY_VALUE) {
	scope->params = mk_key_value_add (scope->params, name, object);
	return;
    }

    ai = g_hash_table_lookup (scope->ti->ai_table, name);

    if (!ai) {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
		     "no such attribute \"%s\" in type %s", name,
		     g_type_name (scope->ti->type));
	return;
    }

    dst_type = ai->is_array ? ai->element_type : ai->type;

    if (ai->is_array) {
	av = mk_key_value_find (scope->params, ai->name);

	if (!av) {
	    g_value_init (&tmp_value, MK_TYPE_ARRAY);
	    g_value_take_boxed (&tmp_value, mk_array_new_type (ai->element_type));

	    scope->params = mk_key_value_add (scope->params, ai->name,
					      &tmp_value);
	    av = mk_key_value_value_at (scope->params, -1);
	}

	MK_ARRAY_OF_VALUE (av) = mk_array_append_value (MK_ARRAY_OF_VALUE (av),
							object);
    } else if (G_VALUE_HOLDS (object, dst_type)) {
	scope->params = mk_key_value_add (scope->params, ai->name, object);
    } else if (g_value_type_transformable (G_VALUE_TYPE (object), ai->type)) {
	g_value_init (&tmp_value, ai->type);
	g_value_transform (object, &tmp_value);

	scope->params = mk_key_value_add (scope->params, ai->name, &tmp_value);
    } else {
	g_assert_not_reached ();
    }
}

static gboolean
ignore_tag (MkString *tag)
{
    return mk_string_has_prefix (tag, "xmlns:");
}

static void
traverse (MkParserAction action, MkString *name, const GValue *value,
	  gpointer user_data, GError **error)
{
    g_auto (GValue) object = G_VALUE_INIT;
    MkBuilder *self = MK_BUILDER (user_data);
    MkBuilderPrivate *p = mk_builder_get_instance_private (self);
    GError *local_error = NULL;
    MkString *type_name;
    Scope sv, *scope;
    AttrInfo *ai;
    GType type;

    switch (action) {
    case MK_PARSER_BEGIN_OBJECT:
	memset (&sv, 0, sizeof (sv));

	if (mk_array_len (p->scope_stack)) {
	    scope = mk_array_get (p->scope_stack, -1);

	    if (scope->ti->type != MK_TYPE_KEY_VALUE) {
		ai = g_hash_table_lookup (scope->ti->ai_table, name);

		if (!ai) {
		    g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
				 "no such attribute \"%s\" in type %s", name,
				 g_type_name (scope->ti->type));
		    return;
		}

		if (ai->classes) {
		    value = mk_key_value_find (ai->classes, name);

		    if (!value) {
			g_set_error (error, MK_ERROR, MK_ERROR_BAD_VALUE,
				     "no class for tag \"%s\" in type %s",
				     name, g_type_name (scope->ti->type));
			return;
		    }

		    g_assert (G_VALUE_HOLDS (value, MK_TYPE_STRING));
		    type_name = g_value_get_boxed (value);
		    type = g_type_from_name (type_name);

		    if (!type) {
			g_set_error (error, MK_ERROR, MK_ERROR_BAD_TYPE,
				     "no such type \"%s\"", type_name);
			return;
		    }

		    sv.ti = get_type_info (self, type, error);
		}
	    }

	    if (!sv.ti) {
		sv.ti = get_type_info (self, ai->is_array ?
				       ai->element_type : ai->type, error);
	    }
	} else {
	    sv.ti = get_type_info (self, p->root_type, error);
	}

	if (!sv.ti) {
	    return;
	}

	if (mk_array_len (p->params_cache)) {
	    /*p->params_cache = mk_array_steal (p->params_cache, -1, &sv.params);*/
	    /*g_message ("hit! %u", mk_key_value_get_count (sv.params));*/

	    /*g_assert (sv.params);*/
	    /*sv.params = mk_key_value_clear (sv.params);*/
	} else {
	    /*g_message ("miss");*/
	}

	sv.params = mk_key_value_new ();
	sv.self = self;

	p->scope_stack = mk_array_append (p->scope_stack, &sv);
	break;

    case MK_PARSER_END_OBJECT:
	g_assert (mk_array_len (p->scope_stack));
	scope = mk_array_get (p->scope_stack, -1);

	create_object (scope->ti, scope->params,
		       &object, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return;
	}

	p->scope_stack = mk_array_remove (p->scope_stack, -1);

	if (mk_array_len (p->scope_stack)) {
	    scope = mk_array_get (p->scope_stack, -1);

	    append_param (scope, name, &object, error);
	} else {
	    g_value_unset (p->root_object);
	    g_value_init (p->root_object, scope->ti->type);
	    g_value_copy (&object, p->root_object);
	}
	break;

    case MK_PARSER_VALUE:
	if (!ignore_tag (name)) {
	    g_assert (mk_array_len (p->scope_stack));
	    scope = mk_array_get (p->scope_stack, -1);

	    append_param (scope, name, value, error);
	}
	break;
    }
}

/**
 * mk_builder_build_from_file:
 *
 * returns: (transfer full) (type GObject*)
 */
gpointer
mk_builder_build (MkBuilder *self, MkParser *parser, 
		  GType type, GError **error)
{
    g_auto (GValue) root_object = G_VALUE_INIT;
    MkBuilderPrivate *p = mk_builder_get_instance_private (self);
    GError *local_error = NULL;

    p->scope_stack = mk_array_clear (p->scope_stack);
    p->root_type = type;
    p->root_object = &root_object;

    mk_parser_traverse (parser, traverse, self, &local_error);

    p->scope_stack = mk_array_clear (p->scope_stack);

    if (local_error) {
	g_propagate_error (error, local_error);
	return NULL;
    }

    if (!G_VALUE_HOLDS (&root_object, G_TYPE_OBJECT)) {
	g_set_error (error, MK_ERROR, MK_ERROR_BAD_TYPE,
		     "builder root tree must be a GObject");

	return NULL;
    }

    g_assert (G_VALUE_HOLDS (&root_object, type));

    return g_value_dup_object (&root_object);
}
